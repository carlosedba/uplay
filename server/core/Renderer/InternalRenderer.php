<?php

namespace Rise\Renderer;

use Rise\Blade;

class InternalRenderer
{
    private $templatesPath;
    private $cachePath;
    private $renderer;

    /**
     * InternalRenderer constructor.
     * @param $templatesPath
     * @param $cachePath
     */
    public function __construct($templatesPath, $cachePath) {
        $this->templatesPath = $templatesPath;
        $this->cachePath = $cachePath;
        $this->renderer = new Blade($templatesPath, $cachePath);
    }

    /**
     * @return mixed
     */
    public function getTemplatesPath()
    {
        return $this->templatesPath;
    }

    /**
     * @return mixed
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }

    /**
     * @return Blade
     */
    public function getRenderer(): Blade
    {
        return $this->renderer;
    }
}