<?php

namespace Rise\Renderer;

use Rise\Blade;
use Rise\Directive\BladeDirective;
use Rise\Store;

class ThemeRenderer
{
    public $name;
    private $templatesPath;
    private $cachePath;
    private $directives;
    private $renderer;
    private $store;

    /**
     * InternalRenderer constructor.
     * @param $templatesPath
     * @param $cachePath
     */
    public function __construct($name, $templatesPath, $cachePath, $directives = [], Store $store)
    {
        $this->name = $name;
        $this->templatesPath = $templatesPath;
        $this->cachePath = $cachePath;
        $this->directives = $directives;
        $this->renderer = new Blade($templatesPath, $cachePath);
        $this->store = $store;

        $this->applyDirectives();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getTemplatesPath()
    {
        return $this->templatesPath;
    }

    /**
     * @return mixed
     */
    public function getCachePath()
    {
        return $this->cachePath;
    }

    /**
     * @return Blade
     */
    public function getRenderer(): Blade
    {
        return $this->renderer;
    }

    public function applyDirectives()
    {
        foreach ($this->directives as $class) {
            $directive = new $class($this->renderer, $this->store);

            if ($directive instanceof BladeDirective) {
                $this->renderer->directive($directive->getName(), $directive);
            }
        }
    }
}