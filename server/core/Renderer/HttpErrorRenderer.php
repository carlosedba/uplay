<?php

namespace Rise\Renderer;

use Throwable;

class HttpErrorRenderer
{
    public function __invoke(Throwable $exception, bool $displayErrorDetails): string
    {
        switch ($exception->getCode())
        {
            case 400: //HttpBadRequestException
                break;
            case 403: //HttpForbiddenException
                break;
            case 500: //HttpInternalServerErrorException
                break;
            case 405: //HttpMethodNotAllowedException
                break;
            case 404: //HttpNotFoundException
                break;
            case 501: //HttpNotImplementedException
                break;
            case 401: //HttpUnauthorizedException
                break;
        }

        $response = "<h1>" . $exception->getMessage() . "</h1>";

        return $response;
    }
}