<?php

namespace Rise\Response\MongoDB;

use Rise\Response\ExceptionResponse;

class UnexpectedValueExceptionResponse extends ExceptionResponse
{
    protected $code = 2000;
    protected $class = "UnexpectedValueException";
    protected $message = "Server response malformed.";
}