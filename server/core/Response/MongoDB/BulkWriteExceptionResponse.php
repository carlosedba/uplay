<?php

namespace Rise\Response\MongoDB;

use Rise\Response\ExceptionResponse;

class BulkWriteExceptionResponse extends ExceptionResponse
{
    protected $code = 2003;
    protected $class = "BulkWriteException";
    protected $message = "Write error.";
}