<?php

namespace Rise\Response\MongoDB;

use Rise\Response\ExceptionResponse;

class InvalidArgumentExceptionResponse extends ExceptionResponse
{
    protected $code = 2002;
    protected $class = "InvalidArgumentException";
    protected $message = "Invalid parameters provided.";
}