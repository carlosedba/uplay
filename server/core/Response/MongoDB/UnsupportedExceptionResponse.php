<?php

namespace Rise\Response\MongoDB;

use Rise\Response\ExceptionResponse;

class UnsupportedExceptionResponse extends ExceptionResponse
{
    protected $code = 2001;
    protected $class = "UnsupportedException";
    protected $message = "The options used are not supported by the selected server.";
}