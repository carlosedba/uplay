<?php

namespace Rise\Response\MongoDB;

use Rise\Response\ExceptionResponse;

class RuntimeExceptionResponse extends ExceptionResponse
{
    protected $code = 2004;
    protected $class = "RuntimeException";
    protected $message = "Driver level error.";
}