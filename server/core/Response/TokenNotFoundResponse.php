<?php

namespace Rise\Response;

class TokenNotFoundResponse
{
    protected $code = 3001;
    protected $message = "Token not found.";

    public function toJSON()
    {
        return json_encode([
            "code" => $this->code,
            "message" => $this->message,
        ]);
    }
}