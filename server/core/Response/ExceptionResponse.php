<?php

namespace Rise\Response;

abstract class ExceptionResponse
{
    protected $code;
    protected $class;
    protected $message;

    public function toJSON()
    {
        return json_encode([
            "code" => $this->code,
            "class" => $this->class,
            "message" => $this->message,
        ]);
    }
}