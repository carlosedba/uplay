<?php

namespace Rise\Response;

class InvalidTokenResponse
{
    protected $code = 3000;
    protected $message = "Invalid token provided.";

    public function toJSON()
    {
        return json_encode([
            "code" => $this->code,
            "message" => $this->message,
        ]);
    }
}