<?php

namespace Rise;

class Database
{
    protected $driver;
    protected $name;
    protected $connection;

    /**
     * Database constructor.
     * @param $driver
     * @param $name
     * @param $connection
     */
    public function __construct($driver, $name, $connection)
    {
        $this->driver = $driver;
        $this->name = $name;
        $this->connection = $connection;
    }

    /**
     * @return mixed
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return $this->connection;
    }
}