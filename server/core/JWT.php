<?php

namespace Rise;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Ecdsa\Sha256 as EcdsaSha256;
use Lcobucci\JWT\Signer\Ecdsa\Sha384 as EcdsaSha384;
use Lcobucci\JWT\Signer\Ecdsa\Sha512 as EcdsaSha512;
use Lcobucci\JWT\Signer\Hmac\Sha256 as HmacSha256;
use Lcobucci\JWT\Signer\Hmac\Sha384 as HmacSha384;
use Lcobucci\JWT\Signer\Hmac\Sha512 as HmacSha512;
use Lcobucci\JWT\Signer\Rsa\Sha256 as RsaSha256;
use Lcobucci\JWT\Signer\Rsa\Sha384 as RsaSha384;
use Lcobucci\JWT\Signer\Rsa\Sha512 as RsaSha512;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Token;
use InvalidArgumentException;

class JWT
{
    private static $iss = "";
    private static $aud = "";
    private static $jti = "";

    protected static function getSigner($signature, $algorithm)
    {
        if (strtolower($signature) == "hmac") {
            switch (strtolower($algorithm)) {
                case 'sha384':
                    return new HmacSha384();
                    break;

                case 'sha512':
                    return new HmacSha512();
                    break;

                default:
                    return new HmacSha256();
            }
        } else if (strtolower($signature) == "rsa") {
            switch (strtolower($algorithm)) {
                case 'sha384':
                    return new RsaSha384();
                    break;

                case 'sha512':
                    return new RsaSha512();
                    break;

                default:
                    return new RsaSha256();
            }
        } else if (strtolower($signature) == "ecdsa") {
            switch (strtolower($algorithm)) {
                case 'sha384':
                    return new EcdsaSha384();
                    break;

                case 'sha512':
                    return new EcdsaSha512();
                    break;

                default:
                    return new EcdsaSha256();
            }
        } else {
            throw new InvalidArgumentException("The signature must be one of the following: HMAC, RSA or ECDSA.");
        }
    }

    protected static function getBuilder($payload)
    {
        $builder = (new Builder())
            // iss claim
            ->issuedBy(self::$iss)
            // aud claim
            ->permittedFor(self::$aud)
            // jti claim
            ->identifiedBy(self::$jti, true)
            // iat claim
            ->issuedAt(time())
            // nbf claim
            ->canOnlyBeUsedAfter(time() + 60)
            // exp claim
            ->expiresAt(time() + (7 * 24 * 60 * 60));

        if (count($payload)) {
            foreach ($payload as $key => $value) {
                $builder->withClaim($key, $value);
            }
        }

        return $builder;
    }

    /**
     * Generate a signed token
     *
     * @param string $signature
     * @param string $algorithm
     * @param string $key
     * @param array $payload
     */
    public static function generate($signature = 'hmac', $algorithm = 'sha256', $key = '', $payload = [])
    {
        if (strtolower($signature) == "hmac") {
            if (strlen($key) > 0) {
                $builder = self::getBuilder($payload);
                $signer = self::getSigner($signature, $algorithm);
                $token = $builder->getToken($signer,  new Key($key));

                return $token;
            } else {
                throw new InvalidArgumentException("The secret key must a string.");
            }
        } else if (strtolower($signature) == "rsa" || strtolower($signature) == "ecdsa") {
            if (strlen($key) > 0) {
                $builder = self::getBuilder($payload);
                $signer = self::getSigner($signature, $algorithm);
                $privateKey = new Key("file://" . $key);
                $token = $builder->getToken($signer,  $privateKey);

                return $token;
            } else {
                throw new InvalidArgumentException("The key argument must a string.");
            }
        } else {
            throw new InvalidArgumentException("The signature must be one of the following: HMAC, RSA or ECDSA.");
        }
    }


    /**
     * Verifies if the token is valid
     *
     * @param string $signature
     * @param string $algorithm
     * @param string $key
     * @param string|Token $token
     */
    public static function verify($signature = 'hmac', $algorithm = 'sha256', $key = '', $token)
    {
        if (is_string($token)) {
            $token = (new Parser())->parse((string) $token);
        }

        if ($token instanceof Token) {
            if (strtolower($signature) == "hmac") {
                $signer = self::getSigner($signature, $algorithm);
                $tokenIsValid = $token->verify($signer, new Key($key));

                return ($tokenIsValid) ? $token : false;
            } else if (strtolower($signature) == "rsa" || strtolower($signature) == "ecdsa") {
                $signer = self::getSigner($signature, $algorithm);
                $publicKey = new Key("file://" . $key);
                $tokenIsValid = $token->verify($signer, $publicKey);

                return ($tokenIsValid) ? $token : false;
            } else {
                throw new InvalidArgumentException("The signature must be one of the following: HMAC, RSA or ECDSA.");
            }
        } else {
            throw new InvalidArgumentException("The token must be a string or a instance of Lcobucci\JWT\Token.");
        }
    }
}