<?php

namespace Rise\Middleware;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Server\MiddlewareInterface;
use Rise\Auth\RequestMethodRule;
use Rise\Auth\RequestPathRule;
use Rise\Response\InvalidTokenResponse;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Rise\JWT;
use Rise\Response\TokenNotFoundResponse;
use RuntimeException;
use Exception;

class JWTAuthMiddleware implements MiddlewareInterface
{
    private $options = [
        "basePath" => "",
        "path" => null,
        "ignore" => null,
        "secure" => true,
        "relaxed" => ["localhost", "127.0.0.1"],
        "signature" => "RSA",
        "algorithm" => "SHA256",
        "publicKey" => null,
    ];

    private $rules;

    /**
     * JWTAuthMiddleware constructor.
     * @param $rules
     */
    public function __construct($options = [])
    {
        $this->options = array_merge($this->options, $options);

        /* Setup stack for rules */
        $this->rules = new \SplStack;

        /* If nothing was passed in options add default rules. */
        if (!isset($options["rules"])) {
            $this->addRule(new RequestMethodRule([
                "ignore" => ["OPTIONS"]
            ]));
        }

        /* If path was given in easy mode add rule for it. */
        if ($this->options["path"] !== null) {
            $this->addRule(new RequestPathRule([
                "basePath" => $this->options["basePath"],
                "path" => $this->options["path"],
                "ignore" => $this->options["ignore"]
            ]));
        }
    }

    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $this->process($request, $handler);
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        /* If rules say we should not authenticate call next and return. */
        if ($this->shouldAuthenticate($request) === false) {
            return $handler->handle($request);
        }

        $scheme = $request->getUri()->getScheme();
        $host = $request->getUri()->getHost();

        /* HTTP allowed only if secure is false or server is in relaxed array. */
        if ("https" !== $scheme && true === $this->options["secure"]) {
            if (!in_array($host, $this->options["relaxed"])) {
                $message = sprintf(
                    "Insecure use of middleware over %s denied by configuration.",
                    strtoupper($scheme)
                );
                throw new RuntimeException($message);
            }
        }

        /* If token cannot be found return with 401 Unauthorized. */
        if (($token = $this->fetchToken($request)) === false) {
            $response = new Response();
            $response = $response->withStatus(401);
            $response = $response->withHeader("Content-Type", "application/json");
            $response->getBody()->write((new TokenNotFoundResponse())->toJSON());
            return $response;
        }

        try {
            JWT::verify($this->options["signature"], $this->options["algorithm"], $this->options["publicKey"], $token);
        } catch (Exception $e) {
            error_log($e);
            $response = new Response();
            $response = $response->withStatus(401);
            $response = $response->withHeader("Content-Type", "application/json");
            $response->getBody()->write((new InvalidTokenResponse())->toJSON());
            return $response;
        }

        $response = $handler->handle($request);
        return $response;
    }

    /**
     * Check if middleware should authenticate
     *
     * @param ServerRequestInterface $request
     * @return boolean True if middleware should authenticate.
     */
    public function shouldAuthenticate(ServerRequestInterface $request)
    {
        /* If any of the rules in stack return false will not authenticate */
        foreach ($this->rules as $callable) {
            if (false === $callable($request)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Add rule to the stack
     *
     * @param callable $callable Callable which returns a boolean.
     * @return self
     */
    public function addRule($callable)
    {
        $this->rules->push($callable);
        return $this;
    }

    private function fetchToken(ServerRequestInterface $request)
    {
        $header = $request->getHeader("Authorization");

        if (count($header)) {
            $matches = [];

            preg_match("/Bearer\s+(.*)$/i", $header[0], $matches);

            if (count($matches)) {
                $token = $matches[1];

                return $token;
            }
        }

        return false;
    }
}