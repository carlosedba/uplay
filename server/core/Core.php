<?php

namespace Rise;

use DI\Bridge\Slim\Bridge as SlimBridge;
use DI\ContainerBuilder;
use MongoDB;
use Memcached;
use ORM;
use PDO;
use Rise\Directive\AssetDirective;
use Rise\Directive\RouteDirective;
use Rise\Renderer\HttpErrorRenderer;
use Rise\Renderer\ThemeRenderer;

class Core
{
    private $options = [
        "basePath" => "/",

        "theme" => "default",

        "memcached" => [
            "persistent_id" => null,
            "servers" => []
        ],

        "databases" => [],
        "definitions" => [],
        "middlewares" => [],
        "routes" => [],
        "store" => []
    ];

    private $database;
    private $memcached;
    private $renderers = [];
    private $store;
    private $slim;

    /**
     * Core constructor.
     * @param string $basePath
     */
    public function __construct(array $options = [])
    {
        $this->options = array_merge($this->options, $options);

        $this->loadMemcached();
        $this->loadDatabases();
        $this->loadStore();
        $this->loadSlim();
    }

    private function loadMemcached()
    {
        $options = $this->options["memcached"];

        if ($options && count($options["servers"])) {
            $persistent_id = $options["persistent_id"] ?? null;

            $this->memcached = new Memcached($persistent_id);

            foreach ($options["servers"] as $server) {
                $host = $server["host"] ?? "127.0.0.1";
                $port = $server["port"] ?? 11211;
                $weight = $server["weight"] ?? 0;

                $this->memcached->addServer($host, $port, $weight);
            }
        }
    }

    private function loadDatabases()
    {
        $databases = $this->options["databases"];

        if (count($databases)) {
            foreach ($databases as $db) {
                switch (strtolower($db["driver"])) {
                    case 'mongodb':
                        $this->addMongoDBConnection($db);
                        break;
                    case 'mysql':
                        $this->addMySQLConnection($db);
                        break;
                }
            }
        }
    }

    private function addMongoDBConnection($options)
    {
        $driver = $options["driver"];
        $database = $options["database"];
        $host = $options["host"];
        $port = $options["port"];
        $username = $options["username"];
        $password = $options["password"];

        $connection = (new MongoDB\Client('mongodb://' . $username . ':' . $password . '@' . $host . ':' . $port))
            ->selectDatabase($database);

        $db = new Database($driver, $database, $connection);

        $this->database = $db;

        return $this;
    }

    private function addMySQLConnection($options)
    {
        $driver = $options["driver"];
        $database = $options["database"];
        $host = $options["host"];
        $port = $options["port"];
        $username = $options["username"];
        $password = $options["password"];
        //$charset = $options["charset"];
        //$collation = $options["collation"];
        //$prefix = $options["prefix"];

        ORM::configure("{$driver}:host={$host}:{$port};dbname={$database}");
        ORM::configure('username', $username);
        ORM::configure('password', $password);
        ORM::configure('return_result_sets', true);
        ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));


    }

    private function loadStore()
    {
        $this->store = new Store();

        foreach ($this->options["store"] as $name => $value) {
            if (class_exists($value)) {
                $obj = new $value($this->memcached, $this->database);
                $this->store->add($name, $obj());
            }
        }
    }

    private function loadSlim()
    {
        $theme = $this->options["theme"];

        $this->addRenderer(new ThemeRenderer(
            "theme",
            THEMES_PATH . $theme,
            CACHE_PATH,
            [AssetDirective::class, RouteDirective::class],
            $this->store
        ));

        $container = $this->buildContainer();

        $this->slim = SlimBridge::create($container);

        $this->slim->setBasePath($this->options["basePath"]);

        $this->slim->addRoutingMiddleware();

        $this->addErrorMiddleware();

        $this->addMiddlewares();

        $this->loadRoutes();

        $this->slim->run();
    }

    private function addRenderer($renderer)
    {
        array_push($this->renderers, $renderer);

        return $this;
    }

    private function buildContainer()
    {
        $builder = new ContainerBuilder();

        $definitions = $this->options["definitions"];

        if (isset($this->database)) {
            $definitions = array_merge($definitions, [
                "db" => $this->database
            ]);
        }

        if (isset($this->memcached)) {
            $definitions = array_merge($definitions, [
                "mc" => $this->memcached
            ]);
        }

        if (isset($this->store)) {
            $definitions = array_merge($definitions, [
                "store" => $this->store
            ]);
        }

        foreach ($this->renderers as $renderer) {
            $definitions = array_merge($definitions, [
                $renderer->getName() => $renderer->getRenderer()
            ]);
        }

        $builder->addDefinitions($definitions);

        return $builder->build();
    }

    private function addErrorMiddleware()
    {
        $errorMiddleware = $this->slim->addErrorMiddleware(true, true, true);
        $errorHandler = $errorMiddleware->getDefaultErrorHandler();
    }

    private function addMiddlewares()
    {
        $middlewares = $this->options["middlewares"];

        foreach ($middlewares as $middleware) {
            $this->slim->add($middleware);
        }
    }

    private function loadRoutes()
    {
        $routes = $this->options["routes"];

        foreach ($routes as $class) {
            $obj = new $class();

            if (is_callable($obj)) {
                $obj($this->slim);
            }
        }
    }
}