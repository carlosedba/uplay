<?php
namespace Rise\Utils;

/**
* IdGenerator
*/
class IdGenerator
{
	static public function uniqueNumberId($len = 4)
	{
		return rand(pow(10, $len-1), pow(10, $len)-1);
	}

	static public function uniqueId($len = 4)
	{
    	return substr(md5(uniqid(mt_rand(), true)), 0, $len);
	}
}