<?php

namespace Rise\Utils;

use Illuminate\Support\Str;

class Arr
{
    public static function toCamelCase(array $arr)
    {
        $new = [];

        foreach ($arr as $key => $value) {
            $new[Str::camel($key)] = $value;
        }

        return $new;
    }
}