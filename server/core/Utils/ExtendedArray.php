<?php
namespace Rise\Utils;

/**
* ExtendedArray
*/
class ExtendedArray
{
  static public function uniqueByKey($arr, $key) {
    $uniquekeys = array();
    $output     = array();

    foreach ($arr as $item) {
      if (!in_array($item[$key], $uniquekeys)) {
        $uniquekeys[] = $item[$key];
        $output[]     = $item;
      }
    }
    
    return $output;
  }
}
?>
