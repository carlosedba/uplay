<?php
define('ROOT_PATH',         dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('CORE_PATH',			__DIR__ . DIRECTORY_SEPARATOR);
define('KEYS_PATH',         dirname(__DIR__) . DIRECTORY_SEPARATOR . 'keys' . DIRECTORY_SEPARATOR);
define('LOGS_PATH',         dirname(__DIR__) . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR);
define('CACHE_PATH',        dirname(__DIR__) . DIRECTORY_SEPARATOR . 'cache');
define('PUBLIC_PATH',       dirname(__DIR__) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR);
define('DATA_PATH',         PUBLIC_PATH . 'data' . DIRECTORY_SEPARATOR);
define('THEMES_PATH',       PUBLIC_PATH . 'themes' . DIRECTORY_SEPARATOR);
