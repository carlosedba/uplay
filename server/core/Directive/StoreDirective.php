<?php

namespace Rise\Directive;

use Rise\Renderer\ThemeRenderer;
use Rise\Store;

class StoreDirective extends BladeDirective
{
    private $renderer;
    private $store;

    /**
     * StoreDirective constructor.
     * @param $renderer
     * @param $store
     */
    public function __construct(ThemeRenderer $renderer, Store $store)
    {
        $this->name = "store";
        $this->renderer = $renderer;
        $this->store = $store;
    }

    public function __invoke($expression)
    {
        $expression = $this->renderer->stripQuotes($expression);
        return $this->store->get($expression);
    }
}