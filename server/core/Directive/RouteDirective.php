<?php

namespace Rise\Directive;

use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\UriNormalizer;
use Rise\Blade;
use Rise\Store;

class RouteDirective extends BladeDirective
{
    private $renderer;
    private $store;

    /**
     * StoreDirective constructor.
     * @param $renderer
     * @param $store
     */
    public function __construct(Blade $renderer, Store $store)
    {
        $this->name = "route";
        $this->renderer = $renderer;
        $this->store = $store;
    }

    public function __invoke($expression)
    {
        $expression = $this->renderer->stripQuotes($expression);
        $siteUrl = $this->store->get('config.site_url');

        return UriNormalizer::normalize(new Uri("{$siteUrl}/${expression}"), UriNormalizer::REMOVE_DUPLICATE_SLASHES)->__toString();
    }
}