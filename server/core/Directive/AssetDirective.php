<?php

namespace Rise\Directive;

use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\Psr7\UriNormalizer;
use Rise\Blade;
use Rise\Store;

class AssetDirective extends BladeDirective
{
    private $renderer;
    private $store;

    /**
     * StoreDirective constructor.
     * @param $renderer
     * @param $store
     */
    public function __construct(Blade $renderer, Store $store)
    {
        $this->name = "asset";
        $this->renderer = $renderer;
        $this->store = $store;
    }

    public function __invoke($expression)
    {
        $expression = $this->renderer->stripQuotes($expression);
        $siteUrl = $this->store->get('config.site_url');
        $theme = $this->store->get('config.theme');

        return UriNormalizer::normalize(new Uri("{$siteUrl}/themes/{$theme}/${expression}"), UriNormalizer::REMOVE_DUPLICATE_SLASHES)->__toString();
    }
}