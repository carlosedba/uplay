<?php

namespace Rise\Directive;

abstract class BladeDirective
{
    protected $name;

    abstract public function __invoke($expression);

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}