<?php

namespace Rise\Directive;

class SetDirective extends BladeDirective
{
    /**
     * SetDirective constructor.
     */
    public function __construct()
    {
        $this->name = "set";
    }

    public function __invoke($expression)
    {
        list($variable, $value) = explode(', ', str_replace(['(', ')'], '', $expression));
        return "<?php {$variable} = {$value}; ?>";
    }
}