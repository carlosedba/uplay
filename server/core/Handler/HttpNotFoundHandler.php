<?php

namespace Rise\Handler;

use Slim\Http\ServerRequest as Request;
use Slim\Http\Response as Response;
use Throwable;

class HttpNotFoundHandler
{
    public function __invoke(Request $request, Throwable $exception, bool $displayErrorDetails)
    {
        $response = new Response();
        $response = $response->withStatus(404);
        $response->getBody()->write('404 NOT FOUND');

        return $response->withStatus(404);
    }
}