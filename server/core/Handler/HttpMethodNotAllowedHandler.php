<?php

namespace Rise\Handler;

use Slim\Http\ServerRequest as Request;
use Slim\Http\Response as Response;
use Throwable;

class HttpMethodNotAllowedHandler
{
    public function __invoke(Request $request, Throwable $exception, bool $displayErrorDetails)
    {
        $response = new Response();
        $response = $response->withStatus(405);
        $response->getBody()->write('405 NOT ALLOWED');

        return $response->withStatus(405);
    }
}