DROP DATABASE IF EXISTS `uplay`;

CREATE DATABASE IF NOT EXISTS `uplay` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `uplay`;

CREATE TABLE IF NOT EXISTS `config` (
    `config_id`                   bigint        NOT NULL AUTO_INCREMENT,
    `config_name`                 varchar(255)  NOT NULL,
    `config_value`                varchar(255)  NOT NULL,
    `autoload`                    varchar(255)  NOT NULL DEFAULT false,
    PRIMARY KEY (`config_id`),
    UNIQUE INDEX ix_config_name (config_name),
    INDEX ix_autoload (autoload)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `users` (
    `user_id`               bigint       NOT NULL AUTO_INCREMENT,
    `first_name`            varchar(255) NOT NULL,
    `last_name`             varchar(255),
    `email`                 varchar(255) NOT NULL,
    `password`              varchar(255) NOT NULL,
    `email_confirmed`       bool         NOT NULL DEFAULT false,
    `created_at`            datetime     NOT NULL DEFAULT NOW(),
    `updated_at`            datetime     NOT NULL DEFAULT NOW() ON UPDATE NOW(),
    PRIMARY KEY (`user_id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `unidades` (
    `unidade_id`                  bigint       NOT NULL AUTO_INCREMENT,
    `nome`                        varchar(255) NOT NULL,
    `cep`                         varchar(255) NOT NULL,
    `rua`                         varchar(255),
    `numero`                      varchar(255),
    `complemento`                 varchar(255),
    `bairro`                      varchar(255),
    `cidade`                      varchar(255),
    `estado`                      varchar(255),
    `pais`                        varchar(255),
    `horario_seg_sex`             varchar(255),
    `horario_sab`                 varchar(255),
    `horario_dom`                 varchar(255),
    `telefone_1`                  varchar(255),
    `telefone_2`                  varchar(255),
    `celular_1`                   varchar(255),
    `celular_2`                   varchar(255),
    `created_at`                  datetime      NOT NULL DEFAULT NOW(),
    `updated_at`                  datetime      NOT NULL DEFAULT NOW() ON UPDATE NOW(),
    PRIMARY KEY (`unidade_id`),
    INDEX ix_nome (nome)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `terms` (
    `term_id`                     bigint       NOT NULL AUTO_INCREMENT,
    `term_name`                   varchar(200),
    `term_slug`		              varchar(200),
    PRIMARY KEY (`term_id`),
    INDEX ix_term_name (term_name),
    INDEX ix_term_slug (term_slug)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `term_taxonomy` (
    `term_taxonomy_id`            bigint       NOT NULL AUTO_INCREMENT,
    `term_id`                     bigint       NOT NULL,
    `parent_id`                   bigint,
    `taxonomy`                    varchar(200) NOT NULL,
    `description`                 text,
    `count`                       bigint       NOT NULL DEFAULT 0,
    PRIMARY KEY (`term_taxonomy_id`),
    FOREIGN KEY (`term_id`) REFERENCES `terms` (`term_id`),
    FOREIGN KEY (`parent_id`) REFERENCES `terms` (`term_id`),
    UNIQUE INDEX ix_term_id_taxonomy (term_id, taxonomy),
    INDEX ix_taxonomy (taxonomy)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `posts` (
    `post_id`                         bigint       NOT NULL AUTO_INCREMENT,
    `post_author_id`                  bigint       NOT NULL,
    `post_title`                      text         NOT NULL,
    `post_excerpt`		              text,
    `post_content`			          longtext,
    `post_status`			          varchar(255) NOT NULL DEFAULT 'publish',
    `post_slug`				          varchar(255) NOT NULL,
    `post_date`                       datetime     NOT NULL DEFAULT NOW(),
    `post_date_utc`                   datetime     NOT NULL DEFAULT NOW() ON UPDATE NOW(),
    `post_modified`                   datetime,
    `post_modified_gmt`               datetime,
    PRIMARY KEY (`post_id`),
    FOREIGN KEY (`post_author_id`) REFERENCES `users` (`user_id`),
    INDEX ix_post_title (post_title),
    INDEX ix_post_author_id (post_author_id),
    INDEX ix_post_status_date_id (post_status, post_date, post_id),
    INDEX ix_post_slug (post_slug)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `term_relationships` (
    `term_relationship_id`        bigint       NOT NULL AUTO_INCREMENT,
    `post_id`                     bigint       NOT NULL,
    `term_taxonomy_id`            bigint       NOT NULL,
    `term_order`                  int(11),
    PRIMARY KEY (`term_relationship_id`),
    FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE,
    FOREIGN KEY (`term_taxonomy_id`) REFERENCES `term_taxonomy` (`term_taxonomy_id`) ON DELETE CASCADE,
    INDEX ix_post_id_term_taxonomy_id (post_id, term_taxonomy_id),
    INDEX ix_term_taxonomy_id (term_taxonomy_id)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `files` (
    `file_id`                     bigint       NOT NULL AUTO_INCREMENT,
    `file_name`                   text         NOT NULL,
    `file_extension`              varchar(255) NOT NULL,
    `file_size`                   bigint       NOT NULL,
    `mime_type`                   varchar(255) NOT NULL,
    `directory`                   varchar(255) NOT NULL,
    `created_at`                  datetime     NOT NULL DEFAULT NOW(),
    `updated_at`                  datetime     NOT NULL DEFAULT NOW() ON UPDATE NOW(),
    PRIMARY KEY (`file_id`),
    INDEX ix_file_name (file_name),
    INDEX ix_file_extension (file_extension)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `post_files` (
    `post_file_id`                bigint       NOT NULL AUTO_INCREMENT,
    `post_id`                     bigint       NOT NULL,
    `file_id`                     bigint       NOT NULL,
    `role`                        varchar(255) NOT NULL,
    PRIMARY KEY (`post_file_id`),
    FOREIGN KEY (`post_id`) REFERENCES `posts` (`post_id`) ON DELETE CASCADE,
    FOREIGN KEY (`file_id`) REFERENCES `files` (`file_id`) ON DELETE CASCADE,
    INDEX ix_post_id_role (post_id, role)
) DEFAULT CHARSET=utf8;

INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('site_url', 'http://localhost/uplay/server/public_html/', true);
INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('site_home', 'http://localhost/uplay/server/public_html/', true);
INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('site_name', 'Academia Uplay Fitness', true);
INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('site_description', '', true);

INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('theme', 'uplay', true);
INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('admin_email', 'carlosedba@outlook.com', true);
INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('timezone_string', 'America/Sao_Paulo', true);

INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('mailserver_url', '', true);
INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('mailserver_login', '', true);
INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('mailserver_pass', '', true);
INSERT INTO `config` (`config_name`, `config_value`, `autoload`) VALUES ('mailserver_port', '', true);

INSERT INTO `users` (`first_name`, `last_name`, `email`, `password`) VALUES
(
    'Carlos Eduardo',
    'Barbosa de Almeida',
    'carlosedba@outlook.com',
    '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4'
);

INSERT INTO `posts` (`post_author_id`, `post_title`, `post_excerpt`, `post_content`, `post_slug`) VALUES
(
    1,
    'A lorem ipsum dolor sit amet, consectetur adipiscing elit',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas congue quisque egestas diam in. Massa tempor nec feugiat nisl pretium. Massa sed elementum tempus egestas.',
    '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas congue quisque egestas diam in. Massa tempor nec feugiat nisl pretium. Massa sed elementum tempus egestas. Ut eu sem integer vitae justo eget magna. Non quam lacus suspendisse faucibus interdum posuere lorem ipsum. Sem fringilla ut morbi tincidunt augue interdum velit euismod. Et malesuada fames ac turpis. Eget egestas purus viverra accumsan. Elementum eu facilisis sed odio morbi quis. Fames ac turpis egestas maecenas pharetra convallis posuere morbi. Felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices. Adipiscing elit duis tristique sollicitudin nibh. Diam vulputate ut pharetra sit. Tortor consequat id porta nibh venenatis cras sed felis eget. Ac turpis egestas maecenas pharetra convallis posuere morbi. Mi bibendum neque egestas congue quisque egestas diam in arcu. Quam nulla porttitor massa id. Tincidunt id aliquet risus feugiat.</p><p>Sit amet risus nullam eget felis. Tincidunt vitae semper quis lectus nulla at volutpat diam. At lectus urna duis convallis convallis tellus. Odio pellentesque diam volutpat commodo sed egestas egestas fringilla. Volutpat diam ut venenatis tellus in metus. In ornare quam viverra orci sagittis eu volutpat. Gravida rutrum quisque non tellus orci ac. Egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam. Tortor at risus viverra adipiscing at. Morbi blandit cursus risus at ultrices. Diam vulputate ut pharetra sit amet aliquam. Urna porttitor rhoncus dolor purus non enim praesent elementum. Ornare arcu odio ut sem nulla pharetra diam sit amet.</p><p>Eros donec ac odio tempor orci dapibus. Fringilla urna porttitor rhoncus dolor purus non. Nunc scelerisque viverra mauris in. Sem fringilla ut morbi tincidunt augue interdum velit. Vulputate sapien nec sagittis aliquam malesuada. Leo vel orci porta non pulvinar neque laoreet suspendisse interdum. Dui accumsan sit amet nulla facilisi morbi tempus. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Elit ut aliquam purus sit amet luctus venenatis lectus magna. Tincidunt eget nullam non nisi. Scelerisque purus semper eget duis at tellus at. Eget egestas purus viverra accumsan in nisl nisi. Tincidunt ornare massa eget egestas purus viverra accumsan. Egestas sed sed risus pretium quam vulputate dignissim. Tortor consequat id porta nibh venenatis cras sed felis. Morbi tristique senectus et netus et. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet. Urna duis convallis convallis tellus id interdum velit laoreet.</p>',
    'a-lorem-ipsum-dolor-sit-amet'
);

INSERT INTO `posts` (`post_author_id`, `post_title`, `post_excerpt`, `post_content`, `post_slug`) VALUES
(
    1,
    'E Lorem ipsum dolor sit amet, consectetur adipiscing elit',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas congue quisque egestas diam in. Massa tempor nec feugiat nisl pretium. Massa sed elementum tempus egestas.',
    '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas congue quisque egestas diam in. Massa tempor nec feugiat nisl pretium. Massa sed elementum tempus egestas. Ut eu sem integer vitae justo eget magna. Non quam lacus suspendisse faucibus interdum posuere lorem ipsum. Sem fringilla ut morbi tincidunt augue interdum velit euismod. Et malesuada fames ac turpis. Eget egestas purus viverra accumsan. Elementum eu facilisis sed odio morbi quis. Fames ac turpis egestas maecenas pharetra convallis posuere morbi. Felis bibendum ut tristique et egestas quis ipsum suspendisse ultrices. Adipiscing elit duis tristique sollicitudin nibh. Diam vulputate ut pharetra sit. Tortor consequat id porta nibh venenatis cras sed felis eget. Ac turpis egestas maecenas pharetra convallis posuere morbi. Mi bibendum neque egestas congue quisque egestas diam in arcu. Quam nulla porttitor massa id. Tincidunt id aliquet risus feugiat.</p><p>Sit amet risus nullam eget felis. Tincidunt vitae semper quis lectus nulla at volutpat diam. At lectus urna duis convallis convallis tellus. Odio pellentesque diam volutpat commodo sed egestas egestas fringilla. Volutpat diam ut venenatis tellus in metus. In ornare quam viverra orci sagittis eu volutpat. Gravida rutrum quisque non tellus orci ac. Egestas erat imperdiet sed euismod nisi porta lorem mollis aliquam. Tortor at risus viverra adipiscing at. Morbi blandit cursus risus at ultrices. Diam vulputate ut pharetra sit amet aliquam. Urna porttitor rhoncus dolor purus non enim praesent elementum. Ornare arcu odio ut sem nulla pharetra diam sit amet.</p><p>Eros donec ac odio tempor orci dapibus. Fringilla urna porttitor rhoncus dolor purus non. Nunc scelerisque viverra mauris in. Sem fringilla ut morbi tincidunt augue interdum velit. Vulputate sapien nec sagittis aliquam malesuada. Leo vel orci porta non pulvinar neque laoreet suspendisse interdum. Dui accumsan sit amet nulla facilisi morbi tempus. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non. Volutpat consequat mauris nunc congue nisi vitae suscipit tellus. Elit ut aliquam purus sit amet luctus venenatis lectus magna. Tincidunt eget nullam non nisi. Scelerisque purus semper eget duis at tellus at. Eget egestas purus viverra accumsan in nisl nisi. Tincidunt ornare massa eget egestas purus viverra accumsan. Egestas sed sed risus pretium quam vulputate dignissim. Tortor consequat id porta nibh venenatis cras sed felis. Morbi tristique senectus et netus et. Phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet. Urna duis convallis convallis tellus id interdum velit laoreet.</p>',
    'e-lorem-ipsum-dolor-sit-amet'
);

INSERT INTO `terms` (`term_name`, `term_slug`) VALUES
('Lorem', 'lorem'),
('Ipsum', 'ipsum');

INSERT INTO `term_taxonomy` (`term_id`, `taxonomy`) VALUES
(1, 'category'),
(2, 'category');

INSERT INTO `term_relationships` (`post_id`, `term_taxonomy_id`) VALUES
(1, 1),
(1, 2),
(2, 1);
