<?php

function config() {
	$prod = require __DIR__ . '/prod.php';
	$dev = require __DIR__ . '/dev.php';

	$default = [
        "basePath" => "",

        "theme" => "uplay",

        "memcached" => [],

        "databases" => [
            [
                "driver" => "mysql",
                "database" => "uplay",
                "host" => "mariadb",
                "port" => 3306,
                "username" => "root",
                "password" => "AquelaSenhaMarota"
            ]
        ],
	];

	switch (ENV) {
		case 'production':
			return array_replace_recursive($default, $prod);
		case 'development':
			return array_replace_recursive($default, $dev);
		default:
			return array_replace_recursive($default, $dev);
	}
}

return config();