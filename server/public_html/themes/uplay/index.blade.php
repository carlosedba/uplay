@extends('layouts.simple')

@section('title')
  Linoflix
@endsection

@section('description')
@endsection

@section('vendor-css')
  @parent
@endsection

@section('application-css')
  @parent
@endsection

@section('fonts')
  @parent
@endsection

@section('content')
  @parent
  <div id="root"></div>
@endsection

@section('footer-sections')
  @parent
@endsection

@section('scripts')
  @parent
  <script src="@asset('vendors.app.bundle.js')"></script>
  <script src="@asset('app.bundle.js')"></script>
@endsection