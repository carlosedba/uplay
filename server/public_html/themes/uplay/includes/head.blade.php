<head>
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<meta name="description" content="@yield('description')">

	@section('vendor-css')
		@show

	@section('application-css')
		@show

	@section('fonts')
		<!-- Fonts -->
			<link rel="stylesheet" href="https://use.typekit.net/rcw6jzq.css"/>
		@show

	@section('js')
		<script src="@asset('vendor/modernizr/modernizr-custom.js')"></script>
		@show
</head>