<!DOCTYPE html>
<html lang="pt-BR">
	@include('includes.head')
	<body class="{{$class ?? ''}}">
		@include('includes.header')
		@section('content')
			@show
		@include('includes.footer')
		@include('includes.scripts')
	</body>
</html>