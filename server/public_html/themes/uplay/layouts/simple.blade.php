<!DOCTYPE html>
<html lang="pt-BR">
	@include('includes.head')
	<body class="{{$class ?? ''}}">
		@section('content')
			@show
		@include('includes.scripts')
	</body>
</html>