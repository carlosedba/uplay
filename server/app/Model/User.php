<?php

namespace Uplay\Model;

use Uplay\BaseModel;

class User extends BaseModel
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'users';

    /**
     * The table primary key column name.
     *
     * @var string
     */
    public static $_id_column = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'sexo',
        'email_confirmed',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
}