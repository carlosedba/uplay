<?php

namespace Uplay\Model;

use Uplay\BaseModel;

class Unidade extends BaseModel
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'unidades';

    /**
     * The table primary key column name.
     *
     * @var string
     */
    public static $_id_column = 'unidade_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'cep',
        'rua',
        'numero',
        'complemento',
        'bairro',
        'cidade',
        'estado',
        'pais',
        'horario_seg_sex',
        'horario_sab',
        'horario_dom',
        'telefone_1',
        'telefone_2',
        'celular_1',
        'celular_2',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}