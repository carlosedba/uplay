<?php

namespace Uplay\Model;

use Uplay\BaseModel;

class TermRelationship extends BaseModel
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'term_relationships';

    /**
     * The table primary key column name.
     *
     * @var string
     */
    public static $_id_column = 'term_relationship_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'term_taxonomy_id',
        'term_order'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function termTaxonomy() {
        return $this->hasOne('TermTaxonomy', 'term_taxonomy_id');
    }
}