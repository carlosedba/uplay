<?php

namespace Uplay\Model;

use Uplay\BaseModel;

class PostFile extends BaseModel
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'post_files';

    /**
     * The table primary key column name.
     *
     * @var string
     */
    public static $_id_column = 'post_file_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_id',
        'file_id',
        'role'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function file() {
        return $this->hasOne('File', 'file_id');
    }
}