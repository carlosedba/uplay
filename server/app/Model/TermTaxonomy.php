<?php

namespace Uplay\Model;

use Uplay\BaseModel;

class TermTaxonomy extends BaseModel
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'term_taxonomy';

    /**
     * The table primary key column name.
     *
     * @var string
     */
    public static $_id_column = 'term_taxonomy_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'term_id',
        'parent_id',
        'taxonomy',
        'description',
        'count'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}