<?php

namespace Uplay\Model;

use Uplay\BaseModel;

class Post extends BaseModel
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'posts';

    /**
     * The table primary key column name.
     *
     * @var string
     */
    public static $_id_column = 'post_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_author_id',
        'post_title',
        'post_excerpt',
        'post_content',
        'post_status',
        'post_slug',
        'post_modified',
        'post_modified_gmt'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function terms() {
        return $this->hasManyThrough('TermTaxonomy', 'TermRelationship', 'post_id', 'term_taxonomy_id');
    }
}