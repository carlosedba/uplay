<?php

namespace Uplay\Model;

use Uplay\BaseModel;

class File extends BaseModel
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'files';

    /**
     * The table primary key column name.
     *
     * @var string
     */
    public static $_id_column = 'file_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file_name',
        'file_extension',
        'mime_type',
        'file_size',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}