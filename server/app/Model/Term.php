<?php

namespace Uplay\Model;

use Uplay\BaseModel;

class Term extends BaseModel
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'terms';

    /**
     * The table primary key column name.
     *
     * @var string
     */
    public static $_id_column = 'term_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'term_name',
        'term_slug'
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}