<?php

namespace Uplay\Model;

use Uplay\BaseModel;

class Config extends BaseModel
{
    /**
     * The table name.
     *
     * @var string
     */
    public static $_table = 'config';

    /**
     * The table primary key column name.
     *
     * @var string
     */
    public static $_id_column = 'config_id';

    /**
     * The fields that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'config_name',
        'config_value',
        'autoload',
    ];
}