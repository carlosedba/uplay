<?php
define('DATA_DIR_PICTURES',                DATA_PATH . 'pictures' . DIRECTORY_SEPARATOR);
define('DATA_DIR_VIDEOS',                  DATA_PATH . 'videos' . DIRECTORY_SEPARATOR);
define('DATA_DIR_PROFILE_PICTURES',        DATA_PATH . 'profile-pictures' . DIRECTORY_SEPARATOR);
define('DATA_DIR_OTHER',                   DATA_PATH . 'other' . DIRECTORY_SEPARATOR);
