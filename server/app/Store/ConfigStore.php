<?php

namespace Uplay\Store;

use Memcached;
use MongoDB\Driver\Exception\InvalidArgumentException;
use MongoDB\Driver\Exception\RuntimeException;
use MongoDB\Exception\UnsupportedException;
use Uplay\BaseModel;
use Uplay\Model\Config;
use Rise\Database;

class ConfigStore
{

    public function __construct()
    {
        //
    }

    public function __invoke()
    {
        $arr = [];
        $result = BaseModel::factory('Config')->findArray();

        foreach ($result as $config) {
            $arr[$config['name']] = $config['value'];
        }

        return $arr;
    }
}