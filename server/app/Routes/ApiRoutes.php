<?php

namespace Uplay\Routes;

use Uplay\Api\ApplicationApi;
use Uplay\Api\FileUploadApi;
use Uplay\Api\UsersApi;
use Psr\Container\ContainerInterface;
use Rise\JWT;
use Slim\App;
use Slim\Http\ServerRequest as Request;
use Slim\Http\Response as Response;
use Slim\Routing\RouteCollectorProxy;
use Uplay\BaseModel;

class ApiRoutes
{

    public function __invoke(App $router)
    {
        $router->group("/api/v1", function (RouteCollectorProxy $group) {
            $group->post("/file-upload[/]",
                [FileUploadApi::class, "main"]);

            $group->post("/application[/]",
                [ApplicationApi::class, "main"]);

            $group->get("/users/{userId}[/]",
                [UsersApi::class, "findOneById"]);
        });

        $router->post('/api/v1/token', function (Request $request, Response $response, ContainerInterface $container) {
            $data = $request->getParsedBody();

            $email = $data['email'];
            $password = $data['password'];

            $factory = BaseModel::factory('User');

            if ($user = $factory->where('email', $email)->findOne()) {
                if ($user->password == hash('sha256', $password)) {
                    $token = JWT::generate('RSA', 'SHA256', KEYS_PATH . "UPLAY_PEM_RSA_16052020", [
                        "user" => [
                            "user_id" => $user->user_id,
                            "first_name" => $user->first_name,
                            "last_name" => $user->last_name,
                            "email" => $user->email,
                        ]
                    ])->__toString();
                    $json = json_encode(array('token' => $token));
                    $response->getBody()->write($json);
                } else {
                    $json = json_encode(array(
                        "error" => [
                            "code" => 1001,
                            "message" => "Invalid password."
                        ]
                    ));

                    $response = $response->withStatus(401);
                    $response->getBody()->write($json);
                }
            } else {
                $json = json_encode(array(
                    "error" => [
                        "code" => 1002,
                        "message" => "Email not found."
                    ]
                ));

                $response = $response->withStatus(401);
                $response->getBody()->write($json);
            }

            return $response;
        });

        $router->post('/api/v1/token/renew', function (Request $request, Response $response, ContainerInterface $container) {
            $data = $request->getParsedBody();
            $token = $data['token'];

            $isValid = JWT::verify('RSA', 'SHA256', KEYS_PATH . "UPLAY_PEM_RSA_16052020.pub", $token);
            $factory = BaseModel::factory('User');

            if ($isValid) {
                if ($user = $factory->where('id', $isValid->getClaim('id'))->findOne()) {
                    $newToken = JWT::generate('RSA', 'SHA256', KEYS_PATH . "UPLAY_PEM_RSA_16052020", [
                        "user" => [
                            "user_id" => $user->user_id,
                            "first_name" => $user->first_name,
                            "last_name" => $user->last_name,
                            "email" => $user->email,
                        ]
                    ])->__toString();
                    $json = json_encode(array('token' => $token));
                    $response->getBody()->write($json);
                } else {
                    $json = json_encode(array(
                        "error" => [
                            "code" => 1003,
                            "message" => "Invalid token provided."
                        ]
                    ));
                }
            } else {
                $json = json_encode(array(
                    "error" => [
                        "code" => 1003,
                        "message" => "Invalid token provided."
                    ]
                ));

                $response = $response->withStatus(401);
                $response->getBody()->write($json);
            }

            return $response;
        });

        $router->post('/api/v1/token/validate', function (Request $request, Response $response, ContainerInterface $container) {
            $data = $request->getParsedBody();
            $token = $data['token'];

            $isValid = JWT::verify('RSA', 'SHA256', KEYS_PATH . "UPLAY_PEM_RSA_16052020.pub", $token);

            if ($token) {
                if ($isValid) {
                    $json = json_encode(array(
                        "token" => $token
                    ));
                } else {
                    $json = json_encode(array(
                        "error" => [
                            "code" => 1003,
                            "message" => "Invalid token provided."
                        ]
                    ));
                    $response = $response->withStatus(401);
                }
            } else {
                $json = json_encode(array(
                    "error" => [
                        "code" => 1004,
                        "message" => "Token not provided."
                    ]
                ));
                $response = $response->withStatus(401);
            }

            $response->getBody()->write($json);

            return $response;
        });
    }
}