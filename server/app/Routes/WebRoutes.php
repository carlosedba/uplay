<?php

namespace Uplay\Routes;

use Slim\Http\ServerRequest as Request;
use Slim\Http\Response as Response;
use Psr\Container\ContainerInterface;
use Slim\App;

class WebRoutes
{
    public function __invoke(App $router)
    {
        $router->get("/[{path:.*}]", function (Request $request, Response $response, ContainerInterface $container) {
            $theme = $container->get("theme");
            $store = $container->get("store");
            $siteUrl = $store->get("config.site_url");

            return $theme->render($response, "index");
        });
    }
}