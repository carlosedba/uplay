<?php

namespace Uplay\Api;

use Slim\Http\ServerRequest as Request;
use Slim\Http\Response as Response;
use Psr\Container\ContainerInterface;
use Uplay\BaseModel;

class UsersApi
{
	private $container;

	/**
	 * UsersApi constructor.
	 * @param $container
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function findAll(Request $request, Response $response)
	{
		$users = BaseModel::factory('User')->findArray();

		$json = json_encode($users);
		$response->getBody()->write($json);
		$response = $response->withHeader("Content-Type", "application/json");

		return $response;
	}

	public function findOneById(Request $request, Response $response, $userId)
	{
		$users = BaseModel::factory('User')
            ->where('user_id', $userId)
            ->findOne()
            ->asArray();

		$json = json_encode($users);
		$response->getBody()->write($json);
		$response = $response->withHeader("Content-Type", "application/json");

		return $response;
	}

	public function insertOne(Request $request, Response $response)
	{
        $data = $request->getParsedBody();

        $user = BaseModel::factory('User')->create(array(
			'first_name'	    => $data['first_name'],
			'last_name'         => $data['last_name'],
			'email' 	        => $data['email'],
			'password' 		    => $data['password'],
		));

		if (!$user->save()) {
			return $response->withStatus(500);
		}

		return $response->withStatus(201);
	}

    public function findOneAndUpdateById(Request $request, Response $response, $id)
    {
        $data = $request->getParsedBody();
		$files = $request->getUploadedFiles();

		$user = BaseModel::factory('User')
            ->where('id', $id)
            ->findOne();

		$user->fillAttributes($data);

		if ($user->save()) {
			$response = $response->withStatus(200);
		} else {
			$response = $response->withStatus(500);
		}

		return $response;
    }

	public function findOneAndDeleteById(Request $request, Response $response, $id)
	{
		$data = $request->getParsedBody();

		$user = BaseModel::factory('User')
            ->where('id', $id)
            ->findOne();

		if ($user->delete()) {
			$response = $response->withStatus(200);
		} else {
			$response = $response->withStatus(500);
		}

		return $response;
	}
}