<?php

namespace Uplay\Api;

use Uplay\BaseModel;
use Psr\Container\ContainerInterface;
use Slim\Http\Response as Response;
use Slim\Http\ServerRequest as Request;
use Exception;
use RuntimeException;
use Uplay\ErrorCodes;

class FileUploadApi
{
    private $container;

    /**
     * EpisodiosApi constructor.
     * @param $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function main(Request $request, Response $response)
    {
        $dir = null;
        $dirName = null;
        $year = date("Y");
        $month = date("m");

        $files = [];
        $errors = [];

        $data = $request->getParsedBody();
        $uploadedFiles = $request->getUploadedFiles();

        switch ($data["type"]) {
            case "picture":
                $dir = DATA_DIR_PICTURES;
                $dirName = 'DATA_DIR_PICTURES';
                break;
            case "video":
                $dir = DATA_DIR_VIDEOS;
                $dirName = 'DATA_DIR_VIDEOS';
                break;
            case "profile-picture":
                $dir = DATA_DIR_PROFILE_PICTURES;
                $dirName = 'DATA_DIR_PROFILE_PICTURES';
                break;
            default:
                $dir = DATA_DIR_OTHER;
                $dirName = 'DATA_DIR_OTHER';
        }

        $dir = $dir . $year . DIRECTORY_SEPARATOR . $month . DIRECTORY_SEPARATOR;

        $dirExists = is_dir($dir);

        if (!$dirExists) {
            $dirExists = mkdir($dir, 0755, true);
        }

        if ($dirExists) {
            for ($i = 0; $i < count($uploadedFiles["file"]); $i++) {
                $file = $uploadedFiles["file"][$i];

                $fileName = $file->getClientFilename();
                $filePath = $dir . $fileName;
                $fileSize = $file->getSize();
                $mimeType = $file->getClientMediaType();

                try {
                    $file->moveTo($filePath);

                    $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);

                    $dbFile = BaseModel::factory('File')
                        ->where("file_name", $fileName)
                        ->findOne();

                    if ($dbFile) {
                        $dbFile->file_size = $fileSize;

                        if (!$dbFile->save()) {
                            array_push($errors, [
                                "error_code"        => ErrorCodes::FILE_COULD_NOT_BE_SAVED,
                                "file_name"         => $fileName,
                                "file_size"         => $fileSize,
                            ]);
                        }
                    } else {
                        $dbFile = BaseModel::factory('File')->create(array(
                            'file_name'	        => $fileName,
                            'file_extension'    => $fileExtension,
                            'file_size'	        => $fileSize,
                            'mime_type'	        => $mimeType,
                            'directory'	        => $dirName,
                        ));

                        $dbFile->save();
                    }

                    $fileId = $dbFile->file_id;

                    array_push($files, [
                        'file_id'	        => $fileId,
                        'file_name'	        => $fileName,
                        'file_extension'    => $fileExtension,
                        'file_size'	        => $fileSize,
                        'mime_type'	        => $mimeType,
                        'directory'	        => $dirName,
                    ]);
                } catch (Exception $e) {
                    array_push($errors, [
                        "error_code"        => ErrorCodes::FILE_COULD_NOT_BE_MOVED,
                        "file_name"         => $fileName,
                        "file_size"         => $fileSize,
                    ]);

                    error_log($e);
                }
            }
        }

        $json = json_encode([
            "succeeded" => $files,
            "failed" => $errors
        ]);

        $response->getBody()->write($json);
        $response = $response->withHeader("Content-Type", "application/json");

        return $response;
    }
}