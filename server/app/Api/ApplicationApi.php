<?php

namespace Uplay\Api;

use Uplay\BaseModel;
use Psr\Container\ContainerInterface;
use Slim\Http\Response as Response;
use Slim\Http\ServerRequest as Request;
use Exception;

class ApplicationApi
{
    private $container;

    /**
     * EpisodiosApi constructor.
     * @param $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function newPost($requestPacket, Response $response)
    {
        $payload = $requestPacket["payload"];

        $postPayload = $payload['post'];
        $postCategories = $payload['categories'];
        $postTags = $payload['tags'];
        $postFiles = $payload['files'];

        $post = [
            'post_author_id'	    => $postPayload['post_author_id'],
            'post_title'            => $postPayload['post_title'],
            'post_excerpt' 	        => $postPayload['post_excerpt'],
            'post_content' 		    => $postPayload['post_content'],
            'post_status' 		    => $postPayload['post_status'],
            'post_slug' 		    => $postPayload['post_slug'],
        ];

        if (isset($postPayload['post_date'])) {
            $post = array_merge($post, [ 'post_date' => $postPayload['post_date'] ]);
        }

        $post = BaseModel::factory('Post')->create($post);

        if ($post->save()) {

            if (count($postCategories)) {
                for ($i = 0; $i < count($postCategories); $i++) {
                    $postCategory = $postCategories[$i];
                    $termTaxonomyId = $postCategory['term_taxonomy_id'];
                    $termOrder = $postCategory['term_order'];

                    $termRelationship = BaseModel::factory('TermRelationship')->create([
                        'post_id' => $post->post_id,
                        'term_taxonomy_id' => $termTaxonomyId,
                        'term_order' => $termOrder
                    ]);

                    $termRelationship->save();
                }
            }

            if (count($postTags)) {
                for ($i = 0; $i < count($postTags); $i++) {
                    $postTag = $postTags[$i];
                    $termTaxonomyId = $postTag['term_taxonomy_id'];
                    $termOrder = $postTag['term_order'];

                    $termRelationship = BaseModel::factory('TermRelationship')->create([
                        'post_id' => $post->post_id,
                        'term_taxonomy_id' => $termTaxonomyId,
                        'term_order' => $termOrder
                    ]);

                    $termRelationship->save();
                }
            }

            if (count($postFiles)) {
                for ($i = 0; $i < count($postFiles); $i++) {
                    $postFile = $postFiles[$i];
                    $fileId = $postFile['file_id'];
                    $role = $postFile['role'];

                    var_dump($postFile);

                    $postFile = BaseModel::factory('PostFile')->create([
                        'post_id' => $post->post_id,
                        'file_id' => $fileId,
                        'role' => $role
                    ]);

                    $postFile->save();
                }
            }


            $json = json_encode($post->asArray());
            $response->getBody()->write($json);
            $response = $response->withHeader("Content-Type", "application/json");

            return $response;
        }

        $response = $response->withStatus(500);
        return $response;
    }

    public function getPosts($requestPacket, Response $response)
    {
        $params = $requestPacket['params'];
        $orderBy = $params['orderBy'];
        $sortOrder = $params['sortOrder'] ?? 'ascending';
        $limit = $params['limit'] ?? 3;
        $after = $params['after'];
        $before = $params['before'];

        $orderAttribute = 'post_id';

        $data = [
            "data" => []
        ];

        $posts = BaseModel::factory('Post');

        if ($orderBy !== NULL) {
            $orderAttribute = array_keys($orderBy)[0];

            if ($sortOrder === 'ascending') {
                $posts = $posts->orderByAsc($orderAttribute);
            } else {
                $posts = $posts->orderByDesc($orderAttribute);
            }
        }

        if ($after !== NULL || $before !== NULL) {
            if ($after !== NULL || $before !== NULL) {
                $data['cursors'] = [];
            }

            if ($after !== NULL) {
                $posts = $posts->whereGt($orderAttribute, base64_decode($after));

                if ($orderBy === NULL) {
                    $posts = $posts->orderByAsc($orderAttribute);
                }
            }

            if ($before !== NULL) {
                $posts = $posts->whereLt($orderAttribute, base64_decode($before));

                if ($orderBy === NULL) {
                    $posts = $posts->orderByDesc($orderAttribute);
                }
            }
        }

        if ($limit !== NULL) $posts = $posts->limit($limit);

        $posts = $posts->findArray();

        for ($i = 0; $i < count($posts); $i++) {
            $post = $posts[$i];

            $categories = BaseModel::factory('Post')
                ->rawQuery("
                    SELECT tr.term_relationship_id, t.* from posts AS p
                    INNER JOIN term_relationships AS tr
                    ON p.post_id = tr.post_id 
                    INNER JOIN term_taxonomy AS tt
                    ON tt.term_taxonomy_id = tr.term_taxonomy_id
                    INNER JOIN terms AS t
                    ON t.term_id = tt.term_id
                    WHERE tt.taxonomy = 'category' AND p.post_id = ?
                ", [$post["post_id"]])
                ->findArray();

            $tags = BaseModel::factory('Post')
                ->rawQuery("
                    SELECT tr.term_relationship_id, t.* from posts AS p
                    INNER JOIN term_relationships AS tr
                    ON p.post_id = tr.post_id 
                    INNER JOIN term_taxonomy AS tt
                    ON tt.term_taxonomy_id = tr.term_taxonomy_id
                    INNER JOIN terms AS t
                    ON t.term_id = tt.term_id
                    WHERE tt.taxonomy = 'tag' AND p.post_id = ?
                ", [$post["post_id"]])
                ->findArray();

            $files = BaseModel::factory('Post')
                ->rawQuery("
                    SELECT pf.post_file_id, f.*, pf.role from posts AS p
                    INNER JOIN post_files AS pf
                    ON pf.post_id = p.post_id 
                    INNER JOIN files AS f
                    ON f.file_id = pf.file_id
                    WHERE p.post_id = ?
                ", [$post["post_id"]])
                ->findArray();

            $post["categories"] = $categories;
            $post["tags"] = $tags;
            $post["files"] = $files;

            array_push($data['data'], $post);
        }

        $first = $data['data'][0];
        $last = $data['data'][$limit - 1];


        $previous = BaseModel::factory('Post');
        $next = BaseModel::factory('Post');

        if ($sortOrder === 'asc') {
            $previous = $previous
                ->whereLt($orderAttribute, $first[$orderAttribute])
                ->orderByDesc($orderAttribute)
                ->findOne();

            $next = $next
                ->whereGt($orderAttribute, $last[$orderAttribute])
                ->orderByAsc($orderAttribute)
                ->findOne();
        }

        if ($sortOrder === 'desc') {
            $previous = $previous
                ->whereGt($orderAttribute, $first[$orderAttribute])
                ->orderByAsc($orderAttribute)
                ->findOne();

            $next = $next
                ->whereLt($orderAttribute, $last[$orderAttribute])
                ->orderByDesc($orderAttribute)
                ->findOne();
        }

        if ($previous !== false) {
            $data['cursors']['prev'] = base64_encode($first[$orderAttribute]);
        }

        if ($next !== false) {
            $data['cursors']['next'] = base64_encode($last[$orderAttribute]);
        }

        //$data['cursors']['before'] = base64_encode($first[$orderAttribute]);
        //$data['cursors']['after'] = base64_encode($last[$orderAttribute]);

        $json = json_encode($data);
        $response->getBody()->write($json);
        $response = $response->withHeader("Content-Type", "application/json");

        return $response;
    }

    public function getPost($requestPacket, Response $response)
    {
        $params = $requestPacket["params"];
        $postId = $params['postId'];

        $data = [];

        $post = BaseModel::factory('Post')
            ->where('post_id', $postId)
            ->findOne()
            ->asArray();

        $categories = BaseModel::factory('Post')
            ->rawQuery("
                SELECT t.*, tr.term_relationship_id from posts AS p
                INNER JOIN term_relationships AS tr
                ON p.post_id = tr.post_id 
                INNER JOIN term_taxonomy AS tt
                ON tt.term_taxonomy_id = tr.term_taxonomy_id
                INNER JOIN terms AS t
                ON t.term_id = tt.term_id
                WHERE tt.taxonomy = 'category' AND p.post_id = ?
            ", [$post["post_id"]])
            ->findArray();

        $tags = BaseModel::factory('Post')
            ->rawQuery("
                SELECT t.*, tr.term_relationship_id from posts AS p
                INNER JOIN term_relationships AS tr
                ON p.post_id = tr.post_id 
                INNER JOIN term_taxonomy AS tt
                ON tt.term_taxonomy_id = tr.term_taxonomy_id
                INNER JOIN terms AS t
                ON t.term_id = tt.term_id
                WHERE tt.taxonomy = 'tag' AND p.post_id = ?
            ", [$post["post_id"]])
            ->findArray();

        $post["categories"] = $categories;
        $post["tags"] = $tags;

        $data = array_merge($data, $post);

        $json = json_encode($data);
        $response->getBody()->write($json);
        $response = $response->withHeader("Content-Type", "application/json");

        return $response;
    }

    public function updatePost($requestPacket, Response $response)
    {
        $params = $requestPacket["params"];
        $postId = $params['postId'];

        $payload = $requestPacket["payload"];

        $post = BaseModel::factory('Post')
            ->where('post_id', $postId)
            ->findOne();

        $post->fill(array_merge($payload, [
            'post_modified' => date('Y-m-d H:i:s'),
            'post_modified_gmt' => gmdate("Y-m-d H:i:s"),
        ]));

        if ($post->save()) {
            $response = $response->withStatus(200);
            return $response;
        }

        $response = $response->withStatus(500);
        return $response;
    }

    public function deletePost($requestPacket, Response $response)
    {
        $params = $requestPacket["params"];
        $postId = $params['postId'];

        if (isset($postId)) {
            $post = BaseModel::factory("Post")
                ->where("post_id", $postId)
                ->findOne();

            if ($post->delete()) {
                $response = $response->withStatus(200);
                return $response;
            }
        }

        $response = $response->withStatus(500);
        return $response;
    }

    public function deleteTermRelationship($requestPacket, Response $response)
    {
        $params = $requestPacket["params"];
        $termRelationshipId = $params["termRelationshipId"];

        if (isset($termRelationshipId)) {
            $termRelationship = BaseModel::factory("TermRelationship")
                ->where("term_relationship_id", $termRelationshipId)
                ->findOne();

            if ($termRelationship->delete()) {
                $response = $response->withStatus(200);
                return $response;
            }
        }

        $response = $response->withStatus(500);
        return $response;
    }

    public function deleteFile($requestPacket, Response $response)
    {
        $params = $requestPacket["params"];

        $fileId = $params["fileId"];

        if (isset($fileId)) {
            $file = BaseModel::factory("File")
                ->where("file_id", $fileId)
                ->findOne();

            $fileName = $file->file_name;
            $createdAt = strtotime($file->created_at);
            $year = date('Y', $createdAt);
            $month = date('m', $createdAt);

            $filePath = constant($file->directory) . $year . DIRECTORY_SEPARATOR . $month . DIRECTORY_SEPARATOR . $fileName;

            if (unlink($filePath)) {
                if ($file->delete()) {
                    $response = $response->withStatus(200);
                    return $response;
                }
            }
        }

        $response = $response->withStatus(500);
        return $response;
    }

    public function main(Request $request, Response $response)
    {
        $requestPacket = $request->getParsedBody();
        $params = $requestPacket["params"];

        if (isset($params)) {
            switch ($params["action"]) {
                case "newPost":
                    return $this->newPost($requestPacket, $response);

                case "getPosts":
                    return $this->getPosts($requestPacket, $response);

                case "getPost":
                    return $this->getPost($requestPacket, $response);

                case "updatePost":
                    return $this->updatePost($requestPacket, $response);

                case "deletePost":
                    return $this->deletePost($requestPacket, $response);

                case "deleteTermRelationship":
                    return $this->deleteTermRelationship($requestPacket, $response);

                case "deleteFile":
                    return $this->deleteFile($requestPacket, $response);
            }
        }

        return $response;
    }
}