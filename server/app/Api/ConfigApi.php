<?php
namespace Uplay\Api;

use Slim\Http\ServerRequest as Request;
use Slim\Http\Response as Response;
use Uplay\BaseModel;
use Psr\Container\ContainerInterface;

class ConfigApi
{
	private $container;

	/**
	 * ConfigApi constructor.
	 * @param $container
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	public function findAll(Request $request, Response $response)
	{
        $configs = BaseModel::factory('Config')->findArray();
        
        $json = json_encode($configs);
        $response->getBody()->write($json);

        $response = $response->withHeader("Content-Type", "application/json");

        return $response;
	}

	public function findOneById(Request $request, Response $response, $id)
	{
	    $config = BaseModel::factory('Config')
            ->where('id', $id)
            ->findOne();

        if ($config) {
            $config = $config->asArray();
            $json = json_encode($config);
            $response->getBody()->write($json);
            $response = $response->withHeader("Content-Type", "application/json");
        } else {
            $response = $response->withStatus(500);
        }

        return $response;
	}

	public function insertOne(Request $request, Response $response)
	{
        $data = $request->getParsedBody();

        $config = BaseModel::factory('Config')->create(array(
            'name'      => $data['name'],
            'value'     => $data['value'],
            'autoload'  => $data['autoload'],
        ));

        if ($config->save()) {
            $response = $response->withStatus(201);
        } else {
            $response = $response->withStatus(500);
        }

        return $response;
	}

	public function findOneAndUpdateById(Request $request, Response $response, $id)
	{
        $data = $request->getParsedBody();

        $config = Model::factory('Config')
            ->where('id', $id)
            ->findOne();

        if ($config) {
            $config->fillAttributes($data);

            if ($config->save()) {
                $response = $response->withStatus(200);
            } else {
                $response = $response->withStatus(500);
            }
        } else {
            $response = $response->withStatus(500);
        }

        return $response;
	}

	public function findOneAndDeleteById(Request $request, Response $response, $id)
	{
        $data = $request->getParsedBody();

        $config = BaseModel::factory('Config')
            ->where('id', $id)
            ->findOne();

        if ($config) {
            if ($config->delete()) {
                $response = $response->withStatus(200);
            } else {
                $response = $response->withStatus(500);
            }
        } else {
            $response = $response->withStatus(500);
        }

        return $response;
	}
}