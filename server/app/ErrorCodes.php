<?php

namespace Uplay;

class ErrorCodes
{
    const FILE_COULD_NOT_BE_MOVED = '2000';
    const FILE_COULD_NOT_BE_SAVED = '2001';
}