<?php

namespace Uplay;

use Uplay\Routes\ApiRoutes;
use Uplay\Routes\WebRoutes;
use Uplay\Store\ConfigStore;
use Rise\Core;
use Rise\Middleware\JWTAuthMiddleware;

require "headers.php";

$config = require "../config/index.php";

BaseModel::$auto_prefix_models = 'Uplay\\Model\\';
BaseModel::$short_table_names = true;

$app = new Core([
    "basePath" => $config["basePath"],

    "theme" => $config["theme"],

    "databases" => $config["databases"],

    "middlewares" => [
        new JWTAuthMiddleware([
            "basePath" => $config["basePath"],
            "path" => ["/api/v1"],
            "ignore" => [
                "/api/v1/token",
            ],
            "secure" => false,
            "publicKey" => KEYS_PATH . "UPLAY_PEM_RSA_16052020.pub"
        ])
    ],

    "routes" => [
        ApiRoutes::class,
        WebRoutes::class,
    ],

    "store" => [
        "config" => ConfigStore::class
    ]
]);