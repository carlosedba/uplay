<?php

namespace Uplay\Services;

use PHPMailer;

class EnvioEmail {

    public function enviarEmailConfirmacaoCadastro($nome, $email, $idUsuario = null)
    {

        if (empty($nome) || empty($email) || empty($idUsuario))
            return false;

        # Link para confirmar o e-mail
        $link = "https://devsinparis.com/confirmar-conta/" . $idUsuario;

        $mail = new PHPMailer();

        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'contatolinoflix@gmail.com';
        $mail->Password = 'AquelaSenhaMarota';
        $mail->SMTPSecure = 'tls';
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        //$mail->SMTPDebug = 3;
        $mail->Port = 587;

        $mail->setFrom('uplay@outlook.com', 'Linoflix');
        $mail->addAddress($email, 'Linoflix');

        $mail->CharSet = 'UTF-8';
        $mail->isHTML(true);

        $mail->Subject = 'Confirmação de e-mail - Linoflix';
        $mail->Body = "<!DOCTYPE HTML> <html> <head> <title></title> <style type=\"text/css\"> table {border-collapse: collapse; border-spacing: 0; } </style> </head> <body style=\"margin: 0;\"> <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <!--tabela principal--> <tbody> <tr> <td> <center> <table width=\"600\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#ffffff\"> <!--segunda tabela --> <tbody> <tr> <td> <img width=\"600\" src=\"https://devsinparis.com/emkt/uplay-confirme-sua-conta/img/1.gif\" style=\"display: block;\"> </td> </tr> <tr> <td> <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tr> <td width=\"60\"> <img width=\"60\" height=\"75\" src=\"https://devsinparis.com/emkt/uplay-confirme-sua-conta/img/3.gif\" style=\"display: block;\"> </td> <td width=\"60\"> <img width=\"320\" height=\"75\"src=\"https://devsinparis.com/emkt/uplay-confirme-sua-conta/img/4.gif\" style=\"display: block;\"> </td> <td width=\"220\"> <img width=\"220\" height=\"75\" src=\"https://devsinparis.com/emkt/uplay-confirme-sua-conta/img/5.gif\" style=\"display: block;\"> </td> </tr> </tbody> </table> </td> </tr> <tr> <td> <img width=\"600\" src=\"https://devsinparis.com/emkt/uplay-confirme-sua-conta/img/6.gif\" style=\"display: block;\">  <!-- img texto --> </td> </tr> <tr> <td> <table width=\"600\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"> <tbody> <tr> <td width=\"150\"> <img width=\"150\" height=\"50\" src=\"https://devsinparis.com/emkt/uplay-confirme-sua-conta/img/7.gif\" style=\"display: block;\"> </td> <td width=\"300\"> <a href=\"{$link}\"> <img width=\"300\" height=\"50\" src=\"https://devsinparis.com/emkt/uplay-confirme-sua-conta/img/8.gif\" style=\"display: block;\"> </a> </td> <td width=\"150\"> <img width=\"150\" height=\"50\" src=\"https://devsinparis.com/emkt/uplay-confirme-sua-conta/img/9.gif\" style=\"display: block;\"> </td> </tr> </tbody> </table> </td> </tr> <tr> <td> <img src=\"https://devsinparis.com/emkt/uplay-confirme-sua-conta/img/10.png\"> </td> </tr> </tbody> </table> </center> </td> </tr> </tbody> </table> </body> </html>";

        if (!$mail->send())
            return false;

        return true;
    }

}