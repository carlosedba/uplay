<?php 
$inner = true;
require_once("inc/includes.php");

if($_REQUEST['filha']){
  $getPaginaPai = $paginas->getBySlug($_REQUEST['slug']);  
  $getPagina = $paginas->getPaginaBySlug($getPaginaPai->id,$_REQUEST['filha']);  
}else{
  $getPaginaPai = $paginas->getBySlug($_REQUEST['slug']);  
  $getPagina = $paginas->getBySlug($_REQUEST['slug']);  
}

if(!$getPagina->id){
  header( $_SERVER['SERVER_PROTOCOL']." 404 Not Found", true );
  header("location:/404.php");
  die();
}

define('META_TITLE', $getPagina->meta_title);
define('META_DESCRIPTION', $getPagina->meta_description);
$pagename = $getPagina->nome;
$capa = $getPagina->capa;
require_once("inc/header.php");
?>

  <section class="bggrey p-format p-y-60">
    <div class="row row-70">
      <div class="large-12 end columns">
      <?php echo $getPagina->conteudo;?>
      </div>
    </div><!--row-->
    
    <br/>

    <?php if($getPagina->id_galeria){
    $getGaleria = $galerias->get($getPagina->id_galeria);
    $getFotos = $fotos->getList($getGaleria->id);?> 

    <div class="row row-70">
      
      <?php 
        foreach ($getFotos as $key) {?>
        
        <div class="large-3 medium-4 columns end">
           <div class="wrapper-photo">
            <a data-lightbox="galeria" data-title="<?php echo $key->legenda;?>" href="/admin/uploads/<?php echo $key->imagem; ?>">
                <img src="/admin/uploads/<?php echo $key->imagem; ?>" alt="Galeria de Fotos" data-title="<?php echo $key->legenda; ?>" />
            </a>
          </div>
        </div>     

      <?php }?>

    </div><!--row-->

    <?php } ?>  



  </section>



<?php 
require_once("inc/footer.php");
?>