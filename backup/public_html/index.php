<?php 
session_start();
$index = true;
require_once("inc/includes.php");
define('META_TITLE', $config->meta_title);
define('META_DESCRIPTION', $config->meta_title);
require_once("inc/header.php");
?>



  <section id="sobre" class="bggrey p-format p-y-90">
    <div class="row row-fluid">
      <?php echo $sobre->html;?>
    </div><!--row-->
  </section><!--sobre-->

  <section class="bg-modalidades p-y-150" id="modalidades">

    <div class="row row-70">
      <div class="large-12 columns section-title">
        <h3 class="color-white">ACADEMIA ESPECIALIZADA EM SEU BEM-ESTAR.</h3>
        <h2 class="color-yellow">NOSSAS MODALIDADES</h2>
      </div>
    </div><!--row-->

    <div class="row row-70">
      <div class="large-12 columns">
        <p class="f-18 color-white m-p-10">Temos <span class="color-yellow">equipamentos</span> importados de ultima geração<br/> e <span class="color-yellow">professores altamente capacitados</span>.</p>
      </div>
    </div><!--row-->

    <div class="row row-70">
      <div class="large-8 columns">
          <br/>
          <ul class="large-block-grid-5 medium-block-grid-4 small-block-grid-2 box-modalidades">
            <?php foreach ($getModalidades as $showModalidades) {?>
              <li>
                <div class="wrapper-modalidade">
                  <a href="/modalidades/<?php echo $showModalidades->slug; ?>">
                    <img src="/admin/uploads/<?php echo $showModalidades->icone; ?>" alt="<?php echo $showModalidades->nome; ?>" title="<?php echo $showModalidades->nome; ?>">
                    <h6 class="txt-uppercase"><?php echo $showModalidades->nome; ?></h6> 
                  </a>
                </div>
              </li>
            <?php } ?>                                                                             
          </ul>
      </div>

    <div class="large-12 columns m-txt-center">
      <br/>
      <a href="#horarios" class="button btn-custom btn-yellow smooth-scroll">Confira os horários</a>
    </div>

    </div><!--row-->

  </section>

  <section id="horarios" class="bggrey p-format p-y-90">

    <div class="row row-70">
      <div class="large-12 columns section-title m-txt-center">
        <h2>CONFIRA OS HORÁRIOS</h2>
      </div>
    </div><!--row-->


    <?php 
      switch (date('N')) {
        case '1':
          $dia_semana = "Segunda";
          break;

        case '2':
          $dia_semana = "Terça";
          break;

        case '3':
          $dia_semana = "Quarta";
          break;

        case '4':
          $dia_semana = "Quinta";
          break;

        case '5':
          $dia_semana = "Sexta";
          break;

        case '6':
          $dia_semana = "Sábado";
          break;
      }

    ?>

    <div class="row row-70">
      <div class="large-9 medium-12 columns">
        <br/>
        <ul class="inline-list menu-horarios">
          <li id="Segunda" class="dia-semana <?php if($dia_semana == "Segunda") echo "active";?>"><a href="#">Segunda</a></li>
          <li id="Terça" class="dia-semana <?php if($dia_semana == "Terça") echo "active";?>"><a href="#">Terça</a></li>
          <li id="Quarta" class="dia-semana <?php if($dia_semana == "Quarta") echo "active";?>"><a href="#">Quarta</a></li>
          <li id="Quinta" class="dia-semana <?php if($dia_semana == "Quinta") echo "active";?>"><a href="#">Quinta</a></li>
          <li id="Sexta" class="dia-semana <?php if($dia_semana == "Sexta") echo "active";?>"><a href="#">Sexta</a></li>
          <li id="Sábado" class="dia-semana <?php if($dia_semana == "Sábado") echo "active";?>"><a href="#">Sábado</a></li>
        </ul>
      </div>


      <div class="large-3 medium-12 columns box-select-unidade">
        <label>
          Selecione uma unidade
          <select name="unidade" id="select_unidades">
            <?php foreach ($getUnidades as $showUnidades) {?>
              <option value="<?php echo $showUnidades->id; ?>"><?php echo $showUnidades->nome; ?></option>
            <?php } ?>
          </select>
        </label>
      </div>
    </div><!--row-->

    <br/><br/>

     <div class="row row-70" id="get-horarios">
     
          <?php 
            $getHorarios = $horarios->getListFront($dia_semana);
            foreach ($getHorarios as $showHorarios) {
            $getModalidade = $modalidades->get($showHorarios->id_modalidade);
          ?>
          
          <?php if($getModalidade->status){?>

          <div class="large-3 medium-4 columns end">
            <div class="wrapper-modalidade-horario">
              <div class="horario"><?php echo date('G:i', strtotime($showHorarios->hora_inicio));?>  - <?php echo date('G:i', strtotime($showHorarios->hora_fim));?></div>
               <img src="/admin/uploads/<?php echo $getModalidade->icone; ?>" alt="Horário" title="Horário">
              <h2><?php echo $getModalidade->nome; ?></h2>
              <a href="/modalidades/<?php echo $getModalidade->slug; ?>" class="button btn-custom">+ Detalhes</a>
            </div>
          </div>

          <?php } ?>


      <?php } ?>
    
     </div><!--row-->
 
  </section>

   <section id="unidades" class="p-y-90">
    <div class="row row-70">
      <div class="large-12 columns section-title">
        <h3 class="txt-uppercase">Estrutura de qualidade, para seu bem-estar</h3>
        <h2 class="txt-uppercase">Conheça nossas unidades</h2>
      </div>
    </div><!--row-->
    
    <br/>

    <div class="row row-70">

    <?php
    $getGaleria = $galerias->get(1);
    $getFotos = $fotos->getList($getGaleria->id);?> 

    <div class="row row-70">
      
      <?php 
        foreach ($getFotos as $key) {?>
        
        <div class="large-3 medium-4 columns end">
           <div class="wrapper-photo">
            <a data-lightbox="galeria" data-title="<?php echo $key->legenda;?>" href="/admin/uploads/<?php echo $key->imagem; ?>">
                <img src="/admin/uploads/<?php echo $key->imagem; ?>" alt="Galeria de Fotos" data-title="<?php echo $key->legenda; ?>" />
            </a>
          </div>
        </div>     

      <?php }?>

    </div><!--row-->


   </section>

<?php 
require_once("inc/footer.php");
?>