<?php 
$inner = true;
require_once("inc/includes.php");
$getModalidade = $modalidades->getBySlug($_REQUEST['slug']);

if(!$getModalidade->id){
  header( $_SERVER['SERVER_PROTOCOL']." 404 Not Found", true );
  header("location:/404.php");
  die();
}

define('META_TITLE', $getModalidade->meta_title);
define('META_DESCRIPTION', $getModalidade->meta_description);
$pagename = $getModalidade->nome;
$capa = $getModalidade->capa;
require_once("inc/header.php");
?>

  <section class="bggrey p-format p-y-60">
    <div class="row row-70">
      <div class="large-12 end columns">
      <?php echo $getModalidade->conteudo;?>
      </div>
    </div><!--row-->
    
    <br/>

    <?php if($getModalidade->id_galeria){
    $getGaleria = $galerias->get($getModalidade->id_galeria);
    $getFotos = $fotos->getList($getModalidade->id);?> 

    <div class="row row-70">
      
      <?php 
        foreach ($getFotos as $key) {?>
        
        <div class="large-3 medium-4 columns end">
           <div class="wrapper-photo">
            <a data-lightbox="galeria" data-title="<?php echo $key->legenda;?>" href="/admin/uploads/<?php echo $key->imagem; ?>">
                <img src="/admin/uploads/<?php echo $key->imagem; ?>" alt="Galeria de Fotos" data-title="<?php echo $key->legenda; ?>" />
            </a>
          </div>
        </div>     

      <?php }?>

    </div><!--row-->

    <?php } ?>  



  </section>



<?php 
require_once("inc/footer.php");
?>