<?php 
if( ! ini_get('date.timezone') ){
    date_default_timezone_set('GMT');
}
if($_GET['dia_semana']){
//Modalides
require_once("admin/modulos/modalidades/modalidades.class.php");
$modalidades = new Modalidades();
$getModalidades = $modalidades->getListFront();

//Unidades
require_once("admin/modulos/unidades/unidades.class.php");
$unidades = new Unidades();
$getUnidades = $unidades->getList();

//Horarios
require_once("admin/modulos/horarios/horarios.class.php");
$horarios = new Horarios();

$getHorarios = $horarios->getListFront($_GET['dia_semana'],$_GET['id_unidade']);
$count = $horarios->getListFront($_GET['dia_semana'],$_GET['id_unidade'])->rowCount();

if($count == 0){?>
<div class="large-12 columns"><h4>Nenhuma modalidade para esse dia</h4></div>	
<?php } ?>

<?php
foreach ($getHorarios as $showHorarios) {
$getModalidade = $modalidades->get($showHorarios->id_modalidade);

if($getModalidade->status){?>
  
  <div class="large-3 medium-4 columns end">
    <div class="wrapper-modalidade-horario">
      <div class="horario"><?php echo date('G:i', strtotime($showHorarios->hora_inicio));?>  - <?php echo date('G:i', strtotime($showHorarios->hora_fim));?></div>
       <img src="/admin/uploads/<?php echo $getModalidade->icone; ?>" alt="Horário" title="Horário">
      <h2><?php echo $getModalidade->nome; ?></h2>
      <a href="/modalidades/<?php echo $getModalidade->slug; ?>" class="button btn-custom">+ Detalhes</a>
    </div>
  </div>

  <?php } ?>



<?php } } ?>
