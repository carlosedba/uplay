const XEvent = window.XEvent = function () {
	console.log('log > XEvent - initialized!')

	this.Handlers = Object.create(Object.prototype)
}

XEvent.prototype.getHandler = function (name) {
	return this.Handlers[name]
}

XEvent.prototype.addHandler = function (name, func, context) {
	if (context === null || context === undefined) context = window
	return Object.defineProperty(this.Handlers, name, { value: func.bind(context) })
}

XEvent.prototype.removeHandler = function (name, func) {
	return delete this.Handlers[name]
}

XEvent.prototype.addTo = function (node, eventType, handlerName, propagation) {
	if (propagation === null || propagation === undefined) propagation = false
	if (node !== null && node !== undefined &&
		eventType !== null && eventType !== undefined &&
		handlerName !== null && handlerName !== undefined) {
		if (node.length === undefined || node.nodeName === 'SELECT') {
			node.addEventListener(eventType, this.getHandler(handlerName), propagation)
		} else {
			;[].forEach.call(node, function (el, i) {
				el.addEventListener(eventType, this.getHandler(handlerName), propagation)
			}.bind(this))
		}
	}
}

XEvent.prototype.removeFrom = function (node, eventType, handlerName, propagation) {
	if (propagation === null || propagation === undefined) propagation = false
	if (node !== null && node !== undefined &&
		eventType !== null && eventType !== undefined &&
		handlerName !== null && handlerName !== undefined) {
		if (node.length === undefined || node.nodeName === 'SELECT') {
			node.removeEventListener(eventType, this.getHandler(handlerName), propagation)
		} else {
			;[].forEach.call(node, function (el, i) {
				el.removeEventListener(eventType, this.getHandler(handlerName), propagation)
			}.bind(this))
		}
	}
}

