const Modal = Rise.prototype.Modal = function () {
	console.log('log > Rise > Modal - initialized!')

	this.windowWidth = window.innerWidth

	this.Event = new window.XEvent()
	this.Event.addHandler('handleOpenModal', this.handleOpenModal)
	this.Event.addHandler('handleCloseModal', this.handleCloseModal)

	document.addEventListener('DOMContentLoaded', this.render.bind(this))
	//window.addEventListener('resize', this.render.bind(this))
}

Modal.prototype.handleOpenModal = function (event) {
	const riseModal = document.querySelector('.rise-modal')

	event.path.forEach(function (el, i) {
		if (el.hasAttribute && el.hasAttribute('data-modal')) {
			const modalName = el.dataset.modal
			const modal = document.querySelector(`[data-name=${modalName}]`)
			riseModal.classList.toggle('is-open')
			modal.classList.toggle('open')
		}
	})
}

Modal.prototype.handleCloseModal = function (event) {
	event.preventDefault()
	event.stopPropagation()

	console.log(event.target)

	const riseModal = document.querySelector('.rise-modal')

	if (event.target.classList.contains('sweet-modal-overlay')) {
		event.target.classList.remove('open')
		setTimeout(function () { riseModal.classList.remove('is-open') }, 260)
	} else if (event.target.classList.contains('sweet-modal-close-link')) {
		event.path.forEach(function (el, i) {
			if (el.classList && el.classList.contains('sweet-modal-overlay')) {
				el.classList.remove('open')
				setTimeout(function () { riseModal.classList.remove('is-open') }, 260)
			}
		})
	}

	return false
}

Modal.prototype.render = function (event) {
	console.log('log > Rise > Modal - render called!')

	const modals = document.querySelectorAll('.rise-modal .sweet-modal-overlay')
	const triggers = document.querySelectorAll('[data-modal]')

	;[].forEach.call(modals, function (modal, i) {
		const close = modal.querySelector('.sweet-modal-close-link')
		this.Event.addTo(modal, 'click', 'handleCloseModal')
		this.Event.addTo(close, 'click', 'handleCloseModal', true)
	}, this)

	;[].forEach.call(triggers, function (trigger, i) {
		this.Event.addTo(trigger, 'click', 'handleOpenModal')
	}, this)
}

