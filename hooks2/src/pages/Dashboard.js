import React, { useState, useEffect } from 'react'

export default function Dashboard(props) {
  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <p className="page-title">Dashboard</p>
          </div>
        </div>
        <div className="page-content"></div>
      </div>
    </div>
  )
}