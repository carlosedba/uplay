import React, { useRef, useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'

import { login } from '@/actions/Auth'

export default function Login(props) {
  const auth = useSelector(state => state.Auth)
  const authenticated = useSelector(state => state.Auth.authenticated)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const dispatch = useDispatch()

  function handleEmailChange(event) {
    const value = event.target.value
    setEmail(value)
  }

  function handleSenhaChange(event) {
    const value = event.target.value
    setPassword(value)
  }

  function handleSubmit(event) {
    event.preventDefault()

    dispatch(login({ email, password }))
  }

  if (authenticated) {
    return (<Redirect to="/projetos"/>)
  }

  return (
    <div className="page page-login" style={{ backgroundImage: `url()` }}>
      <div className="overlay overlay-orange-1"></div>
      <div className="loginbox">
        <h1 className="loginbox-title">Login</h1>
        <form className="form" onSubmit={handleSubmit}>
          <div className="inputs">
            <div className="input">
              <label>E-mail</label>
              <input type="email"placeholder="E-mail" value={email} onChange={handleEmailChange}/>
            </div>
            <div className="input">
              <label>Senha</label>
              <input type="password" placeholder="Senha" value={password} onChange={handleSenhaChange}/>
              {(auth.error && auth.error.code === 1000) ? (
                <div className="input-alert">
                  <div className="svg icon">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510"><path d="M255 0C114.8 0 0 114.8 0 255s114.8 255 255 255 255-114.8 255-255S395.3 0 255 0zm30.4 406.8h-60.7v-60.7h60.7v60.7zm0-121.4h-60.7V103.2h60.7v182.2z"/></svg>
                  </div>
                  <span className="message">Um erro ocorreu, contate administrador do sistema.</span>
                </div>
              ) : null}

              {(auth.error && auth.error.code === 1001) ? (
                <div className="input-alert">
                  <div className="svg icon">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510"><path d="M255 0C114.8 0 0 114.8 0 255s114.8 255 255 255 255-114.8 255-255S395.3 0 255 0zm30.4 406.8h-60.7v-60.7h60.7v60.7zm0-121.4h-60.7V103.2h60.7v182.2z"/></svg>
                  </div>
                  <span className="message">Dados incorretos, tente novamente.</span>
                </div>
              ) : null}
            </div>
          </div>
          <div className="loginbox-links hidden">
            <a href="">Esqueceu a password?</a>
          </div>
          <button className="btn btn-login" type="submit">Entrar</button>
        </form>
        </div>
    </div>
  )
}