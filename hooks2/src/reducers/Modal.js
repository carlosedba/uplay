import {
  ADD_MODAL, OPEN_MODAL, CLOSE_MODAL, UPDATE_MODAL, RESET_MODAL, REMOVE_MODAL,
} from '@/actionTypes'

import update from 'immutability-helper'

const INITIAL_STATE = {}

export default function(state = INITIAL_STATE, action) {
  let modalName, modalParams, modalData, modalHandlers, error

  switch (action.type) {
  case ADD_MODAL:
    return {
      ...state,
      [action.payload.name]: {
        isOpen: false,
        params: {},
        data: {}
      }
    }

    case OPEN_MODAL:
      modalName = action.payload.name
      modalParams = action.payload.params
      modalData = action.payload.data
      modalHandlers = action.payload.handlers

      return update(state, {
        $merge: {
          [modalName]: {
            isOpen: true,
            params: modalParams,
            data: modalData,
            handlers: modalHandlers,
          }
        }
      })

  case CLOSE_MODAL:
    return update(state, {
      [action.payload.name]: { $merge: { isOpen: false } }
    })

  case UPDATE_MODAL:
    return update(state, {
      [action.payload.name]: action.payload.props
    })

  case RESET_MODAL:
    return update(state, {
      [action.payload.name]: { $set: { isOpen: false } }
    })

  case REMOVE_MODAL:
    return update(state, { $unset: [action.payload.name] })

  default:
    return state
  }
}

