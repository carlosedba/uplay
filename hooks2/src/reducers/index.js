import { combineReducers } from 'redux'

import Auth from './Auth'
import Modal from './Modal'

const reducer = combineReducers({
  Auth,
  Modal,
})

export default reducer

