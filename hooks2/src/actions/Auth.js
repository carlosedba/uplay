import ls from 'store'

import AuthHelper from '@/helpers/AuthHelper'

import * as types from '@/actionTypes'

export function login(data) {
  return async dispatch => {
    const { token } = await AuthHelper.fetchToken(data).catch(console.error) || {}

    if (token) {
      const userData = AuthHelper.extractUserData(token)

      ls.set('token', token)
      ls.set('user', userData)
    }
  }
}

export function validateToken(receivedToken) {
  return async dispatch => {
    const { token } = await AuthHelper.validateToken(receivedToken).catch(console.error) || {}

    if (token) {
      const userData = AuthHelper.extractUserData(token)

      ls.set('user', userData)
    }
  }
}

export function renewToken(receivedToken) {
  return async dispatch => {
    const { token } = await AuthHelper.renewToken(receivedToken).catch(console.error) || {}

    if (token) {
      const userData = AuthHelper.extractUserData(token)

      ls.set('token', token)
      ls.set('user', userData)
    }
  }
}

export function logout() {
  return async dispatch => {
    ls.remove('token')
    ls.remove('user')

    dispatch({ type: types.AUTH_LOGOUT })
  }
}