import api from '@/api'

function get(getPacket) {
  return new Promise((resolve, reject) => {
    api.post('/application', {}, getPacket)
      .then((response) => {
        const data = response.data
        if (data) resolve(data)
        else reject(response)
      })
      .catch(reject)
  })
}

function send(getPacket) {
  return new Promise((resolve, reject) => {
    api.post('/application', {}, getPacket)
      .then((response) => {
        resolve(response)
      })
      .catch(reject)
  })
}

export default {
  newPost(payload) {
    return send({
      command: 'newPost',
      payload
    })
  },

  getPosts(params) {
    return send({
      command: 'getPosts',
      params
    })
  },

  getPost(params) {
    return send({
      command: 'getPost',
      params
    })
  },

  updatePost(params, payload) {
    return send({
      command: 'updatePost',
      params,
      payload
    })
  },

  deletePost(params) {
    return send({
      command: 'deletePost',
      params
    })
  },

  deleteTermRelationship(params) {
    return send({
      command: 'deleteTermRelationship',
      params
    })
  },

  deleteFile(params) {
    return send({
      command: 'deleteFile',
      params
    })
  },
}