import React, { useState, useEffect } from 'react'
import {
  Switch,
  Route
} from 'react-router-dom'

import Sidebar from '@/components/Sidebar'
import TopleftIndicators from '@/components/TopleftIndicators'

import Dashboard from '@/pages/Dashboard'
import Dev from '@/pages/Dev'
import Logout from '@/pages/Logout'

import Posts from '@/pages/Post/Posts'
import Post from '@/pages/Post/Post'
import NewPost from '@/pages/Post/NewPost'
import EditPost from '@/pages/Post/EditPost'


export default function AuthorizedLayout() {
  return (
    <div className="default-layout">
      <Sidebar/>

      <TopleftIndicators/>

      <Switch>
        <Route path="/logout" component={Logout}/>
        <Route path="/dev" component={Dev}/>

        <Route path="/posts/new" component={NewPost}/>
        <Route path="/posts/:id/editar" component={EditPost}/>
        <Route path="/posts/:id" component={Post}/>
        <Route path="/posts" component={Posts}/>

        <Route path="/" component={Dashboard}/>
      </Switch>
    </div>
  )
}

