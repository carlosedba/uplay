import React, { useState, useEffect } from 'react'
import {Link} from 'react-router-dom'
import _ from 'lodash'

import UserIndicator from '@/components/UserIndicator'

export default function TopleftIndicators(props) {
  return (
    <div className="topleft-indicators">
      <UserIndicator/>
    </div>
  )
}