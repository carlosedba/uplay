import React, { useState, useEffect } from 'react'
import {Link} from 'react-router-dom'
import {useSelector} from 'react-redux'
import _ from 'lodash'

import eu from '@/assets/img/eu.jpg'

export default function UserIndicator(props) {
  const user = useSelector(state => state.Auth.user)

  return (
    <div className="user-indicator">
      <div className="user-indicator-picture" style={{ backgroundImage: `url(${eu})` }}></div>
    </div>
  )
}''