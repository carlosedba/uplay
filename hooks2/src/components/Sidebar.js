import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'
import classNames from 'classnames'
import _ from 'lodash'

import user_placeholder from '@/assets/img/user_placeholder.png'

import {openModal} from '@/actions/Modal'

export default function Sidebar(props) {
  const user = useSelector(state => state.Auth.user)

  const location = useLocation()

  const dispatch = useDispatch()
  
  function sidebarMenuAction(name, fn, Icon) {
    return (
      <li className={classNames('sidebar-menu-item')} onClick={fn}>
        <a>
          {(Icon) ? (
            <div className="sidebar-menu-icon">
              <Icon/>
            </div>
          ) : null}
          <span className="sidebar-menu-item-text">{name}</span>
        </a>
      </li>
    )
  }

  function sidebarMenuLink(name, url, Icon) {
    return (
      <li className={classNames('sidebar-menu-item', {
        active: isCurrentUrl(url)
      })}>
        <Link to={url}>
          {(Icon) ? (
            <div className="sidebar-menu-icon">
              <Icon/>
            </div>
          ) : null}
          <span className="sidebar-menu-item-text">{name}</span>
        </Link>
      </li>
    )
  }

  function isCurrentUrl(url) {
    const pathname = location.pathname

    if (pathname === url) {
      return true
    }

    if (url.length > 1) {
      url = url.replace('/', '\/')

      const regex = new RegExp('^' + url)

      return pathname.match(regex)
    }
  }

  function handleConfigClick() {
    console.log('handleConfigClick')
    dispatch(openModal('ModalConfig'))
  }

  return (
    <nav className="sidebar">
      <div className="sidebar-top">
        <Link className="sidebar-logo" to="/">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 149.1"><path d="M21.5 79.1h32.4c7.5 0 9.1-6 9.1-9.9V55.3c0-2.1 1.8-3.9 3.9-3.9s3.9 1.8 3.9 3.9v13.8c0 8.2-5.6 17.7-15 17.7h-37c-8.9-.4-14.2-9.6-14.2-17.6V55.3c0-2.1 1.8-3.9 3.9-3.9s3.9 1.8 3.9 3.9v13.8c0 4 1.6 10 9.1 10" fill="#fcc633"/><path d="M131.6 51.4c6 0 11 4.8 11 10.8s-5 10.8-11 10.8H86.8v10c0 2.1-1.9 3.9-3.9 3.9-2.1 0-3.9-1.8-3.9-3.9V55.3c0-2.1 1.8-3.9 3.9-3.9h48.7zm-44.8 7.8v6H132c1.6 0 3.1-1.3 3.1-3 0-1.6-1.5-3-3.1-3H86.8zm126.7 26.2c-.7.9-1.9 1.5-3 1.5h-48.6c-6 0-11.1-4.8-11.1-10.8V55.3c0-2.1 1.8-3.9 3.9-3.9 2 0 3.9 1.8 3.9 3.9v20.8c0 1.6 1.5 3 3.1 3h46.9L227.2 54c1.9-2.5 5-4.2 8.5-4.2 3.3 0 6.3 1.5 8.2 4l19.8 26.4c.6.7 1.1 1.7 1.1 2.8 0 2.1-1.8 3.9-3.9 3.9-1.2 0-2.3-.6-3.1-1.6l-19-25.3c-.7-.9-1.9-2.2-3.2-2.2-1.2 0-2.4 1.4-3.1 2.2l-19 25.4zm22.2-12.3c2.8 0 5 2.3 5 5s-2.3 5-5 5c-2.8 0-5-2.3-5-5s2.2-5 5-5M321 52.6c.8-.6 1.7-1.1 2.8-1.1 2.1 0 3.9 1.8 3.9 3.9 0 1.2-.6 2.4-1.5 3.1-13.6 12-20.7 15.5-31.1 15.5-11.1 0-17.3-3.3-31.3-15.5-.9-.7-1.5-1.9-1.5-3.1 0-2.1 1.8-3.9 3.9-3.9 1.1 0 1.9.5 2.8 1.1 13.9 12.2 17.6 12.9 26.1 13 8.3 0 11.8-.7 25.9-13m-25.9 25.3c2.8 0 5 2.3 5 5 0 2.8-2.3 5-5 5-2.8 0-5.1-2.3-5.1-5s2.3-5 5.1-5" fill="#fff"/><path d="M312.8 22.7c-2-1.7-3.4-4.3-3.4-6.9 0-4.9 4.1-9 9-9 2.1 0 3.9.6 5.4 1.9l61.1 45.4c5.8 4.3 9.7 11.6 9.7 19.6 0 7.5-3.4 14.6-9.2 18.9L324.7 138c-1.7 1.5-3.9 2.6-6.4 2.6-4.9 0-9-4.1-9-9 0-2.8 1.3-5.4 3.6-7.1L371.1 81c2.1-1.7 5-4.5 5-7.3s-3.2-5.6-5-7.1l-58.3-43.9z" fill="#fcc633"/><path d="M152.9 103c-.3.3-.5.6-.7 1s-.3.8-.3 1.3v.4h11.9v1.2H152v4.9h-1.2v-6.5c0-.6.1-1.2.4-1.8.2-.5.6-1 1-1.4s.9-.7 1.4-1c.6-.2 1.1-.4 1.8-.4h9.3v1.2h-9.3c-.5 0-.9.1-1.3.3-.5.3-.9.5-1.2.8m22.3-2.2h1.2v11.1h-1.2v-11.1zm24.9 1.2h-6.3v9.9h-1.2V102h-6.3v-1.2h13.8v1.2zm22.7 9.8l-.4-.2-.4-.4-7.6-9-.3-.2-.4-.1c-.3 0-.5.1-.7.2-.2.2-.3.4-.3.6v9.1h-1.2v-9.2c0-.3.1-.6.2-.8.1-.2.3-.4.5-.6s.4-.3.7-.4.5-.1.8-.1.6.1.9.2c.3.1.5.3.8.6l7.6 9 .3.3.4.1c.2 0 .5-.1.7-.2.2-.2.3-.4.3-.6V101h1.2v9.2c0 .3-.1.6-.3.8-.1.2-.3.4-.5.6s-.4.3-.7.4-.5.1-.8.1c-.2-.1-.6-.2-.8-.3m16.5-8.8c-.3.3-.5.7-.7 1.1s-.3.8-.3 1.3v.4H251v1.2h-12.7v.4c0 .5.1.9.3 1.3s.4.8.7 1c.3.3.7.5 1.1.7.4.2.8.3 1.3.3h9.3v1.2h-9.3c-.6 0-1.2-.1-1.8-.4-.6-.2-1-.6-1.4-1s-.7-.9-1-1.5c-.3-.5-.4-1.1-.4-1.7v-2c0-.6.1-1.2.4-1.8.2-.5.6-1 1-1.4s.9-.7 1.4-1c.5-.2 1.1-.4 1.7-.4h9.3v1.2h-9.3c-.5 0-.9.1-1.3.3-.4.3-.7.5-1 .8m33.2 8.9H262v-1.2h10.5c.3 0 .5 0 .7-.1s.4-.2.6-.4c.2-.2.3-.4.4-.6s.1-.5.1-.7c0-.3-.1-.5-.1-.7-.1-.2-.2-.4-.4-.6-.2-.2-.4-.3-.6-.4s-.5-.1-.7-.1h-8.1c-.4 0-.8-.1-1.2-.2-.4-.2-.7-.4-1-.7-.3-.3-.5-.6-.7-1s-.2-.8-.2-1.2c0-.4.1-.8.2-1.2.2-.4.4-.7.7-1 .3-.3.6-.5 1-.7s.8-.3 1.2-.3H275v1.2h-10.5c-.3 0-.5 0-.7.2-.2.1-.4.2-.6.4-.2.1-.3.3-.4.6-.1.2-.1.5-.2.7 0 .5.2 1 .6 1.3.3.4.8.5 1.3.5h8.1c.4 0 .8.1 1.2.2.4.2.7.4 1 .7.3.3.5.6.6 1 .2.4.2.8.2 1.2 0 .4-.1.8-.2 1.2-.2.4-.4.7-.6 1-.3.3-.6.5-1 .7-.4.1-.8.2-1.3.2m24.2 0h-10.5v-1.2h10.5c.2 0 .5 0 .7-.1s.4-.2.6-.4c.2-.2.3-.4.4-.6s.1-.5.1-.7c0-.3-.1-.5-.1-.7-.1-.2-.2-.4-.4-.6-.2-.2-.4-.3-.6-.4s-.5-.1-.7-.1h-8.1c-.4 0-.8-.1-1.2-.2-.4-.2-.7-.4-1-.7-.3-.3-.5-.6-.7-1s-.2-.8-.2-1.2c0-.4.1-.8.2-1.2.2-.4.4-.7.7-1 .3-.3.6-.5 1-.7s.8-.3 1.2-.3h10.5v1.2h-10.5c-.3 0-.5 0-.7.2-.2.1-.4.2-.6.4-.2.1-.3.3-.4.6-.1.2-.2.5-.2.7 0 .5.2 1 .6 1.3.3.4.8.5 1.3.5h8.1c.4 0 .8.1 1.2.2.4.2.7.4 1 .7.3.3.5.6.6 1 .2.4.3.8.3 1.2 0 .4-.1.8-.3 1.2-.2.4-.4.7-.6 1-.3.3-.6.5-1 .7-.3.1-.7.2-1.2.2" fill="#fff"/></svg>
        </Link>

        <div className="sidebar-site">
          <p className="sidebar-site-name">Uplay Fitness</p>
        </div>

        <ul className="sidebar-menu">
          {sidebarMenuLink('Dashboard', '/')}
        </ul>

        <div className="sidebar-separator"></div>

        <span className="sidebar-label">Conteúdo</span>
        <ul className="sidebar-menu">
          {sidebarMenuLink('Posts', '/posts')}
          {sidebarMenuLink('Páginas', '/paginas')}
          {sidebarMenuLink('Unidades', '/unidades')}
        </ul>

        <div className="sidebar-separator"></div>

        <span className="sidebar-label">Site</span>
        <ul className="sidebar-menu">
          {sidebarMenuAction('Temas', handleConfigClick)}
          {sidebarMenuAction('Configurações', handleConfigClick)}
        </ul>
      </div>

      <div className="sidebar-bottom">
        <ul className="sidebar-menu">
          {sidebarMenuAction('Suporte', handleConfigClick)}
        </ul>
        <div className="sidebar-user hidden">
          <div className="sidebar-user-picture" style={{ backgroundImage: `url(${user_placeholder})` }}></div>

          <div className="sidebar-user-info">
            <span className="sidebar-user-name">{_.get(user, 'first_name')}</span>
            <Link className="sidebar-user-link" to="/logout">Sair</Link>
          </div>
        </div>
      </div>
    </nav>
  )
}

