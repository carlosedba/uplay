export const PRODUCTION = false
export const BASEPATH = (PRODUCTION) ? '/' : '/'

export const SERVER_ADDRESS = (PRODUCTION) ? 'https://devsinparis.com' : 'http://localhost/uplay/server/public_html'
export const MAIN_ENDPOINT = (PRODUCTION) ? `${SERVER_ADDRESS}` : `${SERVER_ADDRESS}/dev.php`
export const API_ENDPOINT = `${MAIN_ENDPOINT}/api/v1`

