import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import moment from 'moment'
import Modal from 'react-modal'

import './assets/css/boilerplate.css'
import './assets/css/buttons.css'
import './assets/css/colors.css'
import './assets/css/common.css'
import './assets/css/daypicker.css'
import './assets/css/login.css'

import MainRouter from './components/MainRouter'

import 'moment/locale/pt-br'

import store from './store'

moment.locale('pt-BR')

const render = Component => {
  ReactDOM.render(
    <Provider store={store}>
      <Component/>
    </Provider>
    , document.getElementById('root')
  )
}

Modal.setAppElement('#root')

render(MainRouter)

// webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept('@/components/MainRouter', () => {
    render(MainRouter)
  })
}