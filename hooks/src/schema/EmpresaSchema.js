export default {
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "S7Empresa",
  "required": [
    "setor",
    "nomeFantasia",
    "razaoSocial",
    "cnpj",
    "cep",
    "rua",
    "numero",
    "complemento",
    "bairro",
    "cidade",
    "estado",
    "pais",
    "telefone",
    "website",
    "faturamento",
    "ebitda",
    "porte",
    "origem",
    "formato",
    "gestao",
    "conselhoAdministracao",
    "headCount",
  ],
  "properties": {
    "setor": {
      "$id": "#/properties/setor",
      "type": "string",
      "minLength": 1
    },
    "nomeFantasia": {
      "$id": "#/properties/nomeFantasia",
      "type": "string",
      "minLength": 1
    },
    "razaoSocial": {
      "$id": "#/properties/razaoSocial",
      "type": "string",
      "minLength": 1
    },
    "cnpj": {
      "$id": "#/properties/cnpj",
      "type": "string",
      "minLength": 1
    },
    "cep": {
      "$id": "#/properties/cep",
      "type": "string",
      "minLength": 1
    },
    "rua": {
      "$id": "#/properties/rua",
      "type": "string",
      "minLength": 1
    },
    "numero": {
      "$id": "#/properties/numero",
      "type": "string",
      "minLength": 1
    },
    "complemento": {
      "$id": "#/properties/complemento",
      "type": "string"
    },
    "bairro": {
      "$id": "#/properties/bairro",
      "type": "string",
      "minLength": 1
    },
    "cidade": {
      "$id": "#/properties/cidade",
      "type": "string",
      "minLength": 1
    },
    "estado": {
      "$id": "#/properties/estado",
      "type": "string",
      "minLength": 1
    },
    "pais": {
      "$id": "#/properties/pais",
      "type": "string",
      "minLength": 1
    },
    "telefone": {
      "$id": "#/properties/telefone",
      "type": "string",
      "minLength": 1
    },
    "website": {
      "$id": "#/properties/website",
      "type": "string",
      "minLength": 1
    },
    "faturamento": {
      "$id": "#/properties/faturamento",
      "type": "object",
      "required": ["currency", "value"]
    },
    "ebitda": {
      "$id": "#/properties/ebitda",
      "type": "string",
      "minLength": 1
    },
    "porte": {
      "$id": "#/properties/porte",
      "type": "object"
    },
    "origem": {
      "$id": "#/properties/origem",
      "type": "object"
    },
    "formato": {
      "$id": "#/properties/formato",
      "type": "array",
      "minItems": 1,
      "items": {
        "$id": "#/properties/formato/items",
        "type": "object"
      }
    },
    "gestao": {
      "$id": "#/properties/gestao",
      "type": "object"
    },
    "conselhoAdministracao": {
      "$id": "#/properties/conselhoAdministracao",
      "type": "object"
    },
    "headCount": {
      "$id": "#/properties/headCount",
      "type": "string",
      "minLength": 1
    },
    "nomeConselheiros": {
      "$id": "#/properties/nomeConselheiros",
      "type": "array",
      "items": {
        "$id": "#/properties/nomeConselheiros/items",
        "type": "string",
        "minLength": 1
      }
    },
    "principaisContatos": {
      "$id": "#/properties/principaisContatos",
      "type": "array",
      "items": {
        "$id": "#/properties/principaisContatos/items",
        "type": "object"
      }
    },
    "regionais": {
      "$id": "#/properties/regionais",
      "type": "array",
      "items": {
        "$id": "#/properties/regionais/items",
        "type": "object"
      }
    }
  }
}