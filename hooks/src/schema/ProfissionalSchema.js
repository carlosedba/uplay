export default {
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "S7Profissional",
  "required": [
    "nome",
    "sobrenome",
    "sexo",
    "deficiente",
    "rg",
    "cpf",
    "dataNascimento",
    "estadoCivil",
    "email",
    "naturalidade",
    "estadoOrigem",
    "cidadeOrigem",
    "paisAtual",
    "estadoAtual",
    "cidadeAtual",
  ],
  "properties": {
    "nome": {
      "$id": "#/properties/nome",
      "type": "string",
      "minLength": 1
    },
    "sobrenome": {
      "$id": "#/properties/sobrenome",
      "type": "string",
      "minLength": 1
    },
    "sexo": {
      "$id": "#/properties/sexo",
      "type": "object"
    },
    "deficiente": {
      "$id": "#/properties/deficiente",
      "type": "object"
    },
    "rg": {
      "$id": "#/properties/rg",
      "type": "string",
      "minLength": 1
    },
    "cpf": {
      "$id": "#/properties/cpf",
      "type": "string",
      "minLength": 1
    },
    "telefone": {
      "$id": "#/properties/telefone",
      "type": "string",
      "minLength": 1
    },
    "dataNascimento": {
      "$id": "#/properties/dataNascimento",
      "type": "string",
      "minLength": 1
    },
    "estadoCivil": {
      "$id": "#/properties/estadoCivil",
      "type": "object"
    },
    "email": {
      "$id": "#/properties/email",
      "type": "string",
      "minLength": 1
    },
    "skype": {
      "$id": "#/properties/skype",
      "type": "string"
    },
    "linkedin": {
      "$id": "#/properties/linkedin",
      "type": "string"
    },
    "naturalidade": {
      "$id": "#/properties/naturalidade",
      "type": "object"
    },
    "estadoOrigem": {
      "$id": "#/properties/estadoOrigem",
      "type": "object"
    },
    "cidadeOrigem": {
      "$id": "#/properties/cidadeOrigem",
      "type": "object"
    },
    "paisAtual": {
      "$id": "#/properties/paisAtual",
      "type": "object"
    },
    "estadoAtual": {
      "$id": "#/properties/estadoAtual",
      "type": "object"
    },
    "cidadeAtual": {
      "$id": "#/properties/cidadeAtual",
      "type": "object"
    },
    "filhos": {
      "$id": "#/properties/filhos",
      "type": "array",
      "items": {
        "$id": "#/properties/filhos/items",
        "type": "string"
      }
    },
    "idiomas": {
      "$id": "#/properties/idiomas",
      "type": "array",
      "items": {
        "$id": "#/properties/idiomas/items",
        "type": "object"
      }
    }
  }
}