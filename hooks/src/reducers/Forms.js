import {
  NEW_FORM,
  UPDATE_FORM,
  UPDATE_FORM_RAW_ATTRIBUTE,
  UPDATE_FORM_COMPUTED_ATTRIBUTE,
  RESET_FORM,
  REMOVE_FORM,
} from '@/actionTypes'

import update from 'immutability-helper'

const INITIAL_STATE = {}

export default function(state = INITIAL_STATE, action) {
  let error
  
  switch (action.type) {
    case NEW_FORM:
      return update(state, {
        [action.payload.id]: { $set: action.payload.props || {
          raw: {},
          computed: {},
        } }
      })

    case UPDATE_FORM:
      return update(state, {
        [action.payload.id]: action.payload.props
      })

    case UPDATE_FORM_RAW_ATTRIBUTE:
      return update(state, {
        [action.payload.id]: {
          raw: {
            [action.payload.name]: action.payload.value
          }
        }
      })

    case UPDATE_FORM_COMPUTED_ATTRIBUTE:
      return update(state, {
        [action.payload.id]: {
          computed: {
            [action.payload.name]: action.payload.value
          }
        }
      })

    case RESET_FORM:
      return update(state, {
        [action.payload.id]: { $set: {} }
      })

    case REMOVE_FORM:
      return update(state, {
        $unset: [action.payload.id]
      })

    default:
      return state
  }
}

