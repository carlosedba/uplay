import {
	INPUT_FIND_SUGGESTIONS,
	INPUT_CLEAR_SUGGESTIONS
} from '@/actionTypes'

const INITIAL_STATE = {
	suggestions: 			{ items: [], error: null, loading: true },
}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
		case INPUT_FIND_SUGGESTIONS:
			return { ...state, suggestions: { items: [], error: null, loading: true } }

		case `${INPUT_FIND_SUGGESTIONS}_PENDING`:
			return { ...state, suggestions: { items: [], error: null, loading: true } }

		case `${INPUT_FIND_SUGGESTIONS}_FULFILLED`:
			return { ...state, suggestions: { items: action.payload.data, error: null, loading: false } }

		case `${INPUT_FIND_SUGGESTIONS}_REJECTED`:
			error = action.payload.data || { message: action.payload.message }
			return { ...state, suggestions: { items: [], error: error, loading: false } }


		case INPUT_CLEAR_SUGGESTIONS:
			return { ...state, suggestions: { items: [], error: null, loading: true } }

		default:
			return state
  }
}
