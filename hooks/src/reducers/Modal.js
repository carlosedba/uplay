import {
  ADD_MODAL, OPEN_MODAL, CLOSE_MODAL, UPDATE_MODAL, RESET_MODAL, REMOVE_MODAL,
} from '@/actionTypes'

import update from 'immutability-helper'

const INITIAL_STATE = {}

export default function(state = INITIAL_STATE, action) {
  let error

  switch (action.type) {
  case ADD_MODAL:
    return { ...state, [action.payload.name]: { isOpen: false } }

  case OPEN_MODAL:
    return update(state, {
      [action.payload.name]: { $merge: { isOpen: true } }
    })

  case CLOSE_MODAL:
    return update(state, {
      [action.payload.name]: { $merge: { isOpen: false } }
    })

  case UPDATE_MODAL:
    return update(state, {
      [action.payload.name]: action.payload.props
    })

  case RESET_MODAL:
    return update(state, {
      [action.payload.name]: { $set: { isOpen: false } }
    })

  case REMOVE_MODAL:
    return update(state, { $unset: [action.payload.name] })

  default:
    return state
  }
}

