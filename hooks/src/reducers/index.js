import { combineReducers } from 'redux'

import Session from './Session'
import Users from './Users'
import Input from './Input'
import Modal from './Modal'
import Forms from './Forms'

const reducer = combineReducers({
  Session,
  Users,
  Input,
  Modal,
  Forms,
})

export default reducer

