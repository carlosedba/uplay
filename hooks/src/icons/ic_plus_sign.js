import React from "react";

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      x="0"
      y="0"
      enableBackground="new 0 0 1024 1024"
      version="1.1"
      viewBox="0 0 1024 1024"
      xmlSpace="preserve"
    >
      <path d="M574 450L574 0 450 0 450 450 0 450 0 574 450 574 450 1024 574 1024 574 574 1024 574 1024 450z"></path>
    </svg>
  );
}

export default Icon;
