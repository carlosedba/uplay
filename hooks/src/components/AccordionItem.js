import React, { useState, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import gsap from 'gsap'

export default function AccordionItem(props) {
  const title = props.title
  const to = props.to

  const accordionItem = useRef()

  const [open, setOpen] = useState(false)

  function handleClick(event) {
    var target = accordionItem.current
    var content = target.querySelector('.ip-exp-block-content')
    var arrow = target.querySelector('.arrow')

    if (!open) {
      setOpen(true)

      gsap.set(target, { height: 'auto' })
      gsap.from(target, 0.16, { height: 55 })

      gsap.to(arrow, 0.25, { rotation: 180 })
    } else {
      setOpen(false)

      gsap.to(target, 0.16, { height: 55 })

      gsap.to(arrow, 0.25, { rotation: 0 })
    }

  }

  function renderAsLink() {
    return (
      <Link className="accordion-item" to={to}>
        <div className="accordion-item-header">
          <div className="left">
            <span className="accordion-item-title">{title}</span>
          </div>
          <div className="right"></div>
        </div>
      </Link>
    )
  }

  function render() {
    return (
      <div className="accordion-item" onClick={handleClick} ref={accordionItem}>
        <div className="accordion-item-header">
          <div className="left">
            <span className="accordion-item-title">{title}</span>
          </div>
          <div className="right">
            <div className="svg arrow">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129">
                <path
                  d="M121.3 34.6c-1.6-1.6-4.2-1.6-5.8 0l-51 51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8 0-1.6 1.6-1.6 4.2 0 5.8l53.9 53.9c.8.8 1.8 1.2 2.9 1.2 1 0 2.1-.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2.1-5.8z"/>
              </svg>
            </div>
          </div>
        </div>
        <div className="accordion-item-content">
          {props.children}
        </div>
      </div>
    )
  }

  if (to) return renderAsLink()
  else return render()
}