import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'

import AuthorizedRoute from '@/components/AuthorizedRoute'
import AuthorizedLayout from '@/components/AuthorizedLayout'

import Login from '@/pages/Login'

import { BASEPATH } from '@/globals'

export default function Main(props) {
  const auth = useSelector(state => state.Session.auth)

  return (
    <div className="app">
        <Router>
          <Switch>
            <Route path="/login" component={Login}/>
            <AuthorizedRoute path="/" component={AuthorizedLayout}/>
          </Switch>
        </Router>
    </div>
  )
}

