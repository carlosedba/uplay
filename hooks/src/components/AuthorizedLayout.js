import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  Switch,
  Route
} from 'react-router-dom'
import { TransitionGroup, CSSTransition } from 'react-transition-group'

import AuthorizedRoute from '@/components/AuthorizedRoute'

import Navbar from '@/components/Navbar'
import Sidebar from '@/components/Sidebar'

import Dashboard from '@/pages/Dashboard'
import Dev from '@/pages/Dev'
import Logout from '@/pages/Logout'

import Posts from '@/pages/Post/Posts'
import Post from '@/pages/Post/Post'
import NewPost from '@/pages/Post/NewPost'
import EditPost from '@/pages/Post/EditPost'

import { addModal } from '@/actions/Modal'

export default function AuthorizedLayout() {
  const authenticated = useSelector(state => state.Session.auth.authenticated)

  const dispatch = useDispatch()

  useEffect(() => {
    console.log('log > AuthorizedLayout initialized!')

    dispatch(addModal('ModalConfig'))
  })

  return (
    <div className="default-layout">
      <Sidebar/>

      <Switch>
        <Route path="/logout" component={Logout}/>
        <Route path="/dev" component={Dev}/>

        <Route path="/posts/new" component={NewPost}/>
        <Route path="/posts/:id/editar" component={EditPost}/>
        <Route path="/posts/:id" component={Post}/>
        <Route path="/posts" component={Posts}/>

        <Route path="/" component={Dashboard}/>
      </Switch>
    </div>
  )
}

