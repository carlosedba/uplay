import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'
import { useTable, usePagination } from 'react-table'

export default function DataTable(props) {
  const columns = props.columns
  const data = props.data
  const customCellRenderers = props.customCellRenderers || []
  const rowsHover = props.rowsHover || false
  const onRowClick = props.onRowClick

  const {
    headerGroups,
    prepareRow,
    page,

    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable({
    columns,
    data,
    initialState: { pageIndex: 0 },
  }, usePagination)

  function getCustomCellRenderer(header) {
    for (const item of customCellRenderers) {
      if (item.header === header) return item
    }

    return null
  }

  function handleRowClick(event, row) {
    if (onRowClick) onRowClick(row)
  }

  return (
    <div className="data-table-wrapper">
      <div className="data-table">

        <div className="data-table-header">
          {headerGroups.map((headerGroup, i) => (
            <div key={i} className="data-table-header-row" {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map((column, i) => (
                <div key={i} className="data-table-header-col" {...column.getHeaderProps()} style={{ width: column.width }}>
                  {column.render('Header')}
                </div>
              ))}
            </div>
          ))}
        </div>

        <div className="data-table-content">
          {page.map(
            (row, i) => {
              prepareRow(row);

              return (
                <div key={i} className={classNames('data-table-row', {
                  'has-hover': rowsHover
                })} {...row.getRowProps()}>
                  {row.cells.map((cell, i) => {
                    let renderer = getCustomCellRenderer(cell.column.Header)

                    if (renderer) return renderer.render(cell, i)
                    else return (
                      <div key={i} className="data-widget-cell-wrapper" onClick={(event) => handleRowClick(event, row)}>
                        <div className="data-widget-cell" {...cell.getCellProps()} style={{ width: cell.column.width }}>
                          {cell.render('Cell')}
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
          )}
        </div>
      </div>

      <div className="pagination-wrapper">

        <div className="pagination-input">
          Ir para página:
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={e => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0
              gotoPage(page)
            }}
            style={{ width: '60px' }}/>
          <select
            value={pageSize}
            onChange={e => {
              setPageSize(Number(e.target.value))
            }}>
            {[10, 20, 30, 40, 50].map(pageSize => (
              <option key={pageSize} value={pageSize}>
                Mostrar {pageSize}
              </option>
            ))}
          </select>
        </div>

        <div className="pagination-info">
          <span>
            Página{' '}
            <strong>
              {pageIndex + 1} de {pageOptions.length}
            </strong>{' '}
          </span>
        </div>

        <div className="pagination">
          <button className="btn btn-seven btn-orange" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {'<<'}
          </button>{' '}
          <button className="btn btn-seven btn-orange" onClick={() => previousPage()} disabled={!canPreviousPage}>
            {'<'}
          </button>{' '}
          <button className="btn btn-seven btn-orange" onClick={() => nextPage()} disabled={!canNextPage}>
            {'>'}
          </button>{' '}
          <button className="btn btn-seven btn-orange" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
            {'>>'}
          </button>{' '}
        </div>
      </div>
    </div>
  )
}