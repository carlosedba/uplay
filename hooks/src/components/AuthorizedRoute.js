import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Route, Redirect } from 'react-router-dom'
import store from 'store'

import { validateToken, renewToken } from '@/actions/Session'

export default function AuthorizedRoute(props) {
  const Component = props.component

  const pending = useSelector(state => state.Session.auth.pending)
  const authenticated = useSelector(state => state.Session.auth.authenticated)

  const [ready, setReady] = useState(false)

  const dispatch = useDispatch()

  useEffect(() => {
    console.log('log > AuthorizedRoute > useEffect called!', props)

    renewTokenIfExists()
  }, [])

  function renewTokenIfExists() {
    console.log('log > AuthorizedRoute > renewTokenIfExists called!', Component.name)

    let token = store.get('token')

    if (!ready && token) {
      dispatch(validateToken(token))
        .then(() => {
          setReady(true)
        })
        .catch((err) => {
          console.error(err)
          setReady(true)
        })
    } else {
      setReady(true)
    }
  }

  return (<Route
    {...props}
    component={null}
    render={(routeProps) => {
      if (pending || !ready) {
        return (<div>Pending</div>)
      } else {
        if (authenticated) {
          return (<Component/>)
        } else {
          return (<Redirect to="/login"/>)
        }
      }
    }}
  />)
}

