import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useLocation } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'
import { useTable } from 'react-table'
import IcPlusSign from '@/icons/ic_plus_sign'

export default function DataWidget(props) {
  const title = props.title
  const columns = props.columns
  const data = props.data
  const customCellRenderers = props.customCellRenderers || []
  const rowsHover = props.rowsHover || false
  const size = props.size
  const showCreateButton = props.showCreateButton || false
  const onRowClick = props.onRowClick

  const {
    headerGroups,
    rows,
    prepareRow
  } = useTable({ columns, data })

  function getCustomCellRenderer(header) {
    for (const item of customCellRenderers) {
      if (item.header === header) return item
    }

    return null
  }

  function handleRowClick(row) {
    if (onRowClick) onRowClick(row)
  }

  return (
    <div className={classNames('data-widget', 'data-widget-two', {
      [size]: size
    })}>
      <div className="data-widget-head">

        <div className="data-widget-header">
          <div className="data-widget-header-left">
            <span className="data-widget-title">{title}</span>
          </div>
          <div className="data-widget-header-right">
            {(showCreateButton) ? (
              <button className="btn btn-seven btn-orange">
                <div className="svg btn-icon"><IcPlusSign/></div>
              </button>
            ) : ''}
          </div>
        </div>

        {headerGroups.map((headerGroup, i) => (
          <div key={i} className="data-widget-row" {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column, i) => (
              <div key={i} className="data-widget-col" {...column.getHeaderProps()} style={{ width: column.width }}>
                {column.render('Header')}
              </div>
            ))}
          </div>
        ))}
      </div>

      <div className="data-widget-content">
        {(rows.length === 0) ? (
          <div className="data-widget-empty">
            <span>Nada para mostrar</span>
          </div>
        ) : ''}
        {rows.map(
          (row, i) => {
            prepareRow(row);

            return (
              <div key={i} className={classNames('data-widget-row', {
                'has-hover': rowsHover
              })} {...row.getRowProps()} onClick={() => handleRowClick(row)}>
                {row.cells.map((cell, i) => {
                  let render = getCustomCellRenderer(cell.column.Header)

                  if (render) return render(cell, i)
                  else return (
                    <div key={i} className="data-widget-cell-wrapper">
                      <div className="data-widget-cell" {...cell.getCellProps()} style={{ width: cell.column.width }}>
                        {cell.render('Cell')}
                      </div>
                    </div>
                  )
                })}
              </div>
            )}
        )}
      </div>
    </div>
  )
}