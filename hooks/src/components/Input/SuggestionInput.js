import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import update from 'immutability-helper'
import classNames from 'classnames'

import * as Forms from '@/actions/Form'

import api from '@/api'

export default function SuggestionInput(props) {
  const collection = props.collection
  const attribute = props.attribute
  const formId = props.formId
  const label = props.label
  const classes = props.classes || ''
  const required = props.required || false
  const initialValue = props.initialValue
  const attributeToQuery = props.attributeToQuery

  const forms = useSelector(state => state.Forms)

  const input = React.useRef()

  const timeToWait = 1500
  let timeToWaitTimeout = null

  const [loading, setLoading] = useState(false)
  const [suggestions, setSuggestions] = useState([])
  const [userInput, setUserInput] = useState('')
  const [focusedSuggestion, setFocusedSuggestion] = useState(-1)
  const [suggestionSelected, setSuggestionSelected] = useState(false)

  const dispatch = useDispatch()

  let value = ''

  useEffect(() => {
    setInitialValue()
  }, [initialValue])

  function initFormAttribute() {
    if (forms[formId]) {
      if (forms[formId]['raw'][attribute] === undefined) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: ''
        }))
      }
    }
  }

  function setInitialValue() {
    if (formId && initialValue) {

      console.log('initialValue', initialValue)

      if (typeof initialValue === 'object') {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: initialValue[attributeToQuery]
        }))

        dispatch(Forms.updateFormComputedAttribute(formId, attribute, {
          $set: initialValue._id
        }))
      }

      if (typeof initialValue === 'string') {
        api.get(`/${collection}/${initialValue}`)
          .then((response) => {
            console.log('response', response)
            const data = response.data

            if (data) {
              let inputValue = data._id
              let inputText = data[attributeToQuery]

              dispatch(Forms.updateFormRawAttribute(formId, attribute, {
                $set: inputText
              }))

              dispatch(Forms.updateFormComputedAttribute(formId, attribute, {
                $set: inputValue
              }))
            }
          })
          .catch((err) => {
            setLoading(false)
          })
      }

      setSuggestionSelected(true)
    }
  }

  function updateRenderedValue() {
    if (forms[formId]) {
      if (forms[formId]['raw'][attribute] !== undefined) {
        value = forms[formId]['raw'][attribute]
      }
    }
  }

  function findSuggestions(str) {
    api.get(`/${collection}`, {
      search: str
    })
      .then((response) => {
        const data = response.data

        if (data) {
          setSuggestions(data)
          setTImeout(() => { setLoading(false) }, 500)
        }
      })
      .catch((err) => {
        setLoading(false)
      })
  }

  function userIsTyping(str = '') {
    setLoading(true)
    setSuggestions([])
    setSuggestionSelected(false)
  }

  function userFinishedTyping(str = '') {
    if (str !== '') findSuggestions(str)
  }

  function handleInputChange(event) {
    if (event) {
      event.persist()

      const inputText = event.target.value

      if (inputText !== null) {
        if (inputText === '') {
          setSuggestions([])
          setLoading(false)
        }

        userIsTyping(inputText)

        if (timeToWaitTimeout !== undefined && timeToWaitTimeout !== null) clearTimeout(timeToWaitTimeout)
        timeToWaitTimeout = setTimeout(userFinishedTyping.bind(this, inputText), timeToWait)
      }

      setUserInput(inputText)
      //setInputText(inputText)
      //setInputValue(inputText)

      dispatch(Forms.updateFormRawAttribute(formId, attribute, {
        $set: inputText
      }))
    }
  }

  function handleInputKeyDown(event) {
    switch (event.keyCode) {
      case 13:
        if (focusedSuggestion > -1) {
          const suggestion = suggestions[focusedSuggestion]

          //setInputText(suggestion[attributeToQuery])
          //setInputValue(suggestion._id)
          setSuggestionSelected(true)

          dispatch(Forms.updateFormRawAttribute(formId, attribute, {
            $set: suggestion[attributeToQuery]
          }))

          dispatch(Forms.updateFormComputedAttribute(formId, attribute, {
            $set: suggestion._id
          }))
        }
        break

      case 38:
        if (suggestions.length > 0) {
          if (focusedSuggestion > -1) {
            setFocusedSuggestion(focusedSuggestion - 1)
          }
        }
        break

      case 40:
        if (suggestions.length > 0) {
          if (focusedSuggestion < suggestions.length - 1) {
            setFocusedSuggestion(focusedSuggestion + 1)
          }
        }
        break
    }
  }

  function handleSuggestionClick(item, event) {
    let suggestionText = ''

    if (item[attributeToQuery]) suggestionText = item[attributeToQuery]

    //setInputText(suggestionText)
    //setInputValue(item.id)
    setSuggestionSelected(true)

    dispatch(Forms.updateFormRawAttribute(formId, attribute, {
      $set: suggestionText
    }))

    dispatch(Forms.updateFormComputedAttribute(formId, attribute, {
      $set: item._id
    }))
  }

  function renderSuggestion(item, i) {
    let suggestionText = ''

    if (item[attributeToQuery]) suggestionText = item[attributeToQuery]

    return (
      <li key={item._id} tabIndex="0" className={classNames({
        'selected': focusedSuggestion === i
      })} onClick={handleSuggestionClick.bind(null, item)}>
        {suggestionText}
      </li>
    )

    /*
    suggestionText = suggestionText.toLowerCase()

    let inputTextLowerCase = inputText.toLowerCase()

    let textEnd = suggestionText.split(inputTextLowerCase)[1]
    let textStart = suggestionText.substring(0, suggestionText.indexOf(textEnd))

    let isHidden = false

    if (suggestionText.indexOf(textEnd) <= 0) isHidden = true

    return (
      <li key={item._id} tabIndex="0" className={classNames({
        'selected': focusedSuggestion === i,
        'hidden': isHidden
      })} onClick={handleSuggestionClick.bind(null, item)}>
        {textStart}
        <span className="bold">{textEnd}</span>
      </li>
    )
     */
  }

  initFormAttribute()
  updateRenderedValue()

  return (
    <div className={classNames('input input-style-2', classes)}>
      <label>{label}:{(required) ? (<span className="orange"> *</span>) : ''}</label>
      <div className="input-container">
        <input
          type="text"
          tabIndex="0"
          value={value}
          onChange={handleInputChange}
          onKeyDown={handleInputKeyDown}
          ref={input}
        />
      </div>

      {value !== '' && !suggestionSelected && (
        <ul className={classNames('suggestions', {
          'is-loading': loading
        })}>
          <div className="spinner"></div>
          {(suggestions.length > 0) && suggestions.map(renderSuggestion)}
        </ul>
      )}
    </div>
  )
}

