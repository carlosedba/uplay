import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'
import { utilsBr } from 'js-brasil'
import MaskedInput from 'react-text-mask'

import * as Forms from '@/actions/Form'

export default function TextInput(props) {
  const formId = props.formId
  const attribute = props.attribute
  const initialValue =  props.initialValue
  const mask = props.mask
  const label = props.label
  const classes = props.classes || ''
  const required = props.required || false
  const submitted = props.submitted || false
  const onChange = props.onChange
  const onFocus = props.onFocus

  const forms = useSelector(state => state.Forms)

  const [inputValue, setInputValue] = useState('')

  const dispatch = useDispatch()

  let textMask, value

  useEffect(() => {
    setInitialValue()
  }, [initialValue])

  function initFormAttribute() {
    if (forms[formId]) {
      if (forms[formId]['raw'][attribute] === undefined) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: ''
        }))
      }
    }
  }

  function updateRenderedValue() {
    if (forms[formId]) {
      if (forms[formId]['raw'][attribute] !== undefined) {
        value = forms[formId]['raw'][attribute]
      }
    }
  }

  function setInitialValue() {
    if (formId && initialValue) {
      let value = initialValue

      if (typeof initialValue === 'number') value = value.toString()

      dispatch(Forms.updateFormRawAttribute(formId, attribute, {
        $set: value
      }))
    }
  }

  function handleInputChange(event) {
    const inputText = event.target.value

    if (inputText !== undefined && inputText !== null) {
      dispatch(Forms.updateFormRawAttribute(formId, attribute, {
        $set: inputText
      }))

      if (onChange) onChange({
        attribute: attribute,
        value: inputText
      })
    }
  }

  function handleInputFocus(event) {
    if (onFocus) onFocus({
      attribute: attribute,
      value: value
    })
  }

  function renderMaskedInput(value) {
    let textMask

    if (typeof mask === "string") {
      textMask = utilsBr.MASKS[mask].textMask

      return (<MaskedInput mask={textMask} value={value || ''} onChange={handleInputChange} onFocus={handleInputFocus}/>)
    } else {
      return (<MaskedInput mask={mask} value={value || ''} onChange={handleInputChange} onFocus={handleInputFocus}/>)
    }
  }

  initFormAttribute()
  updateRenderedValue()

  return (
    <div className={classNames('input', classes)}>
      <label>{label}:{(required) ? (<span className="orange"> *</span>) : ''}</label>
      <div className="input-container">
        {(mask)
          ? renderMaskedInput(value)
          : (<input type="text" value={value || ''} onChange={handleInputChange} onFocus={handleInputFocus}/>)
        }
      </div>
    </div>
  )
}

