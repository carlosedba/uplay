import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'
import { utilsBr } from 'js-brasil'
import MaskedInput from 'react-text-mask'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import MomentLocaleUtils, { formatDate, parseDate } from 'react-day-picker/moment'

import * as Forms from '@/actions/Form'

export default function DateInput(props) {
  const formId = props.formId
  const attribute = props.attribute
  const initialValue = props.initialValue
  const label = props.label
  const classes = props.classes || ''
  const required = props.required || false

  const forms = useSelector(state => state.Forms)

  const [inputValue, setInputValue] = useState('')

  const dispatch = useDispatch()

  useEffect(() => {
    if (initialValue) {
      dispatch(Forms.updateFormRawAttribute(formId, attribute, {
        $set: initialValue
      }))

      if (onChange) onChange({
        attribute: attribute,
        value: initialValue
      })
    }
  }, [])

  function handleInputChange(event) {
    const inputText = event.target.value

    if (inputText !== undefined && inputText !== null) {
      dispatch(Forms.updateFormRawAttribute(formId, attribute, {
        $set: inputText
      }))

      if (onChange) onChange({
        attribute: attribute,
        value: inputText
      })
    }
  }

  let textMask, value

  if (forms[formId]) {
    if (forms[formId]['raw'][attribute] === undefined) {
      dispatch(Forms.updateFormRawAttribute(formId, attribute, {
        $set: ''
      }))
    }
  }

  if (forms[formId]) value = forms[formId]['raw'][attribute]
    
  return (
    <div className={classNames('input', classes)}>
      <label>{label}:{(required) ? (<span className="orange"> *</span>) : ''}</label>
      <div className="input-container">
        <DayPickerInput
          dayPickerProps={{
            localeUtils: MomentLocaleUtils,
            locale: 'pt-br'
          }}
          formatDate={formatDate}
          parseDate={parseDate}
          placeholder=""
          onChange={handleInputChange}/>
      </div>
    </div>
  )
}

