import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'


export default function Form(props) {
  const id = props.id
  const onSubmit = props.onSubmit

  function handleSubmit(event) {    
    event.preventDefault()

    console.log(props)


    if (onSubmit) onSubmit(props)
  }

  return (
    <form className="form" onSubmit={handleSubmit}>
      {props.children}
    </form>
  )
}

