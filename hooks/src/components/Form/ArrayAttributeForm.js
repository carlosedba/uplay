import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import update from 'immutability-helper'

import * as Forms from '@/actions/Form'

export default function ArrayAttributeForm(props) {
  const formId = props.formId
  const parentFormId = props.parentFormId
  const attribute = props.attribute
  const modalName = props.modalName
  const mode = props.mode
  const onSubmit = props.onSubmit

  const forms = useSelector(state => state.Forms)
  const modal = useSelector(state => state.Modal)

  const dispatch = useDispatch()

  function handleSubmit(event) {
    event.preventDefault()

    const sourceForm = forms[formId]
    const sourceFormRaw = sourceForm['raw']
    const sourceFormComputed = sourceForm['computed']

    const targetForm = forms[parentFormId]
    const targetFormRaw = targetForm['raw']
    const targetFormComputed = targetForm['computed']

    if (!mode || mode === 'create') {

      if (sourceFormRaw[attribute] && !targetFormRaw[attribute]) {
        dispatch(Forms.updateFormRawAttribute(parentFormId, attribute, {
          $set: []
        }))
      }

      if (sourceFormComputed[attribute] && !targetFormComputed[attribute]) {
        dispatch(Forms.updateFormComputedAttribute(parentFormId, attribute, {
          $set: []
        }))
      }

      if (sourceFormRaw[attribute]) {
        dispatch(Forms.updateFormRawAttribute(parentFormId, attribute, {
          $push: [sourceFormRaw[attribute]]
        }))
      }

      if (sourceFormComputed[attribute]) {
        dispatch(Forms.updateFormComputedAttribute(parentFormId, attribute, {
          $push: [sourceFormComputed[attribute]]
        }))
      }

    } else if (mode === 'update') {
      dispatch(Forms.updateFormRawAttribute(parentFormId, attribute, {
        [modal[modalName].index]: { $set: sourceFormRaw[attribute] }
      }))

      dispatch(Forms.updateFormComputedAttribute(parentFormId, attribute, {
        [modal[modalName].index]: { $set: sourceFormComputed[attribute] }
      }))
    }

    dispatch(Forms.updateFormRawAttribute(formId, attribute, {
      $set: ""
    }))

    dispatch(Forms.updateFormComputedAttribute(formId, attribute, {
      $set: null
    }))

    onSubmit()
  }

  return (
    <form onSubmit={handleSubmit}>
      {props.children}
    </form>
  )
}