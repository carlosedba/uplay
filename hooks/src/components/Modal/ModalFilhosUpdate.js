import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { closeModal } from '@/actions/Modal'

import * as Forms from '@/actions/Form'

import Modal from '@/components/Modal'
import ArrayAttributeForm from '@/components/Form/ArrayAttributeForm'
import TextInput from '@/components/Input/TextInput'

import IcClose from '@/icons/ic_close'

import IdGenerator from "@/utils/IdGenerator"

const formId = IdGenerator.default()

export default function ModalFilhosUpdate(props) {
  const parentFormId = props.parentFormId
  const attribute = props.attribute

  const modal = useSelector(state => state.Modal)
  const forms = useSelector(state => state.Forms)

  const name = 'ModalFilhosUpdate'
  const title = 'Filhos'

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(Forms.newForm(formId))
  }, [])

  function getRawValue() {
    let currentModal = modal[name]
    let parentForm = forms[parentFormId]

    if (currentModal && parentForm) {
      let value = ''
      let arr = parentForm['raw'][attribute]

      if (Array.isArray(arr)) {
        if (arr[currentModal.index]) {
          value = arr[currentModal.index]
        }
      }

      return value
    }

    return null
  }

  function getComputedValue() {
    let currentModal = modal[name]
    let parentForm = forms[parentFormId]

    if (currentModal && parentForm) {
      let value = ''
      let arr = parentForm['computed'][attribute]

      if (Array.isArray(arr)) {
        if (arr[currentModal.index]) {
          value = arr[currentModal.index]
        }
      }

      return value
    }

    return null
  }

  function handleBtnCloseClick(event) {
    dispatch(closeModal(name))
  }

  function handleSubmit(event) {
    dispatch(closeModal(name))
  }

  return (
    <Modal name={name} contentLabel={title}>
      <div className="modal">
        <div className="modal-header">
          <div className="modal-header-left">
            <div className="modal-titles">
              <span className="modal-hat">{title}</span>
              <span className="modal-title">Editar filho</span>
            </div>
          </div>
          <div className="modal-header-right">
            <button className="btn btn-close" onClick={handleBtnCloseClick}>
              <div className="svg icon">
                <IcClose/>
              </div>
            </button>
          </div>
        </div>
        <div className="modal-content">
          <ArrayAttributeForm formId={formId} parentFormId={parentFormId} attribute={attribute} mode="update" modalName={name} onSubmit={handleSubmit}>
            <div className="inputs input-style-2">
              <TextInput formId={formId} attribute={attribute} classes="w3" label="Nome" required={true} initialValue={getRawValue()}/>
              <button type="submit" className="btn btn-six btn-bright-blue">Salvar</button>
            </div>
          </ArrayAttributeForm>
        </div>
      </div>
    </Modal>
  )
}

