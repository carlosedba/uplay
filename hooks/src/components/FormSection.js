import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { openModal, updateModal } from '@/actions/Modal'
import { removeItemFromNetQueue } from '@/actions/NetQueue'

import IcPencil from '@/icons/ic_pencil'
import IcTrashcan from '@/icons/ic_trashcan'

export default function FormSection(props) {
  const model = props.model
  const action = props.action
  const title = props.title
  const message = props.message
  const Icon = props.icon
  const attributeToDisplay = props.attributeToDisplay
  const customActions = props.customActions
  const onCreateModalName = props.onCreateModalName
  const onUpdateModalName = props.onUpdateModalName

  const forms = useSelector(state => state.Forms)
  const modal = useSelector(state => state.Modal)
  const netQueueItems = useSelector(state => state.NetQueue.items)

  const dispatch = useDispatch()

  function handleNewItemClick(event) {
    dispatch(openModal(onCreateModalName))
  }

  function handleUpdateItemClick(formId, event) {
    dispatch(updateModal(onUpdateModalName, {
      $merge: { formId: formId }
    }))
    
    dispatch(openModal(onUpdateModalName))
  }

  function handleRemoveItemClick(formId, event) {
    dispatch(removeItemFromNetQueue(formId))
  }

  function handleCustomActionClick(formId, modalName, event) {
    dispatch(updateModal(modalName, {
      $merge: { formId: formId }
    }))
    
    dispatch(openModal(modalName))
  }

  function renderSectionContent(mutations) {
    const payloads = netQueueItems.filter((item, i) => {
      if (item.model === model && item.action == action) {
        return true
      }
    })

    return (
      <div className="form-section-content">
        {payloads.map((payload, i) => {
          return (
            <div key={i} className="form-item">
              <div className="form-item-left">
                <span className="form-item-name">{payload.object.raw[attributeToDisplay]}</span>
              </div>
              <div className="form-item-right">
                <div className="actions">
                  {(customActions) && customActions.map((action, i) => {
                    const Icon = action.icon

                    return (
                      <button className="svg action" onClick={handleCustomActionClick.bind(null, payload.id, action.modal)}>
                        <Icon/>
                      </button>
                    )
                  })}
                  <button className="svg action" onClick={handleUpdateItemClick.bind(null, payload.id)}>
                    <IcPencil/>
                  </button>
                  <button className="svg action" onClick={handleRemoveItemClick.bind(null, payload.id)}>
                    <IcTrashcan/>
                  </button>
                </div>
              </div>
            </div>
          )
        })}
      </div>
    )
  }

  return (
    <div className="form-section">
      <p className="form-section-title">{title}</p>
      <button className="form-new-item" onClick={handleNewItemClick}>
        {(Icon) && (
          <div className="svg icon">
            <Icon/>
          </div>
        )}
        {message}
      </button>
      
      {renderSectionContent()}

      {props.children}
    </div>
  )
}

