import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

export default function Accordion(props) {
  return (
    <div className="accordion">
      {props.children}
    </div>
  )
}