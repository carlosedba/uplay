import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'
import classNames from 'classnames'
import ReactSelect from 'react-select'

import * as Forms from '@/actions/Form'

export default function Select(props) {
  const formId = props.formId
  const attribute = props.attribute
  const initialValue = props.initialValue
  const options = props.options
  const size = props.size
  const label = props.label
  const classes = props.classes || ''
  const required = props.required || false
  const styles = props.styles
  const onChange = props.onChange

  const [selectedOption, setSelectedOption] = useState(null)

  const forms = useSelector(state => state.Forms)

  const dispatch = useDispatch()

  let value, reactSelectOptions, customStyles

  useEffect(() => {
    console.log('log > Select > useEffect called!')

    setInitialValue()

  }, [initialValue])

  function initFormAttribute() {
    if (forms[formId]) {
      if (forms[formId]['raw'][attribute] === undefined) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: ''
        }))
      }
    }
  }

  function updateRenderedValue() {
    if (forms[formId]) {
      if (forms[formId]['raw'][attribute] !== undefined) {
        value = forms[formId]['raw'][attribute]
      }
    }
  }

  function isOption(obj) {
    if (obj.label && obj.value) return true
    return false
  }

  function isOptionsArray(arr) {
    for (const item of arr) {
      if (!isOption(item)) return false
      return true
    }
  }

  function isStringArray(arr) {
    for (const item of arr) {
      if (typeof item !== 'string') return false
      return true
    }
  }

  function setInitialValue() {
    console.log('log > Select > setInitialValue called!', initialValue)
    if (formId && initialValue) {
      if (typeof initialValue === 'object' && isOption(initialValue)) {
        dispatch(Forms.updateFormRawAttribute(formId, attribute, {
          $set: initialValue
        }))
      }

      if (typeof initialValue === 'string') {
        console.log('log > Select > setInitialValue', initialValue)
        let option = getOptionByValue(initialValue)

        if (option) {
          dispatch(Forms.updateFormRawAttribute(formId, attribute, {
            $set: option
          }))
        }
      }

      if (Array.isArray(initialValue)) {
        if (isOptionsArray(initialValue)) {
          dispatch(Forms.updateFormRawAttribute(formId, attribute, {
            $set: initialValue
          }))
        }

        if (isStringArray(initialValue)) {
          let options = generateOptions(initialValue)

          dispatch(Forms.updateFormRawAttribute(formId, attribute, {
            $set: options
          }))
        }
      }
    }
  }

  function getOptionByValue(value) {
    for (let option of options) {
      if (option.value === value) return option
    }

    return null
  }

  function generateOptions(options) {
    const arr = []

    for (let option of options) {
      arr.push({ value: option, label: option })
    }

    return arr
  }

  function getSize() {
    switch (size) {
      case 'w1':
        return 100

      case 'w1-q1':
        return 125

      case 'w1-q2':
        return 150

      case 'w1-q3':
        return 175


      case 'w2':
        return 200

      case 'w2-q1':
        return 225

      case 'w2-q2':
        return 250

      case 'w2-q3':
        return 275


      case 'w3':
        return 300

      case 'w3-q1':
        return 325

      case 'w3-q2':
        return 350

      case 'w3-q3':
        return 375


      case 'w4':
        return 400

      case 'w4-q1':
        return 425

      case 'w4-q2':
        return 450

      case 'w4-q3':
        return 475

      default:
        return 150
    }
  }

  function handleChange(selectedOption) {
    if (selectedOption) {
      dispatch(Forms.updateFormRawAttribute(formId, attribute, {
        $set: selectedOption
      }))

      if (onChange) onChange({
        attribute: attribute,
        value: selectedOption.value
      })
    }
  }

  initFormAttribute()
  updateRenderedValue()

  if (typeof options[0] === 'string') {
    reactSelectOptions = generateOptions(options)
  } else {
    reactSelectOptions = options
  }

  let controlStyles = {
    borderRadius: '6px',
    borderColor: 'rgb(229, 229, 234)',
    backgroundColor: '#fff',
  }

  if (size) {
    customStyles = Object.assign({}, {
      control: (provided, state) => ({
        ...provided,
        ...controlStyles,
        width: getSize(),
      })
    }, styles) 
  } else {
    customStyles = {
      control: (provided, state) => ({
        ...provided,
        ...controlStyles,
        width: getSize(),
      })
    }
  }

  return (
    <div className={classNames('input', classes)}>
      <label>{label}:{(required) ? (<span className="orange"> *</span>) : ''}</label>
      <ReactSelect
        {...props}
        styles={customStyles}
        placeholder="Selecione..."
        value={value}
        onChange={handleChange}
        options={reactSelectOptions}/>
    </div>
  )
}

