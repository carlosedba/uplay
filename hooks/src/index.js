import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import browserStore from 'store'
import moment from 'moment'
import Modal from 'react-modal'

import MainRouter from './components/MainRouter'

import store from './store'

import './assets/css/boilerplate.global.css'
import './assets/css/common.global.css'
import './assets/css/s-inputs.global.css'
import './assets/css/daypicker.global.css'
import './assets/css/buttons.global.css'
import './assets/css/data.global.css'
import './assets/css/s-widget.global.css'
import './assets/css/login.global.css'
//import './assets/css/table.global.css'
import './assets/css/ctable.global.css'

import 'moment/locale/pt-br'

moment.locale('pt-BR')

const render = Component => {
  ReactDOM.render(
    <Provider store={store}>
      <Component/>
    </Provider>
    , document.getElementById('root')
  )
}

Modal.setAppElement('#root')

render(MainRouter)

// webpack Hot Module Replacement API
if (module.hot) {
  module.hot.accept('@/components/MainRouter', () => {
    render(MainRouter)
  })
}