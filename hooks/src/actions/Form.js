import axios from 'axios'
import { S7_API_ENDPOINT } from '@/globals'
import * as types from '@/actionTypes'

export function newForm(id, props) { 
  return {
    type: types.NEW_FORM,
    payload: {
      id: id,
      props: props
    }
  }
}

export function updateForm(id, props) { 
  return {
    type: types.UPDATE_FORM,
    payload: {
      id: id,
      props: props,
    }
  }
}

export function updateFormRawAttribute(id, name, value) { 
  return {
    type: types.UPDATE_FORM_RAW_ATTRIBUTE,
    payload: {
      id: id,
      name: name,
      value: value,
    }
  }
}

export function updateFormComputedAttribute(id, name, value) { 
  return {
    type: types.UPDATE_FORM_COMPUTED_ATTRIBUTE,
    payload: {
      id: id,
      name: name,
      value: value,
    }
  }
}

export function resetForm(id) {
  return {
    type: types.RESET_FORM,
    payload: {
      id: id,
    }
  }
}

export function removeForm(id) { 
  return {
    type: types.REMOVE_FORM,
    payload: {
      id: id,
    }
  }
}

