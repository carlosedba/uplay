import Ajv from 'ajv'

import S7EmpresaSchema from '@/schema/EmpresaSchema'
import ProfissionalSchema from '@/schema/ProfissionalSchema'
import FormacaoSchema from '@/schema/FormacaoSchema'
import CargoSchema from '@/schema/CargoSchema'


const schemas = {
  S7Empresa: S7EmpresaSchema,
  S7Profissional: ProfissionalSchema,
  S7Formacao: FormacaoSchema,
  S7Cargo: CargoSchema,
}

export default {
  getSchema(modelName) {
    if (schemas[modelName]) return schemas[modelName]

    return null
  },

  validate(formProps) {
    return new Promise((resolve, reject) => {
      const form = FormHelper.getById(formProps.id)

      if (form) {
        console.log('form', form)

        const schema = this.getSchema(formProps.model)

        if (schema) {
          const ajv = new Ajv({
            allErrors: true
          })
          const ajvValidator = ajv.compile(schema)
          const data = {...form.raw, ...form.computed}
          const result = ajvValidator(data)

          if (result) resolve()
          else reject(ajvValidator.errors)
        } else {
          reject(new Error('Model schema not found.'))
        }
      }

      resolve()
      //reject(new Error('Form not found.'))
    })
  }
}