import axios from "axios"

import * as Forms from "@/actions/Form"

import {trocarNomeSiglaEstado} from "@/utils/Brasil"

export function preencherEnderecoPorCep(dispatch, formId, cep) {
    if (cep.charAt(9) !== '' && cep.charAt(9) !== '_') {
        const cepNumber = parseInt(cep.replace('.', '').replace('-', ''))

        if (cepNumber) {
            axios({
                method: 'get',
                url: `https://viacep.com.br/ws/${cepNumber}/json/`
            }).then((response) => {
                if (!response.data.erro) {
                    dispatch(Forms.updateFormRawAttribute(formId, 'rua', { $set: response.data.logradouro }))
                    dispatch(Forms.updateFormRawAttribute(formId, 'numero', { $set: response.data.complemento }))
                    dispatch(Forms.updateFormRawAttribute(formId, 'bairro', { $set: response.data.bairro }))
                    dispatch(Forms.updateFormRawAttribute(formId, 'cidade', { $set: response.data.localidade }))
                    dispatch(Forms.updateFormRawAttribute(formId, 'estado', { $set: trocarNomeSiglaEstado(response.data.uf) }))
                    dispatch(Forms.updateFormRawAttribute(formId, 'pais', { $set: 'Brasil' }))
                }
            }).catch(console.error)
        }
    }
}