import React, { useRef, useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import { createSelector } from 'reselect'

import { login } from '@/actions/Session'

import bg1 from '@/assets/img/christina-wocintechchat-com-faEfWCdOKIg-unsplash-3-min.jpg'

export default function Login(props) {
  const auth = useSelector(state => state.Session.auth)
  const authenticated = useSelector(state => state.Session.auth.authenticated)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const dispatch = useDispatch()

  function handleEmailChange(event) {
    const value = event.target.value
    setEmail(value)
  }

  function handlePasswordChange(event) {
    const value = event.target.value
    setPassword(value)
  }

  function handleSubmit(event) {
    event.preventDefault()

    dispatch(login({ email, password }))
  }

  if (authenticated) {
    return (<Redirect to="/empresas"/>)
  }

  return (
    <div className="page page-login" style={{ backgroundImage: `url(${bg1})` }}>
      <div className="overlay overlay-orange-1"></div>
      <div className="loginbox">
        <h1 className="loginbox-title">Login</h1>
        <form className="form" onSubmit={handleSubmit}>
          <div className="inputs">
            <div className="input">
              <label>E-mail</label>
              <input type="email"placeholder="E-mail" value={email} onChange={handleEmailChange}/>
              {(auth.error && auth.error.code === 1002) && (
                  <div className="input-alert error">
                    <div className="svg icon">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510"><path d="M255 0C114.8 0 0 114.8 0 255s114.8 255 255 255 255-114.8 255-255S395.3 0 255 0zm30.4 406.8h-60.7v-60.7h60.7v60.7zm0-121.4h-60.7V103.2h60.7v182.2z"/></svg>
                    </div>
                    <span className="message">E-mail incorreto, tente novamente.</span>
                  </div>
              )}
            </div>
            <div className="input">
              <label>Senha</label>
              <input type="password" placeholder="Senha" value={password} onChange={handlePasswordChange}/>
              {(auth.error && auth.error.code === 1001) && (
                  <div className="input-alert error">
                    <div className="svg icon">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510"><path d="M255 0C114.8 0 0 114.8 0 255s114.8 255 255 255 255-114.8 255-255S395.3 0 255 0zm30.4 406.8h-60.7v-60.7h60.7v60.7zm0-121.4h-60.7V103.2h60.7v182.2z"/></svg>
                    </div>
                    <span className="message">Senha incorreta, tente novamente.</span>
                  </div>
              )}
            </div>
          </div>
          <div className="loginbox-links">
            <a href="">Esqueceu a senha?</a>
          </div>
          <button className="btn btn-login" type="submit">Entrar</button>
        </form>
        </div>
    </div>
  )
}

