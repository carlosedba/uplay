import React, {useState, useEffect} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {Link, useParams, useHistory} from 'react-router-dom'
import update from 'immutability-helper'

import {openModal, closeModal, updateModal, addModal} from '@/actions/Modal'

import DataWidget from '@/components/DataWidget'

import IcPlusSign from '@/icons/ic_plus_sign'
import IcPencil from '@/icons/ic_pencil'
import IcTrashcan from '@/icons/ic_trashcan'

export default function Post(props) {
  const params = useParams()
  const history = useHistory()

  const [loading, setLoading] = useState(true)
  const [profissional, setProfissional] = useState([])
  const [experiencias, setExperiencias] = useState([])
  const [formacoes, setFormacoes] = useState([])
  const [filhos, setFilhos] = useState([])

  const dispatch = useDispatch()

  const idiomasColumns = [
    {
      Header: 'Nome',
      accessor: 'idioma.label',
      width: 150
    },
    {
      Header: 'Nível',
      accessor: 'nivel.label',
      width: 150
    }
  ]

  const filhosColumns = [
    {
      Header: 'Nome',
      accessor: 'value',
      width: 300
    }
  ]

  const formacoesColumns = [
    {
      Header: 'Curso',
      accessor: 'curso.nome',
      width: 200
    },
    {
      Header: 'Instituição',
      accessor: 'instituicao.nome',
      width: 325
    },
    {
      Header: 'Ano de conclusão',
      accessor: 'anoConclusao',
      width: 125
    },
    {
      Header: ' ',
      width: 100,
      Cell: ({ row }) => (
        <div className="data-actions no-margin">
          <button className="svg data-action" onClick={(event) => handleFormacaoEdit(event, row.original)}>
            <IcPencil/>
          </button>
          <button className="svg data-action" onClick={(event) => handleFormacaoDelete(event, row.original)}>
            <IcTrashcan/>
          </button>
        </div>
      )
    }
  ]

  const experienciasColumns = [
    {
      Header: 'Cargo',
      accessor: 'cargo.nome',
      width: 200
    },
    {
      Header: 'Nível',
      accessor: 'nivelExperiencia',
      width: 100
    },
    {
      Header: 'Empresa',
      accessor: 'empresa.nomeFantasia',
      width: 200
    }
  ]

  useEffect(() => {
    fetchProfissional()

    dispatch(addModal('ModalPriContatosView'))
    dispatch(addModal('ModalRegionalView'))
  }, [])

  function fetchProfissional() {}

  function fetchExperienciaisProfissionais(profissional) {}

  function fetchFormacoes(profissional) {}

  function toObjectArray(arr) {
    return arr.reduce((acc, currentValue) => {
      return acc.concat({ value: currentValue })
    }, [])
  }

  function handleFormacaoEdit(event, data) {
    event.preventDefault()

    history.push(`/profissionais/${params.id}/formacoes/${data._id}/editar`)
  }

  function handleFormacaoDelete(event, data) {
    FormacaoRepository.remove(data._id)
      .then((response) => {
        fetchFormacoes(profissional._id)
      })
      .catch((err) => {
        alert('Erro ao deletar o item.')
      })
  }

  function handleEdit(data) {
    console.log(data)
  }

  function handleDelete(data) {
    console.log(data)
  }

  function handlePriContatoRowClick(row) {
    dispatch(updateModal('ModalPriContatosView', {
      $merge: { ...row.original }
    }))
    dispatch(openModal('ModalPriContatosView'))
  }

  function handleRegionalRowClick(row) {
    dispatch(updateModal('ModalRegionalView', {
      $merge: { ...row.original }
    }))
    dispatch(openModal('ModalRegionalView'))
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header-lr">
          <div className="page-header-left">
            <div className="page-titles">
              <Link to="/profissionais" className="page-tie">Voltar</Link>
              <p className="page-title">{(!loading) ? profissional.nome + ' ' + profissional.sobrenome : ''}</p>
              <p className="page-text">
                <a href={profissional.email} target="_blank">{profissional.email}</a>
              </p>
            </div>
          </div>
          <div className="page-header-right">
            <Link to={`/profissionais/${params.id}/editar`} className="btn btn-five btn-orange">
              <div className="svg btn-icon"><IcPlusSign/></div>
              Editar dados
            </Link>
          </div>
        </div>
        <div className="page-content">

          <div className="data-widget-grid">
            <div className="data-widget-grid-row">
              <div className="data-widget-grid-col sp3">
                <div className="data-widget-grid-row">
                  <div className="data-widget data-widget-one full">
                    <div className="data-widget-header">
                      <span className="data-widget-title">Informações básicas</span>
                    </div>
                    <div className="data-widget-content">
                      <div className="data-fields data-fields-one">
                        <div className="data-field">
                          <span className="data-field-name">Data de nascimento:</span>
                          <span className="data-field-value">{profissional.dataNascimento}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">Estado civil:</span>
                          <span className="data-field-value">{profissional.estadoCivil}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">RG:</span>
                          <span className="data-field-value">{(!loading) ? profissional.rg : ''}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">CPF:</span>
                          <span className="data-field-value">{profissional.cpf}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">Sexo:</span>
                          <span className="data-field-value">{profissional.sexo}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">Deficiente:</span>
                          <span className="data-field-value">{profissional.deficiente}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">Telefone:</span>
                          <span className="data-field-value">{profissional.telefone}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">Celular:</span>
                          <span className="data-field-value">{profissional.celular}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">E-mail:</span>
                          <span className="data-field-value">{profissional.email}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">Naturalidade:</span>
                          <span className="data-field-value">{(!loading) ? profissional.naturalidade.nome : null}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">Estado de origem:</span>
                          <span className="data-field-value">{(!loading) ? profissional.estadoOrigem.nome : ''}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">Cidade de origem:</span>
                          <span className="data-field-value">{(!loading) ? profissional.cidadeOrigem.nome : ''}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">País atual:</span>
                          <span className="data-field-value">{(!loading) ? profissional.paisAtual.nome : ''}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">Estado atual:</span>
                          <span className="data-field-value">{(!loading) ? profissional.estadoAtual.nome : ''}</span>
                        </div>
                        <div className="data-field">
                          <span className="data-field-name">Cidade atual:</span>
                          <span className="data-field-value">{(!loading) ? profissional.cidadeAtual.nome : ''}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="data-widget-grid-col sp2">
                <div className="data-widget-grid-row">
                  <div className="data-widget-grid-col">
                    <DataWidget
                      size="full"
                      title="Idiomas"
                      columns={idiomasColumns}
                      data={(!loading) ? profissional.idiomas : []}/>
                  </div>
                  <div className="data-widget-grid-col">
                    <DataWidget
                      size="full"
                      title="Filhos"
                      columns={filhosColumns}
                      data={(!loading) ? filhos : []}/>
                  </div>
                </div>
                <div className="data-widget-grid-row">
                  <DataWidget
                    size="full"
                    title="Formações"
                    columns={formacoesColumns}
                    data={(!loading) ? formacoes : []}/>
                </div>
                <div className="data-widget-grid-row">
                  <DataWidget
                    size="full"
                    title="Experiências Posts"
                    columns={experienciasColumns}
                    data={(!loading) ? experiencias : []}/>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  )
}

