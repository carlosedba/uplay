import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {Link, useHistory, useParams} from 'react-router-dom'

import { addModal } from '@/actions/Modal'
import * as Forms from '@/actions/Form'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import CurrencyInput from '@/components/Input/CurrencyInput'
import Select from '@/components/Select'
import FormSection from '@/components/FormSection'
import ArrayAttributeFormSection from '@/components/FormSection/ArrayAttributeFormSection'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'


import IcPlusSign from '@/icons/ic_plus_sign'
import IcMoney from '@/icons/ic_money'

import IdGenerator from '@/utils/IdGenerator'

import * as textMasks from '@/textMasks'
import FormValidator from '@/FormValidator'

const formId = IdGenerator.default()

export default function EditPost(props) {
  const params = useParams()

  const [loading, setLoading] = useState(true)
  const [profissional, setProfissional] = useState({})
  const [naturalidades, setNaturalidades] = useState([])
  const [estadoOrigemList, setEstadoOrigemList] = useState([])
  const [cidadeOrigemList, setCidadeOrigemList] = useState([])
  const [paisAtualList, setPaisAtualList] = useState([])
  const [estadoAtualList, setEstadoAtualList] = useState([])
  const [cidadeAtualList, setCidadeAtualList] = useState([])

  const forms = useSelector(state => state.Forms)

  const dispatch = useDispatch()

  const history = useHistory()

  const formProps = {
    id: formId,
    model: 'S7Profissional',
    action: 'update',
    dependsOn: null,
    proccess: true
  }

  useEffect(() => {
    console.log('log > Editar Post > useEffect called!')
    dispatch(addModal('ModalFilhosCreate'))
    dispatch(addModal('ModalFilhosUpdate'))

    dispatch(Forms.newForm(formId))

    fetchPaises()
    fetchProfissional()
  }, [])

  function fetchProfissional() {}

  function fetchEstadoOrigemList(pais) {
    return new Promise((resolve, reject) => {
      EstadoRepository.find({ pais: pais._id })
        .then((data) => {
          setEstadoOrigemList(data.reduce((acc, currentValue) => {
            return acc.concat({ value: currentValue._id, label: currentValue.nome })
          }, []))
        })
        .then(resolve)
    })
  }

  function fetchCidadeOrigemList(estado) {
    return new Promise((resolve, reject) => {
      CidadeRepository.find({ estado: estado._id })
        .then((data) => {
          setCidadeOrigemList(data.reduce((acc, currentValue) => {
            return acc.concat({ value: currentValue._id, label: currentValue.nome })
          }, []))
        })
        .then(resolve)
    })
  }

  function fetchEstadoAtualList(pais) {
    return new Promise((resolve, reject) => {
      EstadoRepository.find({ pais: pais._id })
        .then((data) => {
          setEstadoAtualList(data.reduce((acc, currentValue) => {
            return acc.concat({ value: currentValue._id, label: currentValue.nome })
          }, []))
        })
        .then(resolve)
    })
  }

  function fetchCidadeAtualList(estado) {
    return new Promise((resolve, reject) => {
      CidadeRepository.find({ estado: estado._id })
        .then((data) => {
          setCidadeAtualList(data.reduce((acc, currentValue) => {
            return acc.concat({ value: currentValue._id, label: currentValue.nome })
          }, []))
        })
        .then(resolve)
    })
  }

  function setObjectId(id) {
    dispatch(Forms.updateFormComputedAttribute(formId, '_id', {
      $set: id
    }))
  }

  function setFilhos(filhos) {
    dispatch(Forms.updateFormRawAttribute(formId, 'filhos', {
      $set: filhos
    }))
  }

  function setIdiomas(idiomas) {
    dispatch(Forms.updateFormRawAttribute(formId, 'idiomas', {
      $set: idiomas
    }))

    dispatch(Forms.updateFormComputedAttribute(formId, 'idiomas', {
      $set: idiomas
    }))
  }

  function fetchPaises() { }

  function handleNaturalidadeChange(event) {
    console.log('log > Editar Empresa > handleNaturalidadeChange called!')

    EstadoRepository.find({ pais: event.value })
      .then((data) => {
        setEstadoOrigemList(data.reduce((acc, currentValue) => {
          return acc.concat({ value: currentValue._id, label: currentValue.nome })
        }, []))
      })
  }

  function handleEstadoOrigemChange(event) {
    console.log('log > Editar Empresa > handleEstadoOrigemChange called!')
    CidadeRepository.find({ estado: event.value })
      .then((data) => {
        setCidadeOrigemList(data.reduce((acc, currentValue) => {
          return acc.concat({ value: currentValue._id, label: currentValue.nome })
        }, []))
      })
  }

  function handlePaisAtualChange(event) {
    console.log('log > Editar Empresa > handlePaisAtualChange called!')
    EstadoRepository.find({ pais: event.value })
      .then((data) => {
        setEstadoAtualList(data.reduce((acc, currentValue) => {
          return acc.concat({ value: currentValue._id, label: currentValue.nome })
        }, []))
      })
  }

  function handleEstadoAtualChange(event) {
    console.log('log > Editar Empresa > handleEstadoAtualChange called!')
    CidadeRepository.find({ estado: event.value })
      .then((data) => {
        setCidadeAtualList(data.reduce((acc, currentValue) => {
          return acc.concat({ value: currentValue._id, label: currentValue.nome })
        }, []))
      })
  }

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps)
          .then(() => {
            alert('Post atualizado com sucesso!')
            history.push('/profissionais')
          })
          .catch((err) => {
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((err) => {
        console.error(err)
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to={`/profissionais/${params.id}`} className="page-tie">Voltar</Link>
            <p className="page-title">Editar Profissional</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <div className="multi-input">
                  <TextInput formId={formId} classes="w2" label="Nome" attribute="nome" required={true} initialValue={profissional.nome}/>
                  <TextInput formId={formId} classes="w2-q2" label="Sobrenome" attribute="sobrenome" required={true} initialValue={profissional.sobrenome}/>
                  <Select
                    formId={formId}
                    size="w1-q3"
                    label="Sexo"
                    attribute="sexo"
                    required={true}
                    initialValue={profissional.sexo}
                    options={[
                      { value: 'Masculino', label: 'Masculino' },
                      { value: 'Feminino', label: 'Feminino' },
                      { value: 'Outro', label: 'Outro' },
                    ]}/>
                  <Select
                    formId={formId}
                    size="w1-q2"
                    label="Deficiente"
                    attribute="deficiente"
                    required={true}
                    initialValue={profissional.deficiente}
                    options={[
                      { value: 'Sim', label: 'Sim' },
                      { value: 'Não', label: 'Não' },
                    ]}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w1-q1" label="RG" attribute="rg" required={true} mask={textMasks.RG} initialValue={profissional.rg}/>
                  <TextInput formId={formId} classes="w1-q2" label="CPF" attribute="cpf" required={true} mask="cpf" initialValue={profissional.cpf}/>
                  <TextInput formId={formId} classes="w1-q2" label="Telefone" attribute="telefone" mask="telefone" initialValue={profissional.telefone}/>
                  <TextInput formId={formId} classes="w1-q2" label="Celular" attribute="celular" mask="celular" initialValue={profissional.celular}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w2" label="Data de nascimento" attribute="dataNascimento" required={true} mask={textMasks.DATE} initialValue={profissional.dataNascimento}/>
                  <Select
                    formId={formId}
                    size="w2"
                    label="Estado civil"
                    attribute="estadoCivil"
                    required={true}
                    initialValue={profissional.estadoCivil}
                    options={[
                      { value: 'Solteiro (a)', label: 'Solteiro (a)' },
                      { value: 'Casado (a)', label: 'Casado (a)' },
                      { value: 'Divorciado (a)', label: 'Divorciado (a)' },
                      { value: 'Viúvo (a)', label: 'Viúvo (a)' },
                      { value: 'União estável', label: 'União estável' },
                    ]}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w3-q1" label="E-mail" attribute="email" required={true} initialValue={profissional.email}/>
                  <TextInput formId={formId} classes="w2" label="Skype" attribute="skype" initialValue={profissional.skype}/>
                  <TextInput formId={formId} classes="w2" label="LinkedIn" attribute="linkedin" intialValue={profissional.linkedin}/>
                </div>
                <div className="multi-input">
                  <Select
                    formId={formId}
                    size="w2"
                    label="Naturalidade"
                    attribute="naturalidade"
                    required={true}
                    options={naturalidades}
                    initialValue={(!loading) ? profissional.naturalidade._id : null}
                    onChange={handleNaturalidadeChange}/>
                  <Select
                    formId={formId}
                    size="w2-q1"
                    label="Estado de origem"
                    attribute="estadoOrigem"
                    required={true}
                    options={estadoOrigemList}
                    initialValue={(!loading) ? profissional.estadoOrigem._id : null}
                    onChange={handleEstadoOrigemChange}/>
                  <Select
                    formId={formId}
                    size="w2"
                    label="Cidade de origem"
                    attribute="cidadeOrigem"
                    required={true}
                    options={cidadeOrigemList}
                    initialValue={(!loading) ? profissional.cidadeOrigem._id : null}/>
                </div>
                <div className="multi-input">
                  <Select
                    formId={formId}
                    size="w2"
                    label="País atual"
                    attribute="paisAtual"
                    required={true}
                    options={paisAtualList}
                    initialValue={(!loading) ? profissional.paisAtual._id : null}
                    onChange={handlePaisAtualChange}/>
                  <Select
                    formId={formId}
                    size="w2-q1"
                    label="Estado atual"
                    attribute="estadoAtual"
                    required={true}
                    options={estadoAtualList}
                    initialValue={(!loading) ? profissional.estadoAtual._id : null}
                    onChange={handleEstadoAtualChange}/>
                  <Select
                    formId={formId}
                    size="w2"
                    label="Cidade atual"
                    attribute="cidadeAtual"
                    required={true}
                    options={cidadeAtualList}
                    initialValue={(!loading) ? profissional.cidadeAtual._id : null}/>
                </div>
              </div>
            </Form>

            <ArrayAttributeFormSection
              formId={formId}
              attribute="filhos"
              title="Filhos"
              message="Adicionar filho"
              icon={IcPlusSign}
              onCreateModalName="ModalFilhosCreate"
              onUpdateModalName="ModalFilhosUpdate">
            </ArrayAttributeFormSection>

            <ObjectArrayAttributeFormSection
              formId={formId}
              attribute="idiomas"
              title="Idiomas"
              message="Adicionar idioma"
              icon={IcPlusSign}
              attributeIsSelect={true}
              attributeToDisplay="idioma"
              onCreateModalName="ModalIdiomaCreate"
              onUpdateModalName="ModalIdiomaUpdate">
            </ObjectArrayAttributeFormSection>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

