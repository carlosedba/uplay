import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'

import { addModal } from '@/actions/Modal'
import * as Forms from '@/actions/Form'

import Form from '@/components/Form'
import TextInput from '@/components/Input/TextInput'
import SuggestionInput from '@/components/Input/SuggestionInput'
import CurrencyInput from '@/components/Input/CurrencyInput'
import Select from '@/components/Select'
import FormSection from '@/components/FormSection'
import ArrayAttributeFormSection from '@/components/FormSection/ArrayAttributeFormSection'
import ObjectArrayAttributeFormSection from '@/components/FormSection/ObjectArrayAttributeFormSection'


import IcPlusSign from '@/icons/ic_plus_sign'
import IcMoney from '@/icons/ic_money'

import IdGenerator from '@/utils/IdGenerator'

import * as textMasks from '@/textMasks'
import FormValidator from '@/FormValidator'
import TextAttributeFormSection from '@/components/FormSection/TextAttributeFormSection'

const formId = IdGenerator.default()

export default function NewPost(props) {
  const [naturalidades, setNaturalidades] = useState([])
  const [estadoOrigemList, setEstadoOrigemList] = useState([])
  const [cidadeOrigemList, setCidadeOrigemList] = useState([])
  const [paisAtualList, setPaisAtualList] = useState([])
  const [estadoAtualList, setEstadoAtualList] = useState([])
  const [cidadeAtualList, setCidadeAtualList] = useState([])

  const forms = useSelector(state => state.Forms)

  const dispatch = useDispatch()

  const history = useHistory()

  const formProps = {
    id: formId,
    model: 'S7Profissional',
    action: 'create',
    dependsOn: null,
    proccess: true
  }

  useEffect(() => {
    dispatch(addModal('ModalFilhosCreate'))
    dispatch(addModal('ModalFilhosUpdate'))

    dispatch(addModal('ModalIdiomaCreate'))
    dispatch(addModal('ModalIdiomaUpdate'))

    dispatch(addModal('ModalExpProfissionalCreate'))
    dispatch(addModal('ModalExpProfissionalUpdate'))
    dispatch(addModal('ModalExpProfissionalSalario'))

    dispatch(addModal('ModalFormacaoCreate'))
    dispatch(addModal('ModalFormacaoUpdate'))

    dispatch(addModal('ModalAbordagem'))
    dispatch(addModal('ModalComentarios'))

    dispatch(Forms.newForm(formId))

    fetchPaises()
  }, [])

  function fetchPaises() {}

  function handleNaturalidadeChange(event) {}

  function handleEstadoOrigemChange(event) {}

  function handlePaisAtualChange(event) {}

  function handleEstadoAtualChange(event) {}

  async function handleSubmit(event) {
    FormValidator.validate(formProps)
      .then(() => {
        FormHelper.submit(formProps)
          .then(() => {
            alert('Post cadastrado com sucesso!')
            history.push('/profissionais')
          })
          .catch((err) => {
            alert('Um erro ocorreu ao salvar os dados.')
          })
      })
      .catch((errors) => {
        alert('Preencha todos campos obrigatórios.')
      })
  }

  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <Link to="/profissionais" className="page-tie">Profissionais</Link>
            <p className="page-title">Cadastrar Profissional</p>
            <p className="page-subtitle">Informe os dados em seus respectivos campos:</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="page-content-wrapper page-content-form-wrapper">
            <Form {...formProps}>
              <div className="inputs input-style-2">
                <div className="multi-input">
                  <TextInput formId={formId} classes="w2" label="Nome" attribute="nome" required={true}/>
                  <TextInput formId={formId} classes="w2-q2" label="Sobrenome" attribute="sobrenome" required={true}/>
                  <Select formId={formId} size="w1-q3" label="Sexo" attribute="sexo" required={true}
                          options={[
                            { value: 'Masculino', label: 'Masculino' },
                            { value: 'Feminino', label: 'Feminino' },
                            { value: 'Outro', label: 'Outro' },
                          ]}/>
                  <Select formId={formId} size="w1-q2" label="Deficiente" attribute="deficiente" required={true}
                          options={[
                            { value: 'Sim', label: 'Sim' },
                            { value: 'Não', label: 'Não' },
                          ]}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w1-q1" label="RG" attribute="rg" mask={textMasks.RG}/>
                  <TextInput formId={formId} classes="w1-q2" label="CPF" attribute="cpf" mask="cpf"/>
                  <TextInput formId={formId} classes="w1-q2" label="Telefone" attribute="telefone" mask="telefone"/>
                  <TextInput formId={formId} classes="w1-q2" label="Celular 1" attribute="celular1" mask="celular"/>
                  <TextInput formId={formId} classes="w1-q2" label="Celular 2" attribute="celular2" mask="celular"/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w06" label="Idade" attribute="idade"/>
                  <TextInput formId={formId} classes="w2" label="Data de nascimento" attribute="dataNascimento" mask={textMasks.DATE}/>
                  <Select formId={formId} size="w2" label="Estado civil" attribute="estadoCivil"
                          options={[
                            { value: 'Solteiro (a)', label: 'Solteiro (a)' },
                            { value: 'Casado (a)', label: 'Casado (a)' },
                            { value: 'Divorciado (a)', label: 'Divorciado (a)' },
                            { value: 'Viúvo (a)', label: 'Viúvo (a)' },
                            { value: 'União estável', label: 'União estável' },
                          ]}/>
                  <TextInput formId={formId} classes="w2-q2" label="Emprego do cônjuge" attribute="empregoConjuge"/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w3-q1" label="E-mail" attribute="email" required={true}/>
                  <TextInput formId={formId} classes="w3-q1" label="E-mail comercial" attribute="emailComercial" required={true}/>
                </div>
                <div className="multi-input">
                  <TextInput formId={formId} classes="w2" label="Skype" attribute="skype"/>
                  <TextInput formId={formId} classes="w2" label="LinkedIn" attribute="linkedin"/>
                </div>
                <div className="multi-input">
                  <Select formId={formId} size="w2" label="Naturalidade" attribute="naturalidade" required={true} options={naturalidades} onChange={handleNaturalidadeChange}/>
                  <Select formId={formId} size="w2-q1" label="Estado de origem" attribute="estadoOrigem" required={true} options={estadoOrigemList} onChange={handleEstadoOrigemChange}/>
                  <Select formId={formId} size="w2" label="Cidade de origem" attribute="cidadeOrigem" required={true} options={cidadeOrigemList}/>
                </div>
                <div className="multi-input">
                  <Select formId={formId} size="w2" label="País atual" attribute="paisAtual" required={true} options={paisAtualList} onChange={handlePaisAtualChange}/>
                  <Select formId={formId} size="w2-q1" label="Estado atual" attribute="estadoAtual" required={true} options={estadoAtualList} onChange={handleEstadoAtualChange}/>
                  <Select formId={formId} size="w2" label="Cidade atual" attribute="cidadeAtual" required={true} options={cidadeAtualList}/>
                </div>
              </div>
            </Form>

            <ArrayAttributeFormSection
              formId={formId}
              attribute="filhos"
              title="Filhos"
              message="Adicionar filho"
              icon={IcPlusSign}
              onCreateModalName="ModalFilhosCreate"
              onUpdateModalName="ModalFilhosUpdate">
            </ArrayAttributeFormSection>

            <ObjectArrayAttributeFormSection
              formId={formId}
              attribute="idiomas"
              title="Idiomas"
              message="Adicionar idioma"
              icon={IcPlusSign}
              attributeIsSelect={true}
              attributeToDisplay="idioma"
              onCreateModalName="ModalIdiomaCreate"
              onUpdateModalName="ModalIdiomaUpdate">
            </ObjectArrayAttributeFormSection>

            <FormSection
              model="S7ExperienciaProfissional"
              action="create"
              title="Experiências profissionais"
              message="Adicionar experiência"
              icon={IcPlusSign}
              attributeToDisplay="cargo"
              onCreateModalName="ModalExpProfissionalCreate"
              onUpdateModalName="ModalExpProfissionalUpdate"
              customActions={[
                { icon: IcMoney, modal: 'ModalExpProfissionalSalario' }
              ]}>
            </FormSection>

            <FormSection
              model="S7Formacao"
              action="create"
              title="Formações"
              message="Adicionar formação"
              icon={IcPlusSign}
              attributeToDisplay="curso"
              onCreateModalName="ModalFormacaoCreate"
              onUpdateModalName="ModalFormacaoUpdate">
            </FormSection>

            <TextAttributeFormSection
              title="Abordagem"
              message="Editar abordagem"
              icon={IcPlusSign}
              onCreateModalName="ModalAbordagem">
            </TextAttributeFormSection>

            <TextAttributeFormSection
              title="Comentários"
              message="Editar comentários"
              icon={IcPlusSign}
              onCreateModalName="ModalComentarios">
            </TextAttributeFormSection>

            <button className="btn btn-six btn-orange" onClick={handleSubmit}>Salvar</button>
          </div>
        </div>
      </div>
    </div>
  )
}

