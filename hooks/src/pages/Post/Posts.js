import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link, useHistory } from 'react-router-dom'
import update from 'immutability-helper'

import DataTable from "@/components/DataTable"

import IcPlusSign from '@/icons/ic_plus_sign'
import IcPencil from '@/icons/ic_pencil'
import IcTrashcan from '@/icons/ic_trashcan'

export default function Posts(props) {
  const [posts, setPosts] = useState([])

  const history = useHistory()

  useEffect(() => {
    getPosts()
  }, [])

  const columns = [
    {
      Header: 'Nome',
      accessor: 'name',
      width: 350
    },
    {
      Header: ' ',
      width: 100,
      Cell: ({ row }) => (
        <div className="data-actions no-margin">
          <button className="svg data-action" onClick={(event) => handleEdit(event, row.original)}>
            <IcPencil/>
          </button>
          <button className="svg data-action" onClick={() => handleDelete(row.original)}>
            <IcTrashcan/>
          </button>
        </div>
      )
    }
  ]

  function getPosts() {}

  function handleEdit(event, data) {
    event.stopPropagation()

    history.push(`/posts/${data._id}/editar`)
  }

  function handleDelete(data) {
    event.stopPropagation()

    console.log(data)
  }

  function handleRowClick(row) {
    history.push(`/posts/${row.original._id}`)
  }

  return (
    <div className="page">;
      <div className="widget widget-posts">
        <div className="widget-header">
          <div className="left">
            <div className="widget-titles">
              <p className="widget-title">Posts</p>
            </div>
          </div>
          <div className="right">
            <div className="filters"></div>

            <Link to="/posts/cadastrar" className="btn btn-five btn-yellow">
              <div className="svg btn-icon"><IcPlusSign/></div>
              Novo Post
            </Link>
          </div>
        </div>
        <div className="widget-content"></div>
      </div>
    </div>
  )
}

