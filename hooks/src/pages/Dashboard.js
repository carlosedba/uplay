import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import update from 'immutability-helper'

export default function Dashboard(props) {
  return (
    <div className="page">
      <div className="page-wrapper">
        <div className="page-header">
          <div className="page-titles">
            <p className="page-title">Dashboard</p>
            <p className="page-text"></p>
          </div>
        </div>
        <div className="page-content">
          <div className="bottombar"></div>
        </div>
      </div>
    </div>
  )
}

