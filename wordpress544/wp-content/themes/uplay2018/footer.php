<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Academia_Uplay_Fitness_2018
 */

?>

<footer class="footer">
  <div class="footer-sections">
    <section class="section"></section>
  </div>
  <div class="footer-content">
    <div class="footer-content-left">
      <a class="footer-logo" href="<?php echo site_url(); ?>">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 149.1">
          <style>.lu0{fill:#fcc633}.lu1{fill:#fff}</style><path class="lu0" d="M21.5 79.1h32.4c7.5 0 9.1-6 9.1-9.9V55.3c0-2.1 1.8-3.9 3.9-3.9 2.1 0 3.9 1.8 3.9 3.9v13.8c0 8.2-5.6 17.7-15 17.7h-37c-8.9-.4-14.2-9.6-14.2-17.6V55.3c0-2.1 1.8-3.9 3.9-3.9s3.9 1.8 3.9 3.9v13.8c0 4 1.6 10 9.1 10"/><path class="lu1" d="M131.6 51.4c6 0 11 4.8 11 10.8 0 6-5 10.8-11 10.8H86.8v10c0 2.1-1.9 3.9-3.9 3.9-2.1 0-3.9-1.8-3.9-3.9V55.3c0-2.1 1.8-3.9 3.9-3.9h48.7zm-44.8 7.8v6H132c1.6 0 3.1-1.3 3.1-3 0-1.6-1.5-3-3.1-3H86.8zM213.5 85.4c-.7.9-1.9 1.5-3 1.5h-48.6c-6 0-11.1-4.8-11.1-10.8V55.3c0-2.1 1.8-3.9 3.9-3.9 2 0 3.9 1.8 3.9 3.9v20.8c0 1.6 1.5 3 3.1 3h46.9L227.2 54c1.9-2.5 5-4.2 8.5-4.2 3.3 0 6.3 1.5 8.2 4l19.8 26.4c.6.7 1.1 1.7 1.1 2.8 0 2.1-1.8 3.9-3.9 3.9-1.2 0-2.3-.6-3.1-1.6l-19-25.3c-.7-.9-1.9-2.2-3.2-2.2-1.2 0-2.4 1.4-3.1 2.2l-19 25.4zm22.2-12.3c2.8 0 5 2.3 5 5s-2.3 5-5 5c-2.8 0-5-2.3-5-5s2.2-5 5-5M321 52.6c.8-.6 1.7-1.1 2.8-1.1 2.1 0 3.9 1.8 3.9 3.9 0 1.2-.6 2.4-1.5 3.1-13.6 12-20.7 15.5-31.1 15.5-11.1 0-17.3-3.3-31.3-15.5-.9-.7-1.5-1.9-1.5-3.1 0-2.1 1.8-3.9 3.9-3.9 1.1 0 1.9.5 2.8 1.1 13.9 12.2 17.6 12.9 26.1 13 8.3 0 11.8-.7 25.9-13m-25.9 25.3c2.8 0 5 2.3 5 5 0 2.8-2.3 5-5 5-2.8 0-5.1-2.3-5.1-5s2.3-5 5.1-5"/><path class="lu0" d="M312.8 22.7c-2-1.7-3.4-4.3-3.4-6.9 0-4.9 4.1-9 9-9 2.1 0 3.9.6 5.4 1.9l61.1 45.4c5.8 4.3 9.7 11.6 9.7 19.6 0 7.5-3.4 14.6-9.2 18.9L324.7 138c-1.7 1.5-3.9 2.6-6.4 2.6-4.9 0-9-4.1-9-9 0-2.8 1.3-5.4 3.6-7.1L371.1 81c2.1-1.7 5-4.5 5-7.3s-3.2-5.6-5-7.1l-58.3-43.9z"/><path class="lu1" d="M152.9 103c-.3.3-.5.6-.7 1-.2.4-.3.8-.3 1.3v.4h11.9v1.2H152v4.9h-1.2v-6.5c0-.6.1-1.2.4-1.8.2-.5.6-1 1-1.4.4-.4.9-.7 1.4-1 .6-.2 1.1-.4 1.8-.4h9.3v1.2h-9.3c-.5 0-.9.1-1.3.3-.5.3-.9.5-1.2.8M175.2 100.8h1.2v11.1h-1.2zM200.1 102h-6.3v9.9h-1.2V102h-6.3v-1.2h13.8zM222.8 111.8l-.4-.2-.4-.4-7.6-9-.3-.2-.4-.1c-.3 0-.5.1-.7.2-.2.2-.3.4-.3.6v9.1h-1.2v-9.2c0-.3.1-.6.2-.8.1-.2.3-.4.5-.6.2-.2.4-.3.7-.4.3-.1.5-.1.8-.1.3 0 .6.1.9.2.3.1.5.3.8.6l7.6 9 .3.3.4.1c.2 0 .5-.1.7-.2.2-.2.3-.4.3-.6V101h1.2v9.2c0 .3-.1.6-.3.8-.1.2-.3.4-.5.6-.2.2-.4.3-.7.4-.3.1-.5.1-.8.1-.2-.1-.6-.2-.8-.3M239.3 103c-.3.3-.5.7-.7 1.1-.2.4-.3.8-.3 1.3v.4H251v1.2h-12.7v.4c0 .5.1.9.3 1.3.2.4.4.8.7 1 .3.3.7.5 1.1.7.4.2.8.3 1.3.3h9.3v1.2h-9.3c-.6 0-1.2-.1-1.8-.4-.6-.2-1-.6-1.4-1-.4-.4-.7-.9-1-1.5-.3-.5-.4-1.1-.4-1.7v-2c0-.6.1-1.2.4-1.8.2-.5.6-1 1-1.4.4-.4.9-.7 1.4-1 .5-.2 1.1-.4 1.7-.4h9.3v1.2h-9.3c-.5 0-.9.1-1.3.3-.4.3-.7.5-1 .8M272.5 111.9H262v-1.2h10.5c.3 0 .5 0 .7-.1.2-.1.4-.2.6-.4.2-.2.3-.4.4-.6.1-.2.1-.5.1-.7 0-.3-.1-.5-.1-.7-.1-.2-.2-.4-.4-.6-.2-.2-.4-.3-.6-.4-.2-.1-.5-.1-.7-.1h-8.1c-.4 0-.8-.1-1.2-.2-.4-.2-.7-.4-1-.7-.3-.3-.5-.6-.7-1-.2-.4-.2-.8-.2-1.2 0-.4.1-.8.2-1.2.2-.4.4-.7.7-1 .3-.3.6-.5 1-.7.4-.2.8-.3 1.2-.3H275v1.2h-10.5c-.3 0-.5 0-.7.2-.2.1-.4.2-.6.4-.2.1-.3.3-.4.6-.1.2-.1.5-.2.7 0 .5.2 1 .6 1.3.3.4.8.5 1.3.5h8.1c.4 0 .8.1 1.2.2.4.2.7.4 1 .7.3.3.5.6.6 1 .2.4.2.8.2 1.2 0 .4-.1.8-.2 1.2-.2.4-.4.7-.6 1-.3.3-.6.5-1 .7-.4.1-.8.2-1.3.2M296.7 111.9h-10.5v-1.2h10.5c.2 0 .5 0 .7-.1.2-.1.4-.2.6-.4.2-.2.3-.4.4-.6.1-.2.1-.5.1-.7 0-.3-.1-.5-.1-.7-.1-.2-.2-.4-.4-.6-.2-.2-.4-.3-.6-.4-.2-.1-.5-.1-.7-.1h-8.1c-.4 0-.8-.1-1.2-.2-.4-.2-.7-.4-1-.7-.3-.3-.5-.6-.7-1-.2-.4-.2-.8-.2-1.2 0-.4.1-.8.2-1.2.2-.4.4-.7.7-1 .3-.3.6-.5 1-.7.4-.2.8-.3 1.2-.3h10.5v1.2h-10.5c-.3 0-.5 0-.7.2-.2.1-.4.2-.6.4-.2.1-.3.3-.4.6-.1.2-.2.5-.2.7 0 .5.2 1 .6 1.3.3.4.8.5 1.3.5h8.1c.4 0 .8.1 1.2.2.4.2.7.4 1 .7.3.3.5.6.6 1 .2.4.3.8.3 1.2 0 .4-.1.8-.3 1.2-.2.4-.4.7-.6 1-.3.3-.6.5-1 .7-.3.1-.7.2-1.2.2"/>
        </svg>
      </a>
    </div>
    <div class="footer-content-right">
      <div class="footer-info">
        <p class="footer-info-title">Contato</p>
        <p class="footer-info-text">Financeiro e Franquias: <a href="mailto:financeiro@uplayfit.com">financeiro@uplayfit.com</a></p>
        <p class="footer-info-text">Telefone: <?php echo get_theme_mod('uplay2018_rodape_telefone'); ?></p>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>

<script type="text/javascript">
  function setHeroSlider() {
    const heroSlides = document.querySelector('.hero-slides')

    if (heroSlides !== null && heroSlides !== undefined) {
      const slider = tns({
        //autoHeight: true,
        container: '.hero-slides',
        items: 1,
        slideBy: 'page',
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayButtonOutput: false,
        speed: 560,
        controlsText: ['<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.7 62.2"><path fill="#fff" d="M19 2.8l-.8-1.4L1.7 31.1l16.5 29.7.8-1.4L3.2 31.1z"/></svg>', '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20.7 62.2"><path fill="#fff" d="M17.4 31.1L1.7 59.4l.7 1.4L19 31.1 2.4 1.4l-.7 1.4z"/></svg>']
      })
    }
  }

  setHeroSlider()
</script>
<script type="text/javascript">
  function setUnidadesMouseDrag() {
    const sectionCards = document.querySelector('#unidades .section-cards')

    if (sectionCards !== null && sectionCards !== undefined) {
      new Flickity('#unidades .section-cards', {
        freeScroll: true,
        wrapAround: true,
        pageDots: false,
        percentPosition: false,
        prevNextButtons: true,
        friction: 0.22,
        selectedAttraction: 0.015,
        autoPlay: 2600,
        cellAlign: 'left',
        //arrowShape: 'M65.1 2.4L63.7 0 35.9 50l27.8 50 1.3-2.4L38.5 50 65.1 2.4z',
      })
    }
  }

  setUnidadesMouseDrag()
</script>
<script type="text/javascript">
  function setupHamburgerMenu() {
    const btn = document.querySelector('.hamburger')
    const menu = document.querySelector('.navbar-menu')
    const links = document.querySelectorAll('.navbar-menu a')

    if (btn !== null && menu != null) {
      btn.addEventListener('click', function (e) {
        btn.classList.toggle('is-active')
        menu.classList.toggle('is-open')
      })

      ;[].forEach.call(links, function (el, i) {
        el.addEventListener('click', function (e) {
          btn.classList.remove('is-active')
          menu.classList.remove('is-open')
        })
      })
    }
  }
  setupHamburgerMenu()
</script>
<script type="text/javascript">
  function setSectionCardOnePicClick() {
    var sectionCardOneList = document.querySelectorAll('.section-card-one')
    var sectionCardOnePictureList = document.querySelectorAll('.section-card-one .card-picture')

    ;[].forEach.call(sectionCardOnePictureList, function (el, i) {
      el.addEventListener('click', function (event) {
        var url = sectionCardOneList[i].dataset.url

        window.location.href = url
      })
    })
  }

  setSectionCardOnePicClick()
</script>
</body>
</html>
