<?php

function fixo() {
  $field_value = get_field('numero_de_telefone_fixo');

  if (!empty($field_value)) {
    echo 'Fixo: ' . $field_value;
  }
}

function whatsapp() {
  $field_value = get_field('numero_de_whatsapp');

  if (!empty($field_value)) {
    echo 'WhatsApp: ' . $field_value;
  }
}

function instagram() {
  $field_value = get_field('conta_do_instagram');

  if (!empty($field_value)) {
    echo 'Instagram: <a href="https://instagram.com/' . $field_value . '" target="_blank">@' . $field_value . '</a>';
  }
}

function has_gallery_pictures($id)
{
  $images = acf_photo_gallery('fotos', $id);

  if (count($images)) {
    return true;
  }

  return false;
}

function gallery_pictures($id) {
  $images = acf_photo_gallery('fotos', $id);

  if (count($images)) {
    foreach ($images as $image) {
      echo '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">';
      echo '<a href="' . $image['full_image_url'] . '" itemprop="contentUrl" data-size="1024x768">';
      echo '<img src="' . $image['thumbnail_image_url'] . '" itemprop="thumbnail"/>';
      echo '</a>';
      echo '<figcaption itemprop="caption description"></figcaption>';
      echo '</figure>';
    }
  }
}

function google_maps() {
  $field_value = get_field('url_de_embed_do_google_maps');

  if (!empty($field_value)) {
    echo '<section class="section section-map">';
    echo '<iframe src="'. $field_value . '" frameborder="0" style="border:0" allowfullscreen></iframe>';
    echo '</section>';
  }
}

function list_all_unidades($except = []) {
  return new WP_Query([
    'post_type' => 'unidades',
    'post_status' => 'publish',
    'post__not_in' => $except
  ]);
}


get_header();
?>

<?php
while (have_posts()):
  the_post();
  $id = get_the_ID();
?>
  <section class="hero hero-unidade" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
    <div class="hero-wrapper">
      <div class="hero-left">
        <div class="hero-content">
          <div class="unidade-titles">
            <p class="unidade-hat">Unidade</p>
            <p class="unidade-title"><?php the_title(); ?></p>
          </div>
          <div class="unidade-endereco">
            <p class="unidade-hat">Endereço</p>
            <p class="unidade-title"><?php the_field('endereco_completo'); ?></p>
          </div>
          <div class="unidade-horarios">
            <p class="unidade-hat">Horários</p>
            <table>
              <thead>
              <tr>
                <td>Seg - Sex</td>
                <td>Sábado</td>
                <td>Domingo</td>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td><?php the_field('horario_de_segunda_a_sexta'); ?></td>
                <td><?php the_field('horario_de_sabado'); ?></td>
                <td><?php the_field('horario_de_domingo'); ?></td>
              </tr>
              </tbody>
            </table>
          </div>
          <div class="unidade-contato">
            <p class="unidade-hat">Contato</p>
            <p class="unidade-title"><?php fixo(); ?><br><?php whatsapp(); ?><br><?php instagram(); ?></p>
          </div>
        </div>
      </div>
      <div class="hero-right"></div>
    </div>
  </section>


  <?php if (has_gallery_pictures($id)): ?>
  <section class="section section-four">
    <div class="section-wrapper">
      <div class="section-row">
        <div class="row-titles">
          <p class="row-title">Fotos</p>
        </div>

        <div class="gallery">
          <?php gallery_pictures($id); ?>
        </div>

        <!-- Root element of PhotoSwipe. Must have class pswp. -->
        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

          <!-- Background of PhotoSwipe.
               It's a separate element, as animating opacity is faster than rgba(). -->
          <div class="pswp__bg"></div>

          <!-- Slides wrapper with overflow:hidden. -->
          <div class="pswp__scroll-wrap">

            <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
            <!-- don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
              <div class="pswp__item"></div>
              <div class="pswp__item"></div>
              <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

              <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                  <div class="pswp__preloader__icn">
                    <div class="pswp__preloader__cut">
                      <div class="pswp__preloader__donut"></div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
              </div>

              <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
              </button>

              <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
              </button>

              <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
  <?php endif; ?>

  <?php google_maps(); ?>
<?php endwhile; ?>

<section id="unidades" class="section section-three" style="background-image: url('<?php echo get_template_directory_uri() . '/resources/img-min/FB017_0001XY2.jpg' ?>')">
  <div class="section-wrapper">
    <div class="section-row small-pad">
      <div class="row-titles">
        <p class="row-title">Outras Unidades</p>
      </div>
      <div class="section-cards">
        <?php
        $query = list_all_unidades([$id]);
        while ($query->have_posts()) :
          $query->the_post();
          ?>
          <div class="section-card section-card-one" data-url="<?php the_permalink(); ?>">
            <div class="card-picture" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')"></div>
            <p class="card-title"><?php the_title(); ?></p>
            <div class="card-info">
              <label>Endereço:</label>
              <p><?php the_field('endereco_completo'); ?></p>
            </div>
            <a class="btn" href="<?php the_permalink(); ?>">Ver mais</a>
          </div>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
    var initPhotoSwipeFromDOM = function(gallerySelector) {

      // parse slide data (url, title, size ...) from DOM elements
      // (children of gallerySelector)
      var parseThumbnailElements = function(el) {
        var thumbElements = el.childNodes,
          numNodes = thumbElements.length,
          items = [],
          figureEl,
          linkEl,
          size,
          item;

        for(var i = 0; i < numNodes; i++) {

          figureEl = thumbElements[i]; // <figure> element

          // include only element nodes
          if(figureEl.nodeType !== 1) {
            continue;
          }

          linkEl = figureEl.children[0]; // <a> element

          size = linkEl.getAttribute('data-size').split('x');

          // create slide object
          item = {
            src: linkEl.getAttribute('href'),
            w: parseInt(size[0], 10),
            h: parseInt(size[1], 10)
          };



          if(figureEl.children.length > 1) {
            // <figcaption> content
            item.title = figureEl.children[1].innerHTML;
          }

          if(linkEl.children.length > 0) {
            // <img> thumbnail element, retrieving thumbnail url
            item.msrc = linkEl.children[0].getAttribute('src');
          }

          item.el = figureEl; // save link to element for getThumbBoundsFn
          items.push(item);
        }

        return items;
      };

      // find nearest parent element
      var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
      };

      // triggers when user clicks on thumbnail
      var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
          return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
          return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
          childNodes = clickedListItem.parentNode.childNodes,
          numChildNodes = childNodes.length,
          nodeIndex = 0,
          index;

        for (var i = 0; i < numChildNodes; i++) {
          if(childNodes[i].nodeType !== 1) {
            continue;
          }

          if(childNodes[i] === clickedListItem) {
            index = nodeIndex;
            break;
          }
          nodeIndex++;
        }



        if(index >= 0) {
          // open PhotoSwipe if valid index found
          openPhotoSwipe( index, clickedGallery );
        }
        return false;
      };

      // parse picture index and gallery index from URL (#&pid=1&gid=2)
      var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
          params = {};

        if(hash.length < 5) {
          return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
          if(!vars[i]) {
            continue;
          }
          var pair = vars[i].split('=');
          if(pair.length < 2) {
            continue;
          }
          params[pair[0]] = pair[1];
        }

        if(params.gid) {
          params.gid = parseInt(params.gid, 10);
        }

        return params;
      };

      var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
          gallery,
          options,
          items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

          // define gallery index (for URL)
          galleryUID: galleryElement.getAttribute('data-pswp-uid'),

          getThumbBoundsFn: function(index) {
            // See Options -> getThumbBoundsFn section of documentation for more info
            var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
              pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
              rect = thumbnail.getBoundingClientRect();

            return {x:rect.left, y:rect.top + pageYScroll, w:rect.width};
          }

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
          if(options.galleryPIDs) {
            // parse real index when custom PIDs are used
            // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
            for(var j = 0; j < items.length; j++) {
              if(items[j].pid == index) {
                options.index = j;
                break;
              }
            }
          } else {
            // in URL indexes start from 1
            options.index = parseInt(index, 10) - 1;
          }
        } else {
          options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
          return;
        }

        if(disableAnimation) {
          options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
      };

      // loop through all gallery elements and bind events
      var galleryElements = document.querySelectorAll( gallerySelector );

      for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
      }

      // Parse URL and open gallery if it contains #&pid=3&gid=1
      var hashData = photoswipeParseHash();
      if(hashData.pid && hashData.gid) {
        openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
      }
    };

    // execute above function
    initPhotoSwipeFromDOM('.gallery');
  </script>
<?php
get_footer();
