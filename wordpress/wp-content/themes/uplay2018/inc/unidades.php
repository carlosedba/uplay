<?php

function custom_post_type() {
    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Unidades', 'Post Type General Name', 'uplay2018' ),
        'singular_name'       => _x( 'Unidade', 'Post Type Singular Name', 'uplay2018' ),
        'menu_name'           => __( 'Unidades', 'uplay2018' ),
        'parent_item_colon'   => __( 'Unidade Pai', 'uplay2018' ),
        'all_items'           => __( 'Todas Unidades', 'uplay2018' ),
        'view_item'           => __( 'Ver Unidade', 'uplay2018' ),
        'add_new_item'        => __( 'Nova Unidade', 'uplay2018' ),
        'add_new'             => __( 'Adicionar nova', 'uplay2018' ),
        'edit_item'           => __( 'Editar Unidade', 'uplay2018' ),
        'update_item'         => __( 'Atualizar Unidade', 'uplay2018' ),
        'search_items'        => __( 'Procurar por Unidade', 'uplay2018' ),
        'not_found'           => __( 'Nada encontrado', 'uplay2018' ),
        'not_found_in_trash'  => __( 'Nada encontrado na Lixeira', 'uplay2018' ),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label'               => __( 'unidades', 'uplay2018' ),
        'description'         => __( 'Informações de unidades', 'uplay2018' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'thumbnail', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 4,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
    );

    // Registering your Custom Post Type
    register_post_type( 'unidades', $args );
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action( 'init', 'custom_post_type', 0 );