<?php
/**
 * Academia Uplay Fitness 2018 Theme Customizer
 *
 * @package Academia_Uplay_Fitness_2018
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function uplay2018_customize_register($wp_customize)
{
  $wp_customize->add_panel('uplay2018_theme_options', [
    'title' => __('Opções do Tema', 'uplay2018'),
    'description' => __('Altere partes do tema como os slides e os textos da página inicial.', 'uplay2018'),
  ]);

  $wp_customize->add_section('uplay2018_slider_options', [
    'panel' => 'uplay2018_theme_options',
    'title' => __('Slider', 'uplay2018'),
    'description' => __('Ajuste as opções do slider.', 'uplay2018')
  ]);

  $wp_customize->add_setting('uplay2018_slider_toggle', []);
  $wp_customize->add_control('uplay2018_slider_toggle', [
    'label' => __('Ativado', 'uplay2018'),
    'section' => 'uplay2018_slider_options',
    'type' => 'checkbox'
  ]);

  $wp_customize->add_setting('uplay2018_slider_picture_1', []);
  $wp_customize->add_control(
    new WP_Customize_Image_Control(
      $wp_customize,
      'uplay2018_slider_picture_1',
      [
        'label' => __('Imagem 1', 'uplay2018'),
        'section' => 'uplay2018_slider_options'
      ]
    )
  );

  $wp_customize->add_setting('uplay2018_slider_picture_2', []);
  $wp_customize->add_control(
    new WP_Customize_Image_Control(
      $wp_customize,
      'uplay2018_slider_picture_2',
      [
        'label' => __('Imagem 2', 'uplay2018'),
        'section' => 'uplay2018_slider_options'
      ]
    )
  );

  $wp_customize->add_setting('uplay2018_slider_picture_3', []);
  $wp_customize->add_control(
    new WP_Customize_Image_Control(
      $wp_customize,
      'uplay2018_slider_picture_3',
      [
        'label' => __('Imagem 3', 'uplay2018'),
        'section' => 'uplay2018_slider_options'
      ]
    )
  );

  $wp_customize->add_setting('uplay2018_slider_picture_4', []);
  $wp_customize->add_control(
    new WP_Customize_Image_Control(
      $wp_customize,
      'uplay2018_slider_picture_4',
      [
        'label' => __('Imagem 4', 'uplay2018'),
        'section' => 'uplay2018_slider_options'
      ]
    )
  );

  $wp_customize->add_setting('uplay2018_slider_picture_5', []);
  $wp_customize->add_control(
    new WP_Customize_Image_Control(
      $wp_customize,
      'uplay2018_slider_picture_5',
      [
        'label' => __('Imagem 5', 'uplay2018'),
        'section' => 'uplay2018_slider_options'
      ]
    )
  );

  $wp_customize->add_section('uplay2018_frase_apresentacao', [
    'panel' => 'uplay2018_theme_options',
    'title' => __('Frase de apresentação', 'uplay2018'),
  ]);

  $wp_customize->add_setting('uplay2018_frase_apresentacao_1', []);
  $wp_customize->add_control('uplay2018_frase_apresentacao_1', [
    'label' => __('Texto 1', 'uplay2018'),
    'section' => 'uplay2018_frase_apresentacao',
    'type' => 'text'
  ]);

  $wp_customize->add_setting('uplay2018_frase_apresentacao_2', []);
  $wp_customize->add_control('uplay2018_frase_apresentacao_2', [
    'label' => __('Texto 2', 'uplay2018'),
    'section' => 'uplay2018_frase_apresentacao',
    'type' => 'text'
  ]);

  $wp_customize->add_section('uplay2018_introducao', [
    'panel' => 'uplay2018_theme_options',
    'title' => __('Introdução', 'uplay2018'),
  ]);

  $wp_customize->add_setting('uplay2018_introducao_titulo', []);
  $wp_customize->add_control('uplay2018_introducao_titulo', [
    'label' => __('Título', 'uplay2018'),
    'section' => 'uplay2018_introducao',
    'type' => 'text'
  ]);

  $wp_customize->add_setting('uplay2018_introducao_texto', []);
  $wp_customize->add_control('uplay2018_introducao_texto', [
    'label' => __('Texto', 'uplay2018'),
    'section' => 'uplay2018_introducao',
    'type' => 'textarea'
  ]);

  $wp_customize->add_setting('uplay2018_introducao_imagem', []);
  $wp_customize->add_control(
    new WP_Customize_Image_Control(
      $wp_customize,
      'uplay2018_introducao_imagem',
      [
        'label' => __('Imagem', 'uplay2018'),
        'section' => 'uplay2018_introducao'
      ]
    )
  );

  $wp_customize->get_setting('blogname')->transport = 'postMessage';
  $wp_customize->get_setting('blogdescription')->transport = 'postMessage';
  $wp_customize->get_setting('header_textcolor')->transport = 'postMessage';

  if (isset($wp_customize->selective_refresh)) {
    $wp_customize->selective_refresh->add_partial(
      'blogname',
      array(
        'selector' => '.site-title a',
        'render_callback' => 'uplay2018_customize_partial_blogname',
      )
    );
    $wp_customize->selective_refresh->add_partial(
      'blogdescription',
      array(
        'selector' => '.site-description',
        'render_callback' => 'uplay2018_customize_partial_blogdescription',
      )
    );
  }
}

add_action('customize_register', 'uplay2018_customize_register');

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function uplay2018_customize_partial_blogname()
{
  bloginfo('name');
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function uplay2018_customize_partial_blogdescription()
{
  bloginfo('description');
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function uplay2018_customize_preview_js()
{
  wp_enqueue_script('uplay2018-customizer', get_template_directory_uri() . '/js/customizer.js', array('customize-preview'), _S_VERSION, true);
}

add_action('customize_preview_init', 'uplay2018_customize_preview_js');
