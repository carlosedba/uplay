<?php

function uplay2018_customize_register( $wp_customize ) {
    $wp_customize->add_panel('uplay2018_theme_options', [
        'title' => __('Opções do Tema', 'uplay2018'),
        'description' => __('Modificações do tema como os slides e textos da página inicial podem ser feitas aqui.', 'uplay2018'),
    ]);

    //$wp_customize->add_control('home_slider', []);
}

add_action( 'customize_register', 'uplay2018_customize_register' );

