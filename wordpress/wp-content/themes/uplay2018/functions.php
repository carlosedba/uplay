<?php
/**
 * Academia Uplay Fitness 2018 functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Academia_Uplay_Fitness_2018
 */

if (!defined( 'THEME_VERSION')) {
	// Replace the version number of the theme on each release.
	define( 'THEME_VERSION', '1.0.0' );
}

if (!function_exists( 'uplay2018_setup')) {
  function uplay2018_setup() {
    // Make theme available for translation.
    load_theme_textdomain('uplay2018', get_template_directory() . '/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');

    // Let WordPress manage the page title.
    add_theme_support('title-tag');

    // Enable support for Post Thumbnails on posts and pages.
    add_theme_support('post-thumbnails');

    // Register the navigation menu.
    register_nav_menus([
      'navbar-menu' => esc_html__('Navbar Menu', 'uplay2018')
    ]);

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support(
      'html5',
      array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
        'style',
        'script',
      )
    );

    // Set up the WordPress core custom background feature.
    add_theme_support(
      'custom-background',
      apply_filters(
        'uplay2018_custom_background_args',
        [
          'default-color' => 'ffffff',
          'default-image' => '',
        ]
      )
    );

    // Add theme support for selective refresh for widgets.
    add_theme_support('customize-selective-refresh-widgets');
  }
}
add_action('after_setup_theme', 'uplay2018_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function uplay2018_content_width() {
	$GLOBALS['content_width'] = 640;
}
add_action('after_setup_theme', 'uplay2018_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function uplay2018_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'uplay2018' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'uplay2018' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'uplay2018_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function uplay2018_scripts() {

  // Vendor CSS
  wp_enqueue_style(
    'normalize-vendor-style',
    get_template_directory_uri() . '/vendor/normalize/normalize.css',
    [],
    '1.0.0'
  );
  wp_enqueue_style(
    'hamburgers-vendor-style',
    get_template_directory_uri() . '/vendor/hamburgers/hamburgers.min.css',
    [],
    '1.0.0'
  );
  wp_enqueue_style(
    'flickity-vendor-style',
    get_template_directory_uri() . '/vendor/flickity/flickity.min.css',
    [],
    '1.0.0'
  );

  if (is_front_page()) {
    wp_enqueue_style(
      'tiny-slider-vendor-style',
      get_template_directory_uri() . '/vendor/tiny-slider/tiny-slider.css',
      [],
      '1.0.0'
    );
  }

  if (get_post_type() == 'unidades') {
    wp_enqueue_style(
      'photoswipe-vendor-style',
      get_template_directory_uri() . '/vendor/photoswipe/photoswipe.css',
      [],
      '1.0.0'
    );
    wp_enqueue_style(
      'photoswipe-skin-vendor-style',
      get_template_directory_uri() . '/vendor/photoswipe/default-skin/default-skin.css',
      [],
      '1.0.0'
    );
  }

  // Application CSS
	wp_enqueue_style(
	  'uplay2018-style',
    get_stylesheet_uri(),
    [],
    '1.0.0'
  );
	wp_enqueue_style(
	  'uplay2018-inputs-style',
    get_template_directory_uri() . '/resources/css/inputs.css',
    [],
    '1.0.0'
  );
	wp_enqueue_style(
	  'uplay2018-buttons-style',
    get_template_directory_uri() . '/resources/css/buttons.css',
    [],
    '1.0.0'
  );
	wp_enqueue_style(
	  'uplay2018-main-style',
    get_template_directory_uri() . '/resources/css/main.css',
    [],
    '1.0.0'
  );

  // Fonts
  wp_enqueue_style(
    'poppins-font',
    get_template_directory_uri() . '/resources/fonts/poppins/stylesheet.css',
    [],
    '1.0.0'
  );
  wp_enqueue_style(
    'futura-font',
    get_template_directory_uri() . '/resources/fonts/futura/stylesheet.css',
    [],
    '1.0.0'
  );
  wp_enqueue_style(
    'neosanspro-font',
    get_template_directory_uri() . '/resources/fonts/neosanspro/stylesheet.css',
    [],
    '1.0.0'
  );

  wp_enqueue_script(
    'flickity-vendor-script',
    get_template_directory_uri() . '/vendor/flickity/flickity.pkgd.min.js',
    [],
    '1.0.0',
    true
  );

  // Vendor Scripts
  if (is_front_page()) {
    wp_enqueue_script(
      'tiny-slider-helper-vendor-script',
      get_template_directory_uri() . '/vendor/tiny-slider/tiny-slider.helper.ie8.js',
      [],
      '1.0.0',
      true
    );
    wp_enqueue_script(
      'tiny-slider-vendor-script',
      get_template_directory_uri() . '/vendor/tiny-slider/tiny-slider.js',
      [],
      '1.0.0',
      true
    );
  }

  if (get_post_type() == 'unidades') {
    wp_enqueue_script(
      'photoswipe-vendor-script',
      get_template_directory_uri() . '/vendor/photoswipe/photoswipe.min.js',
      [],
      '1.0.0',
      true
    );
    wp_enqueue_script(
      'photoswipe-ui-vendor-script',
      get_template_directory_uri() . '/vendor/photoswipe/photoswipe-ui-default.min.js',
      [],
      '1.0.0',
      true
    );
  }

	if (is_singular() && comments_open() && get_option( 'thread_comments' )) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action('wp_enqueue_scripts', 'uplay2018_scripts');

// Implement the Custom Header feature.
require get_template_directory() . '/inc/custom-header.php';

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Functions which enhance the theme by hooking into WordPress.
require get_template_directory() . '/inc/template-functions.php';

// Customizer additions.
require get_template_directory() . '/inc/customizer.php';

// Unidades - Custom Type
require get_template_directory() . '/inc/unidades.php';

//flush_rewrite_rules( false );
