<?php

function fixo() {
  $field_value = get_field('numero_de_telefone_fixo');

  if (!empty($field_value)) {
    echo 'Fixo: ' . $field_value;
  }
}

function whatsapp() {
  $field_value = get_field('numero_de_whatsapp');

  if (!empty($field_value)) {
    echo 'WhatsApp: ' . $field_value;
  }
}

function instagram() {
  $field_value = get_field('conta_do_instagram');

  if (!empty($field_value)) {
    echo 'Instagram: <a href="https://instagram.com/' . $field_value . '" target="_blank">@' . $field_value . '</a>';
  }
}

function gallery_pictures($id) {
  $images = acf_photo_gallery('fotos', $id);

  if (count($images)) {
    var_dump($images);
    var_dump(get_field('fotos'));
    foreach ($images as $image) {
      echo '<figure itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">';
      echo '<a href="' . $image['url'] . '" itemprop="contentUrl" data-size="1024x768">';
      echo '<img src="' . $image['url'] . '" itemprop="thumbnail"/>';
      echo '</a>';
      echo '<figcaption itemprop="caption description"></figcaption>';
      echo '</figure>';
    }
  }
}

function google_maps() {
  $field_value = get_field('url_de_embed_do_google_maps');

  if (!empty($field_value)) {
    echo '<section class="section section-map">';
    echo '<iframe src="'. $field_value . '" frameborder="0" style="border:0" allowfullscreen></iframe>';
    echo '</section>';
  }
}

function list_all_unidades($except = []) {
  return new WP_Query([
    'post_type' => 'unidades',
    'post_status' => 'publish',
    'post__not_in' => $except
  ]);
}


get_header();
?>

<?php
while (have_posts()):
  the_post();
  $id = get_the_ID();
?>
  <section class="hero hero-unidade" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
    <div class="hero-wrapper">
      <div class="hero-left">
        <div class="hero-content">
          <div class="unidade-titles">
            <p class="unidade-hat">Unidade</p>
            <p class="unidade-title"><?php the_title(); ?></p>
          </div>
          <div class="unidade-endereco">
            <p class="unidade-hat">Endereço</p>
            <p class="unidade-title"><?php the_field('endereco_completo'); ?></p>
          </div>
          <div class="unidade-horarios">
            <p class="unidade-hat">Horários</p>
            <table>
              <thead>
              <tr>
                <td>Seg - Sex</td>
                <td>Sábado</td>
                <td>Domingo</td>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td><?php the_field('horario_de_segunda_a_sexta'); ?></td>
                <td><?php the_field('horario_de_sabado'); ?></td>
                <td><?php the_field('horario_de_domingo'); ?></td>
              </tr>
              </tbody>
            </table>
          </div>
          <div class="unidade-contato">
            <p class="unidade-hat">Contato</p>
            <p class="unidade-title"><?php fixo(); ?><br><?php whatsapp(); ?><br><?php instagram(); ?></p>
          </div>
        </div>
      </div>
      <div class="hero-right"></div>
    </div>
  </section>

  <section class="section section-four">
    <div class="section-wrapper">
      <div class="section-row">
        <div class="row-titles">
          <p class="row-title">Fotos</p>
        </div>

        <div class="gallery">
          <?php gallery_pictures($id); ?>
        </div>

        <!-- Root element of PhotoSwipe. Must have class pswp. -->
        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

          <!-- Background of PhotoSwipe.
               It's a separate element, as animating opacity is faster than rgba(). -->
          <div class="pswp__bg"></div>

          <!-- Slides wrapper with overflow:hidden. -->
          <div class="pswp__scroll-wrap">

            <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
            <!-- don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
              <div class="pswp__item"></div>
              <div class="pswp__item"></div>
              <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

              <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button class="pswp__button pswp__button--share" title="Share"></button>

                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo https://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                  <div class="pswp__preloader__icn">
                    <div class="pswp__preloader__cut">
                      <div class="pswp__preloader__donut"></div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
              </div>

              <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
              </button>

              <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
              </button>

              <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </section>

  <?php google_maps(); ?>
<?php endwhile; ?>

<section id="unidades" class="section section-three" style="background-image: url('<?php echo get_template_directory_uri() . '/resources/img-min/FB017_0001XY2.jpg' ?>')">
  <div class="section-wrapper">
    <div class="section-row small-pad">
      <div class="row-titles">
        <p class="row-title">Outras Unidades</p>
      </div>
      <div class="section-cards">
        <?php
        $query = list_all_unidades([$id]);
        while ($query->have_posts()) :
          $query->the_post();
          ?>
          <div class="section-card section-card-one" data-url="<?php the_permalink(); ?>">
            <div class="card-picture" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')"></div>
            <p class="card-title"><?php the_title(); ?></p>
            <div class="card-info">
              <label>Endereço:</label>
              <p><?php the_field('endereco_completo'); ?></p>
            </div>
            <a class="btn" href="<?php the_permalink(); ?>">Ver mais</a>
          </div>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
