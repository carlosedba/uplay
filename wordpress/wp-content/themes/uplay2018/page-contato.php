<?php /* Template Name: Contato */
get_header();
?>

<?php
while (have_posts()):
  the_post();
  ?>
  <section class="page-header page-header-alpha" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
    <div class="page-header-overlay"></div>
    <div class="page-header-wrapper">
      <p class="header-title"><?php the_title(); ?></p>
      <div class="breadcrumbs">
        <a class="breadcrumbs-item" href="<?php site_url(); ?>">Início</a>
        <a class="breadcrumbs-item active" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
      </div>
    </div>
  </section>

  <section class="section section-contato">
    <div class="section-wrapper">
      <div class="section-row">
        <?php the_content(); ?>
      </div>
    </div>
  </section>
<?php endwhile; ?>

<?php
get_footer();
