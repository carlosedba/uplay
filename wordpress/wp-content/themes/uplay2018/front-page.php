<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Academia_Uplay_Fitness_2018
 */

function get_hero_slides($num_slides) {
  for ($i = 1; $i < $num_slides + 1; $i++) {
    get_hero_slide($i);
  }
}

function get_hero_slide($num) {
  $prefix = 'uplay2018_slider_picture_';
  $theme_mod_name = $prefix . $num;
  $theme_mod_value = get_theme_mod($theme_mod_name);

  if ($theme_mod_value) {
    echo '<div class="hero-slide" style="background-image: url(' . $theme_mod_value . ')"></div>';
  }
}

function list_all_unidades($except = []) {
  return new WP_Query([
    'post_type' => 'unidades',
    'post_status' => 'publish',
    'post__not_in' => $except
  ]);
}

get_header();
?>

<section class="hero">
  <div class="hero-slides">
    <?php get_hero_slides(5); ?>
  </div>
  <div class="hero-titles">
    <p class="hero-pretitle"><?php echo get_theme_mod('uplay2018_frase_apresentacao_1'); ?></p>
    <p class="hero-title"><?php echo get_theme_mod('uplay2018_frase_apresentacao_2'); ?></p>
  </div>
</section>

<?php
while (have_posts()) :
  the_post();
?>
  <section class="section section-two">
    <div class="section-wrapper">
      <div class="section-row">
        <div class="section-column">
          <div class="column-titles">
            <p class="column-title"><?php the_field('titulo'); ?></p>
            <p class="column-subtitle"></p>
            <div class="column-text"><?php the_field('texto'); ?></div>
          </div>
        </div>
        <div class="section-column">
          <div class="column-lateral-image" style="background-image: url('<?php echo wp_get_attachment_image_url(get_field('imagem'), 'large-width'); ?>')"></div>
        </div>
      </div>
    </div>
  </section>
<?php endwhile; ?>

<section id="unidades" class="section section-three" style="background-image: url('<?php echo get_template_directory_uri() . '/resources/img-min/FB017_0001XY2.jpg' ?>')">
  <div class="section-wrapper">
    <div class="section-row small-pad">
      <div class="row-titles">
        <p class="row-title">Nossas Unidades</p>
      </div>
      <div class="section-cards">
        <?php
        $query = list_all_unidades();
        while ($query->have_posts()) :
          $query->the_post();
        ?>
          <div class="section-card section-card-one" data-url="<?php the_permalink(); ?>">
            <div class="card-picture" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')"></div>
            <p class="card-title"><?php the_title(); ?></p>
            <div class="card-info">
              <label>Endereço:</label>
              <p><?php the_field('endereco_completo'); ?></p>
            </div>
            <a class="btn" href="<?php the_permalink(); ?>">Ver mais</a>
          </div>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
</section>

<?php
get_footer();
